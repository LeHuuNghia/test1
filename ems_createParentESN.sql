USE [ctdata]
GO
/****** Object:  StoredProcedure [dbo].[ems_createParentESN]    Script Date: 4/12/2021 11:18:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* Copyright (C) 1995 - 2012 Absolute Software Corp.  All Rights Reserved.

<Name> ems_createParentESN </Name>
<Description>

Creates a new ParentESN for the given account.

</Description>

<Consumers>   All Objects - pages or procs that use this  </Consumers>
<Enterprise>  Yes       </Enterprise>
<SubSystem> CommonDB </SubSystem>
<Version>2.2</version>
<CreateOnDatabases>   ctdata, ltrctdata  </CreateOnDatabases>

<Package> Make_LicenseMaintain </Package>
<History>
Date          Release  Task #   Who Description
===================================================================================
12-Apr-2007            TMS11560 EL  Implemented standard comment header block.
14-Jan-2011   CDB6.11  WI40361  TM  Update UTC date columns in account_license
18-Jan-2011   CDB6.11  WI40801  TM  Add UTC date column in ParentESN 
27-Jan-2011   CDB6.11  WI39448  TM  Remove unused columns on ams_order 
03-Mar-2012	  CDB6.15  WI41142  TNQ Commondb : copy account, util_moveESN - move UTC dates for tables
</History>

*/


ALTER PROCEDURE [dbo].[ems_createParentESN]
	@AccountID	int
AS
SET     NOCOUNT        ON
SET     ROWCOUNT       0
SET     XACT_ABORT     ON

DECLARE  
	 @ctid		 int,
	 @ParentESN	 varchar(20),
	 @orgnum	 varchar(50),
	 @order_id	 int,
	 @OrderNumber	 varchar(50),
	 @rs		 int,
	 @has_commit int

Set @has_commit = 0; 
	  
-- Generate parent ESN for customer
-- Make sure enough unused ESNs are available
IF (SELECT count(*) FROM license(NOLOCK) WHERE orgnum = 'unused') < 10
BEGIN
	--report error message to user	
	SELECT 'Not enough unused ESNs to create a new parent ESN.' as err_desc	

	--raise error
	RAISERROR ('Not enough unused ESNs. %d ', 16,1, @@PROCID) --with LOG

END
ELSE
BEGIN
BEGIN TRAN
	--grab to first available esn
   	SELECT TOP 1 @ctid = l.CTID, @ParentESN = l.ESN 
	FROM license l(UPDLOCK) 
	left join parentesn pe(nolock) on l.esn = pe.esn
	WHERE orgnum = 'unused' and pe.esn is null

	--if the esn is not a parent esn (10 digits long) then raise error
   	IF datalength(@ParentESN) != 10
      		RAISERROR ('Parent ESN is not 10 bytes. %d ', 20,1, @@PROCID) --with LOG

   	-- Populate PARENTESN table
   	INSERT PARENTESN (
		ESN, 
		comment, 
		CreateDateUTC )
     	SELECT 
		@ParentESN, 
		'New Order ' + convert( varchar, @AccountID),
		GetUTCDate() 

   	-- Insert correspondent record to computer table
   	INSERT computer(
		ctid, 
		esn, 
		computer_id,
		serial,
		computer_make_id,
		computer_model_id,
		user_name, 
		creation_date,
		last_update_date,
		last_update_user,
		asset_number,
		account_id)
   	SELECT
		@ctid
		,upper(@ParentESN)
		,1
	        ,'PARENT ESN' -- serial
	        ,207 -- computer_make_id (a blank)
	        ,1516 -- computer_model_id (a blank)
	        ,'' -- user_name
	        ,GetDate()
	        ,GetDate()
	        ,'New Order'
	        ,''
		,@AccountID

   	----Start: Move Parent ESN to account----
   	
	--grab the account's orgnum
	SELECT @orgnum = orgnum 
	FROM dbo.vwAccount(NOLOCK)
    	WHERE account_id = @AccountID
	
	--raise error if it's null or blank
   	IF @orgnum IS NULL or @orgnum = ''
     		RAISERROR ('orgnum is empty. %d ', 20,1, @@PROCID) --with LOG
	
	--change the orgnum in license table from 'unused'
   	UPDATE license 
	SET orgnum = @orgnum
    	WHERE ESN = @ParentESN
	
	--grab an order_id to insert into account_license
   	SELECT TOP 1 @order_id = order_id 
	FROM ams_order(NOLOCK)
    	WHERE account_id = @AccountID 
	ORDER BY order_id

   	-- if the account has no orders then create an order
   	IF @order_id IS NULL
    	BEGIN
        	SELECT @OrderNumber = 'NEW' + convert( varchar, convert( int, rand()*1000000 ))

		--run ems_nextSequence_util 'order_id' to get the next available account_id
		EXEC @rs = ems_nextSequence_util 'order_id', @order_id output 
		if @rs != 0
      			RAISERROR (60101, 20,1) --with LOG

		Insert Into dbo.ams_order(
			 order_id
			,account_id
			,order_type_id
			,product_id
			,quantity
			,monitoring_start_date
			,monitoring_end_date
			,purchase_order_no
			,special_instructions
			,creation_date
			,last_update_date
			,last_update_user
			,item_no
			,real_quantity
			,monitoring_start_dateUTC
			,monitoring_end_dateUTC
			,creation_dateUTC
			,last_update_dateUTC)
		VALUES(	
			 @order_id
			,@accountid
			,6
			,1
			,0 --@quantity
                        ,GetDate()
			,GetDate()
			,@OrderNumber
			,'PARENT ESN - DO NOT USE'
			,GetDate()
			,GetDate()
			,'NEW Order'
			,''
			,0
			,GetUTCDate()
			,GetUTCDate()
			,GetUTCDate()
			,GetUTCDate()
			)
		
    	END

	--insert account_id/esn/order_id into account_license
   	INSERT account_license(
	       ctid,
	       esn,
	       account_id,
	       order_id,
	       creation_date,
	       last_update_date,
	       last_update_user,
	       computer_no, 
		   creation_dateUTC,
		   last_update_dateUTC )
   	SELECT
	       @ctid,
	       upper(@ParentESN),
	       @AccountID,
	       @order_id,
	       GetDate(),
	       GetDate(),
	       'New Order',
	       1,
		   GetUTCDate(),
		   GetUTCDate() 
	----End: Move Parent ESN to account----

	Set @has_commit = 1; 

	--parent esn creation successful, return err_desc
	SELECT 'OK' as err_desc
COMMIT

IF @has_commit = 1
BEGIN
	WAITFOR DELAY '00:00:04';

	UPDATE al
	SET al.creation_dateUTC = GETUTCDATE() 
	FROM account_license al
	WHERE al.esn = UPPER(@ParentESN)
	--AND al.ctid = @ctid
END 
return 1

END
