/**
 * Created by byan on 12/17/2015.
 */

import sql from 'mssql';
import Q from 'q';
import _ from 'lodash';

var SqlServerService = function(){
  this.cachedConnections = {};

  var connConfig = sails.config.connections[sails.config.lookupDB];
  this.persist = connConfig.persist;
}

// get connection from esn or account id on ctdata or ccdata
//dbName can be CTDATA or CCDATA, default to CTDATA
SqlServerService.prototype.getConnection = function(esn, accountId, dbName) {

  var self = this;
  var deferred = Q.defer();

  if ((esn == null || esn == undefined) && (accountId == null || accountId == undefined)) {
    var err = 'esn or accountId must have a value';
    deferred.reject(err);

    return deferred.promise;
  }

  if (dbName == null || dbName ==undefined) {
    dbName = 'CTDATA';
  }

  if (dbName != 'CTDATA' && dbName != 'CCDATA') {
    var err = 'Wrong dbName for connection: ' + dbName;
    deferred.reject(err);

    return deferred.promise;
  }

  sails.log.info('getConnection....');

  var dbPathOut;

  self.getDBPath(dbName, accountId, esn)
    .then(function(dbPath){

      dbPathOut = dbPath;
      sails.log.info('getDBPath return ' + dbPath);

      if (self.cachedConnections[dbPath] && self.cachedConnections[dbPath].connected && self.persist == true) {
        sails.log.info('using cached connection for ' + dbPath);
        return deferred.resolve(self.cachedConnections[dbPath]);
      }
      else {
        return self.getConnectionByDBPath(dbPath);
      }

    })
    .then(function(connection){
      if (connection) {
        sails.log.info('dbPath for account = ' + dbPathOut + ', ' + accountId);
        //console.dir(connection);
        deferred.resolve(connection);
      }
    })
    .catch(function (err) {
      sails.log.error('getConnection failed : ' + err);
      return deferred.reject(err);
    });

  return deferred.promise;
}

SqlServerService.prototype.getDBPath = function(dbName, accountId, esn) {

  var self = this;
  var deferred = Q.defer();

  sails.log.info('SqlServerService.prototype.getDBPath start ....' + JSON.stringify({dbName:dbName, accountId:accountId, esn:esn}));

  var storedProc = 'dbo.ems_getDBPath';
  var params = [
    {
      name:'DBName',
      type:sql.VarChar(255),
      value:dbName,
      input:true
    },
    {
      name:'AccountId',
      type:sql.Int,
      value:accountId,
      input:true
    },
    {
      name:'ESN',
      type:sql.VarChar(20),
      value:esn,
      input:true
    },
    {
      name:'DBPath_Out',
      type:sql.VarChar(255),
      input:false
    }
  ];

  self.getLookupDBConnection()
    .then(function(connLookupDB){
      return self.execStoredProcedureOnDB(connLookupDB, storedProc, params);
    })
    .then(function(result){
      var dbPath = result.params.DBPath_Out.value; //like DV2CORP3DB.ctdata
      sails.log.info('SqlServerService.prototype.getDBPath done: ' + dbPath + ', from ' + JSON.stringify({dbName:dbName, accountId:accountId, esn:esn}));
      deferred.resolve(dbPath);
    })
    .catch(function (err) {
      sails.log.error('SqlServerService.prototype.getDBPath : ' + JSON.stringify({dbName:dbName, accountId:accountId, esn:esn}), err);
      return deferred.reject(err);
    });

  return deferred.promise;
}

SqlServerService.prototype.getRedirectionDBConnection = function() {

  var self = this;
  var deferred = Q.defer();

  sails.log.info('getRedirectionDBConnection ....');


  var dbName = 'RedirectionDB';   //cache will by by this name
  if (self.cachedConnections[dbName] && self.cachedConnections[dbName].connected == true && self.persist == true) {
    sails.log.info('using cached RedirectionDB connection ....');
    deferred.resolve(self.cachedConnections[dbName]);
    return deferred.promise;
  }

  self.getDBPath(dbName,0,null)
    .then(function(dbPath){
      return self.getConnectionByDBPath(dbPath);
    })
    .then(function(connection){
      //cache it
      sails.log.info('caching RedirectionDB connection ....');
      self.cachedConnections[dbName] = connection;
      deferred.resolve(connection);
    })
    .catch(function (err) {
      sails.log.error('getRedirectionDBConnection failed : ' + err);
      return deferred.reject(err);
    });

  return deferred.promise;
}


// dbPath is like DV2CORP3DB.ctdata
SqlServerService.prototype.getConnectionByDBPath = function(dbPath) {

  var self = this;
  var deferred = Q.defer();

  var dbPaths = dbPath.split('.');
  if (dbPaths.length < 2) {
    var err = 'getConnectionByDBPath failed... ' + 'dbPath ' + dbPath + ' is incorrect';
    sails.log.error(err);
    deferred.reject(err);

    return deferred.promise;
  }

  // assume all the silos use the same credential as lookupDB
  var connConfig = _.cloneDeep(sails.config.connections[sails.config.lookupDB]);  //sails.config.connections[sails.config.lookupDB];

  //To support in case of diffrent domain
  //EX : ems_api : dv2int2ems1api.dv.absolute.com
  //     sql     : dv2internals2.absolute.com
  //Because of ems_api and sql are different domain , so to make sure ems_api can connect to sql server, system need use full_domain

  connConfig.server = dbPaths[0] + connConfig.full_domain;

  connConfig.database = dbPaths[1];
  connConfig.options.database = connConfig.database;

  var connection =  new sql.ConnectionPool(connConfig, function(err) {

    if (err) {
      sails.log.error('getLookupDBConnection failed: ' + err);
      return deferred.reject(err);
    }

    self.cachedConnections[dbPath] = connection;
    //console.log(connection);
    deferred.resolve(connection);
  });

  return deferred.promise;
}

SqlServerService.prototype.getLookupDBConnection = function() {

  var self = this;
  var deferred = Q.defer();

  if (self.cachedConnections[sails.config.lookupDB] && self.cachedConnections[sails.config.lookupDB].connected == true && self.persist == true) {
    sails.log.info('using cached lookupDB ...');
    deferred.resolve(self.cachedConnections[sails.config.lookupDB]);
    return deferred.promise;
  }

  var connConfig = sails.config.connections[sails.config.lookupDB];

  sails.log.info('persist: ' + self.persist);

  var connection = new sql.ConnectionPool(connConfig, function(err) {

    if (err) {
      sails.log.error('getLookupDBConnection failed: ' + err);
      return deferred.reject(err);
    }

    //cache the connection
    sails.log.info('caching lookupDB ...');
    self.cachedConnections[sails.config.lookupDB] = connection;
    deferred.resolve(connection);
  });


  return deferred.promise;

}

//this will be used for looking up DB: exec stored procedure with given connection
SqlServerService.prototype.execStoredProcedureOnDB = function (connection, storedProc, paramList) {

  var self = this;

  var deferred = Q.defer();

  sails.log.info('execStoredProcedureOnDB ....' + storedProc);
  sails.log.info('paramList=', paramList);

  var request  = connection.request();


  if (paramList && paramList.length > 0) {
    paramList.forEach(function (param) {

      if (param.input) {
        request.input(param.name, param.type, param.value)
      }
      else {
        request.output(param.name, param.type);
      }
    });
  }

  request.execute(storedProc, function (err, data, returnValue) {
    //close connection for non-persist connection
    if (self.persist == false) {
      sails.log.info('close connection...');
      connection.close();
    }
    if (err) {
      sails.log.error("Execute stored procedure error. " + err);
      return deferred.reject(err);
    }

    sails.log.info('execStoredProcedureOnDB ....' + storedProc + ' done.');


    if (data && data.output && Object.keys(data.output).length) {
      var returnList = data.output;
      for (var key in returnList) {
        if (returnList.hasOwnProperty(key)) {
          if(request && request.parameters && request.parameters[key]){
            request.parameters[key].value = returnList[key];
          }
        }
      }
    }

    return deferred.resolve({
       recordSet: data.recordsets,
	   params: request.parameters,
       returnValue: data.returnValue

    })
  })

  return deferred.promise;

}

//execute stored procedure on ctdata based on esn or accountId
//esn, accountId one of them must be valid
//dbName is CTDATA or CCDATA. Default to CTDATA
SqlServerService.prototype.execStoredProcedure = function (esn, accountId, storedProc, paramList, dbName) {

  var self = this;

  var deferred = Q.defer();

  self.getConnection(esn, accountId, dbName)
    .then(function(connection){
      return self.execStoredProcedureOnDB(connection,storedProc, paramList);
    })
    .then(function(result){
      deferred.resolve(result);
    })
    .catch(function (err) {
      sails.log.error('execStoredProcedure failed: ' + storedProc + ', ' + err);
      return deferred.reject(err);
    });

  return deferred.promise;
}

//esn, accountId one of them must be valid
SqlServerService.prototype.execQueryOnDB = function (connection, sqlStmt, paramList) {

  var self = this;

  var deferred = Q.defer();

  sails.log.info('execQueryOnDB ....' + sqlStmt);

  var request = connection.request();

  if (paramList && paramList.length > 0) {
    paramList.forEach(function (param) {
      request.input(param.name, param.type, param.value);
    });
  }

  sails.log.info('request.parameters : ' + JSON.stringify(request.parameters));

  request.query(sqlStmt, function (err, data) {
    //close connection for non-persist connection
    if (self.persist == false) {
      sails.log.info('close connection...');
      connection.close();
    }

    if (err) {
      sails.log.error("execQueryOnDB error. " + sqlStmt + '. ' + err);
      return deferred.reject(err);
    }

    return deferred.resolve(data.recordset);
  });

  return deferred.promise;
}

// execute query on ctdata for a ESN or AccountId
//esn, accountId one of them must be valid
//dbName is CTDATA or CCDATA. Default to CTDATA
SqlServerService.prototype.execQuery = function (esn, accountId, sqlStmt, paramList, dbName) {

  var self = this;

  var deferred = Q.defer();

  sails.log.info('execQuery ....');

  self.getConnection(esn, accountId, dbName)
    .then(function (connection) {
      sails.log.info('execQuery: sqlStmt = ' + sqlStmt);
      return self.execQueryOnDB(connection, sqlStmt, paramList );
    })
    .then(function(result){
      deferred.resolve(result);
    })
    .catch(function (err) {
      sails.log.error('execQuery failed: ' + sqlStmt + ', ' + err);
      return deferred.reject(err);
    });

  return deferred.promise;
}

//execute Stored Procedure on RedirectDB (Activation DB)
SqlServerService.prototype.execStoredProcedureOnRedirectDB = function (storedProc, paramList) {

  var self = this;

  var deferred = Q.defer();

  self.getRedirectionDBConnection()
    .then(function(connection){
      return self.execStoredProcedureOnDB(connection,storedProc, paramList);
    })
    .then(function(result){
      deferred.resolve(result);
    })
    .catch(function (err) {
      sails.log.error('execStoredProcedure failed: ' + storedProc + ', ' + err);
      return deferred.reject(err);
    });

  return deferred.promise;
}

//execute Stored Procedure on ActivationDB (Activation DB)
SqlServerService.prototype.execQueryOnRedirectDB = function (sqlStmt, paramList) {

  var self = this;

  var deferred = Q.defer();

  sails.log.info('execQuery ....');

  self.getRedirectionDBConnection()
    .then(function (connection) {
      return self.execQueryOnDB(connection, sqlStmt, paramList );
    })
    .then(function(result){
      deferred.resolve(result);
    })
    .catch(function (err) {
      sails.log.error('execQuery failed: ' + sqlStmt + ', ' + err);
      return deferred.reject(err);
    });

  return deferred.promise;
}

SqlServerService.prototype.execStoredProcedureOnOriDB = function (storedProc, paramList) {

  var self = this;

  var deferred = Q.defer();

  self.getOriDBConnection()
    .then(function(connection){
      return self.execStoredProcedureOnDB(connection,storedProc, paramList);
    })
    .then(function(result){
      deferred.resolve(result);
    })
    .catch(function (err) {
      sails.log.error('execStoredProcedure failed: ' + storedProc + ', ' + err);
      return deferred.reject(err);
    });

  return deferred.promise;
}

SqlServerService.prototype.getOriDBConnection = function() {

  var self = this;
  var deferred = Q.defer();

  sails.log.info('getOriDBConnection ....');


  var dbName = 'OriDB';   //cache will by by this name
  if (self.cachedConnections[dbName] && self.cachedConnections[dbName].connected == true && self.persist == true) {
    sails.log.info('using cached OriDB connection ....');
    deferred.resolve(self.cachedConnections[dbName]);
    return deferred.promise;
  }

  self.getDBPath(dbName,0,null)
    .then(function(dbPath){
      return self.getConnectionByDBPath(dbPath);
    })
    .then(function(connection){
      //cache it
      sails.log.info('caching OriDB connection ....');
      self.cachedConnections[dbName] = connection;
      deferred.resolve(connection);
    })
    .catch(function (err) {
      sails.log.error('getOriDBConnection failed : ' + err);
      return deferred.reject(err);
    });

  return deferred.promise;
}

SqlServerService.prototype.execStoredProcedureOnUserDB = function (storedProc, paramList) {

  var self = this;

  var deferred = Q.defer();

  self.getUserDBConnection()
    .then(function(connection){
      return self.execStoredProcedureOnDB(connection,storedProc, paramList);
    })
    .then(function(result){
      deferred.resolve(result);
    })
    .catch(function (err) {
      sails.log.error('execStoredProcedure failed: ' + storedProc + ', ' + err);
      return deferred.reject(err);
    });

  return deferred.promise;
}

SqlServerService.prototype.getUserDBConnection = function() {

  var self = this;
  var deferred = Q.defer();

  sails.log.info('getUserDBConnection ....');


  var dbName = 'UserDB';   //cache will by by this name
  if (self.cachedConnections[dbName] && self.cachedConnections[dbName].connected == true && self.persist == true) {
    sails.log.info('using cached UserDB connection ....');
    deferred.resolve(self.cachedConnections[dbName]);
    return deferred.promise;
  }

  self.getDBPath(dbName,0,null)
    .then(function(dbPath){
      return self.getConnectionByDBPath(dbPath);
    })
    .then(function(connection){
      //cache it
      sails.log.info('caching UserDB connection ....');
      self.cachedConnections[dbName] = connection;
      deferred.resolve(connection);
    })
    .catch(function (err) {
      sails.log.error('getUserDBConnection failed : ' + err);
      return deferred.reject(err);
    });

  return deferred.promise;
}

/*
 * Examples of Server Names
 *
 * ServerNames for Dev and QA-Env
 */
SqlServerService.prototype.getAgentLoggingConnection = function(serverName){

  var self = this;
  var deferred = Q.defer();

  sails.log.info('getLoggingConnection ....');

  var dbName = 'CTDATALOG';
  var dbPath =  serverName + '.' + dbName;

  //Cached by server Name.
  if (self.cachedConnections[dbPath] && self.cachedConnections[dbPath].connected == true && self.persist == true) {
    sails.log.info('using cached Logging connection ....');
    deferred.resolve(self.cachedConnections[dbPath]);
    return deferred.promise;
  }

  self.getConnectionLoggingDB(dbPath)
    .then(function(connection){
      //return connections.
      deferred.resolve(connection);
    })
    .catch(function (err) {
      sails.log.error('getAgentLoggingConnection failed : ' + err);
      return deferred.reject(err);
    });

  return deferred.promise;
}

//execute Stored Procedure on LoggingDB
SqlServerService.prototype.execQueryOnAgentLoggingDB = function (sqlStmt, paramList,serverName) {

  var self = this;

  var deferred = Q.defer();

  self.getAgentLoggingConnection(serverName)
    .then(function (connection) {

      sails.log.info('getAgentLoggingConnection and Preparing query data on ctdatalog');

      return self.execQueryOnDB(connection, sqlStmt, paramList );
    })
    .then(function(result){
      deferred.resolve(result);
    })
    .catch(function (err) {
      sails.log.error('execQuery failed: ' + sqlStmt + ', ' + err);
      return deferred.reject(err);
    });

  return deferred.promise;
}


// dbPath is like DV2CORP3DB.ctdata
SqlServerService.prototype.getConnectionLoggingDB = function(dbPath) {

  var self = this;
  var deferred = Q.defer();

  var dbPaths = dbPath.split('.');
  if (dbPaths.length < 2) {
    var err = 'getConnectionByDBPath failed... ' + 'dbPath ' + dbPath + ' is incorrect';
    sails.log.error(err);
    deferred.reject(err);

    return deferred.promise;
  }

  // assume all the silos use the same credential as lookupDB
  var connConfig = _.cloneDeep(sails.config.connections[sails.config.lookupDB]);  //sails.config.connections[sails.config.lookupDB];

  //To support in case of different domain
  //EX : ems_api : dv2int2ems1api.dv.absolute.com
  //     sql     : dv2internals2.absolute.com
  //Because of ems_api and sql are different domain , so to make sure ems_api can connect to sql server, system need use full_domain

  connConfig.server = dbPaths[0] + connConfig.server_agent_full_domain;

  connConfig.database = dbPaths[1];
  connConfig.options.database = connConfig.database;

  connConfig.user  = connConfig.server_agent_user;
  connConfig.password = connConfig.server_agent_pass;
  connConfig.domain = connConfig.server_agent_domain;

  var connection = new sql.ConnectionPool(connConfig, function(err) {

    if (err) {
      sails.log.error('getLookupDBConnection failed: ' + err);
      return deferred.reject(err);
    }

    self.cachedConnections[dbPath] = connection;
    //console.log(connection);
    deferred.resolve(connection);
  });

  return deferred.promise;
}

SqlServerService.prototype.getLTRDBConnection = function() {

  var self = this;
  var deferred = Q.defer();

  sails.log.info('getLTRDBConnection ....');


  var dbName = 'LTRDB';   //cache will by by this name
  if (self.cachedConnections[dbName] && self.cachedConnections[dbName].connected == true && self.persist == true) {
    sails.log.info('using cached LTRDB connection ....');
    deferred.resolve(self.cachedConnections[dbName]);
    return deferred.promise;
  }

  self.getDBPath(dbName,12773,'')
    .then(function(dbPath){
      return self.getConnectionByDBPath(dbPath);
    })
    .then(function(connection){
      //cache it
      sails.log.info('caching LTRDB connection ....');
      self.cachedConnections[dbName] = connection;
      deferred.resolve(connection);
    })
    .catch(function (err) {
      sails.log.error('getLTRDBConnection failed : ' + err);
      return deferred.reject(err);
    });

  return deferred.promise;
}

SqlServerService.prototype.execStoredProcedureOnLTRDB = function (storedProc, paramList) {

  var self = this;

  var deferred = Q.defer();

  self.getLTRDBConnection()
    .then(function(connection){
      return self.execStoredProcedureOnDB(connection,storedProc, paramList);
    })
    .then(function(result){
      deferred.resolve(result);
    })
    .catch(function (err) {
      sails.log.error('execStoredProcedure failed: ' + storedProc + ', ' + err);
      return deferred.reject(err);
    });

  return deferred.promise;
}




module.exports = new SqlServerService();
