/**
 * Policy Mappings
 *
 * Policies are simple functions which run before your controllers.
 * You can apply one or more policies to a given controller, or protect
 * its actions individually.
 *
 * Any policy file (e.g. `api/policies/authenticated.js`) can be accessed
 * below by its filename, minus the extension, (e.g. "authenticated")
 */

export default {
  policies: {

    '*': ['isAuthenticated'],

    RootController: {
      'home': true
    },

    VersionController: {
      'findVersions': true
    },

    AuthController: {
      'signin': true,
      'check': ['isAuthenticated']
    },

    'v1/AccountController':{
      'getMergeAccountConfigurationsforWorker': true,
      'accountmergehistory':true
    },

    'v2/AccountController':{
      'runExportJob' : true,
      'deleteAllUsersByAccountID': true,
      'getCountUserByAccountId': true,
      'getMergeAccountConfigurationsforWorker': true,
      'accountmergehistory': true,
      'getDeviceFreezeDDS5': true,
      'getIsFastContactSupportedForAccount':true,
      'getIMEINumberByESN': true
    },

    'v2/DeviceController':{
      'runSNJob': true
    },



    'v1/ESNMoveController': {
      'passRequestToWorker': true
    }

  }
}
