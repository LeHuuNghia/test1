/**
 * Winston Logger Configuration
 * For detailed information take a look here - https://github.com/Kikobeats/sails-hook-winston
 */

const winston = require('winston');
const { combine, timestamp, printf, colorize, json } = winston.format;
const WinstonLogStash = require('winston3-logstash-transport');


const myFormat = printf(({ level, message, label, timestamp }) => {
  return `${timestamp} ${level}: ${message}`;
});

const myCustomLevels = {
  colors: {
    info: 'green',
    error: 'red',
    debug: 'yellow',
    warning: 'red'
  }
};

winston.addColors(myCustomLevels.colors);


const logger = winston.createLogger({
    'transports': [
    new (winston.transports.Console)({
      'level': 'error',
      format: combine(
        timestamp(),
        myFormat,
        colorize()
      ),
    }),
    new winston.transports.File({
      'level': 'error',
      format: combine(
        timestamp(),
        myFormat,
        colorize(),
        json()
      ),
      'filename': '/var/log/absolute-ems-api/out.log',
      'maxsize': 5120000,
      'maxFiles': 3
    })
  ]
})

export default {
  log: {
    'level' : 'error',
    'colors': true,
    'custom': logger
  }
};
