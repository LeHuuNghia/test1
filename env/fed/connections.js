/**
 * Connections API Configuration
 *
 * Connections are like "saved settings" for your adapters.
 * Each model must have a `connection` property (a string) which is references the name of one
 * of these connections.  If it doesn't, the default `connection` configured in `config/models.js`
 * will be applied.  Of course, a connection can (and usually is) shared by multiple models.
 *
 * NOTE: If you're using version control, you should put your passwords/api keys
 * in `config/local.js`, environment variables, or use another strategy.
 */

export default {
  connections: {
    /**
     * MongoDB configuration
     * @type {Object}
     */

      lookupDB: {
        server: 'mssql-persistance.cdwswjp7nuvv.ca-central-1.rds.amazonaws.com', //'dv2corp3db',
        user: 'rds_ems', //'emsuser',
        password: 'Absolute0225$',  //'Password2015',
        database: 'ctdata',
        //domain: process.env.EMS_SQLSERVER_LOOKUP_DB_DOMAIN, //'ABSOLUTE'
        //full_domain : process.env.EMS_SQLSERVER_LOOKUP_DB_FULL_DOMAIN, //'.absolute.com' or ''

        server_agent : process.env.EMS_SQLSERVER_LOOKUP_CTDATALOG_DB,        //'pdsctlog1,pdsctlog1'
        server_agent_user : process.env.EMS_SQLSERVER_LOOKUP_CTDATALOG_USER ,   //'qa-automation'
        server_agent_pass : process.env.EMS_SQLSERVER_LOOKUP_CTDATALOG_USER_PASSWORD ,   //'thxity01'
        server_agent_domain : process.env.EMS_SQLSERVER_LOOKUP_CTDATALOG_DOMAIN , //'ABSOLUTE'
        server_agent_full_domain : process.env.EMS_SQLSERVER_LOOKUP_CTDATALOG_FULL_DOMAIN, //'.absolute.com' or ''

        requestTimeout :10 * 60 * 1000,
        persist:false,
        options:{
        encrypt: process.env.EMS_SQLSERVER_LOOKUP_DB_ENCRYPTION === '1',
		packetSize: 16384
      }
    },

      lookupDB_dev: {
        server: 'dv2archdb3.absolute.com', //dv2internals2.absolute.com
        user: 'DevWS',
        password: 'Dev$WS',  //'Password2015',
        database: 'ctdata',
        domain: 'ABSOLUTE',
        full_domain : '.absolute.com', //'.absolute.com' or ''

        server_agent : 'pdsctlog1,pdsctlog1',
        server_agent_user : 'qa-automation',
        server_agent_pass : 'thxity01',
        server_agent_domain : 'ABSOLUTE',
        server_agent_full_domain : '.absolute.com',

        persist:false,
        requestTimeout :10 * 60 * 1000,
        options:{
          encrypt:true
        },
        stream:true,
        pool: {
          max: 10,
          min: 0,
          idleTimeoutMillis: 300000
        }
    }
  }
}
