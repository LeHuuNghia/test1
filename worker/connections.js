/**
 * Connections
 * (sails.config.connections)
 *
 * `Connections` are like "saved settings" for your adapters.  What's the difference between
 * a connection and an adapter, you might ask?  An adapter (e.g. `sails-mysql`) is generic--
 * it needs some additional information to work (e.g. your database host, password, user, etc.)
 * A `connection` is that additional information.
 *
 * Each model must have a `connection` property (a string) which is references the name of one
 * of these connections.  If it doesn't, the default `connection` configured in `config/models.js`
 * will be applied.  Of course, a connection can (and usually is) shared by multiple models.
 * .
 * Note: If you're using version control, you should put your passwords/api keys
 * in `config/local.js`, environment variables, or use another strategy.
 * (this is to prevent you inadvertently sensitive credentials up to your repository.)
 *
 * For more information on configuration, check out:
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.connections.html
 */

module.exports.connections = {

  /***************************************************************************
  *                                                                          *
  * Local disk storage for DEVELOPMENT ONLY                                  *
  *                                                                          *
  * Installed by default.                                                    *
  *                                                                          *
  ***************************************************************************/
  localDiskDb: {
    adapter: 'sails-disk'
  },

  localSqlite3: {
    filename: process.env.EMS_WORKER_DB_PATH || './.tmp/db.sqlite'
  },

  lookupDB: {
    server: 'mssql-persistance.cdwswjp7nuvv.ca-central-1.rds.amazonaws.com', //'dv2corp3db',
    user: 'rds_ems' , //'emsuser',
    password: 'Absolute0225$',  //'Password2015',
    database: 'ctdata',
    //domain: process.env.EMS_SQLSERVER_LOOKUP_DB_DOMAIN, //'ABSOLUTE',
    //full_domain : process.env.EMS_SQLSERVER_LOOKUP_DB_FULL_DOMAIN, //'.absolute.com' or ''
    requestTimeout :10 * 60 * 1000,
    persist:false,
    options:{
		encrypt: process.env.EMS_SQLSERVER_LOOKUP_DB_ENCRYPTION === '1'
	}
  },

  lookupDB_dev: {
    server: 'dv2internals2.absolute.com', //dv2internals2.absolute.com
    user: 'DevWS',
    password: 'Dev$WS',  //'Password2015',
    database: 'ctdata',
    domain: 'ABSOLUTE',
    full_domain : '.absolute.com', //'.absolute.com' or ''
    requestTimeout :10 * 60 * 1000,
    persist:false,
    options:{
		encrypt:false
	}
  }

  /***************************************************************************
  *                                                                          *
  * More adapters: https://github.com/balderdashy/sails                      *
  *                                                                          *
  ***************************************************************************/

};
