/**
 * Built-in Log Configuration
 * (sails.config.log)
 *
 * Configure the log level for your app, as well as the transport
 * (Underneath the covers, Sails uses Winston for logging, which
 * allows for some pretty neat custom transports/adapters for log messages)
 *
 * For more information on the Sails logger, check out:
 * http://sailsjs.org/#!/documentation/concepts/Logging
 */

var cluster = require('cluster');
var winston = require('winston');
require('winston-logstash-udp');

module.exports.log = {

  /***************************************************************************
   *                                                                          *
   * Valid `level` configs: i.e. the minimum log level to capture with        *
   * sails.log.*()                                                            *
   *                                                                          *
   * The order of precedence for log levels from lowest to highest is:        *
   * silly, verbose, info, debug, warn, error                                 *
   *                                                                          *
   * You may also set the level to "silent" to suppress all logs.             *
   *                                                                        *
   ***************************************************************************/

  level: 'error',
  colors: false,
  custom: getCustomLogger()
};


function getCustomLogger() {
  var logger = new (winston.Logger)();
  var winstonOptions = {
    level: 'error',
    colorize: true,
    timestamp: false,
    json: false,
    showLevel: false
  };
  var fileOptions = {
    level: 'error',
    colorize: false,
    timestamp: true,
    json: true,
    filename: process.env.FULLPATHLOGFILE || './logs/out.log',
    maxsize: 5120000,
    maxFiles: 3
  };
  var logStashOptions = {
    host: process.env.LOGSTASH_HOST,
    port: process.env.LOGSTASH_PORT,
    appName: 'EMS-WORKER'
  };
  var label = null;
  if (cluster.isMaster) {
    label = 'master';
  } else {
    label = 'worker #' + cluster.worker.id;
  }
  winstonOptions.label = label;
  fileOptions.label = label;
  logStashOptions.label = label;
  logger.add(winston.transports.Console, winstonOptions);
  logger.add(winston.transports.File, fileOptions);
  logger.add(winston.transports.LogstashUDP, logStashOptions);
  return logger;
}
