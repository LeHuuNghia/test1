USE [ActivationDB]
GO

/****** Object:  UserDefinedFunction [dbo].[fn_getEnvironmentSetting]    Script Date: 1/29/2021 1:20:00 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/* Copyright (C) 1995 - 2012 Absolute Software Corp.  All Rights Reserved.
<Name> fn_getEnvironmentSetting </Name>
<Description>
Retrieve the value of a given setting from table EnvironmentSettings
</Description>
<Consumers>   All  </Consumers>
<Enterprise>  Yes  </Enterprise>
<SubSystem> CommonDB </SubSystem>
<CreateOnDatabases> All </CreateOnDatabases>
<Package> Make_ConfigurationBase </Package>
<Version> 2.2 </Version>
<History>
Date          Release  Task # Who Description
===================================================================================
                                  Initial Version
09 July 2009 CommonDB6.4 30239  VT  Increase capacity of outside email addresses in mailbot whitelist 
</History>
*/
CREATE FUNCTION [dbo].[fn_getEnvironmentSetting](@SettingName nvarchar(100)) 
RETURNS nvarchar(Max)
AS
BEGIN
    DECLARE @SettingValue nvarchar(Max);

	SELECT Top 1 @SettingValue = SettingValue 
	FROM CommonConfiguration.EnvironmentSettings With (NoLock) 
	Where SettingName in (@@SERVERNAME+'.'+DB_NAME()+'.'+@SettingName, @@SERVERNAME+'.'+@SettingName, @SettingName)
	order by Len(SettingName) desc;
    
	--RETURN @SettingValue;
	SET @SettingValue = Replace(@SettingValue,'<@@servername>', @@servername);
	--SET @SettingValue = Replace(@SettingValue,'[<@@servername>].', '');

	RETURN @SettingValue


END
GO

EXEC sys.sp_addextendedproperty @name=N'ABS_Consumers', @value=N'All' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'FUNCTION',@level1name=N'fn_getEnvironmentSetting'
GO

EXEC sys.sp_addextendedproperty @name=N'ABS_CreateOnDatabases', @value=N'All' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'FUNCTION',@level1name=N'fn_getEnvironmentSetting'
GO

EXEC sys.sp_addextendedproperty @name=N'ABS_Enterprise', @value=N'Yes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'FUNCTION',@level1name=N'fn_getEnvironmentSetting'
GO

EXEC sys.sp_addextendedproperty @name=N'ABS_History', @value=N'Date          Release  Task # Who Description
===================================================================================
                                  Initial Version
09 July 2009 CommonDB6.4 30239  VT  Increase capacity of outside email addresses in mailbot whitelist ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'FUNCTION',@level1name=N'fn_getEnvironmentSetting'
GO

EXEC sys.sp_addextendedproperty @name=N'ABS_Package', @value=N'Make_ConfigurationBase' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'FUNCTION',@level1name=N'fn_getEnvironmentSetting'
GO

EXEC sys.sp_addextendedproperty @name=N'ABS_SubSystem', @value=N'CommonDB' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'FUNCTION',@level1name=N'fn_getEnvironmentSetting'
GO

EXEC sys.sp_addextendedproperty @name=N'ABS_Version', @value=N'2.2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'FUNCTION',@level1name=N'fn_getEnvironmentSetting'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Retrieve the value of a given setting from table EnvironmentSettings' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'FUNCTION',@level1name=N'fn_getEnvironmentSetting'
GO


