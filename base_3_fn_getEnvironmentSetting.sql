set ansi_nulls on;
set quoted_identifier on;
go
if Object_Id('dbo.fn_getEnvironmentSetting') Is Not Null 
Begin
	Drop Function dbo.fn_getEnvironmentSetting
End

go
/* Copyright (C) 1995 - 2012 Absolute Software Corp.  All Rights Reserved.
<Name> fn_getEnvironmentSetting </Name>
<Description>
Retrieve the value of a given setting from table EnvironmentSettings
</Description>
<Consumers>   All  </Consumers>
<Enterprise>  Yes  </Enterprise>
<SubSystem> CommonDB </SubSystem>
<CreateOnDatabases> All </CreateOnDatabases>
<Package> Make_ConfigurationBase </Package>
<Version> 2.2 </Version>
<History>
Date          Release  Task # Who Description
===================================================================================
                                  Initial Version
09 July 2009 CommonDB6.4 30239  VT  Increase capacity of outside email addresses in mailbot whitelist 
</History>
*/
CREATE FUNCTION dbo.fn_getEnvironmentSetting(@SettingName nvarchar(100)) 
RETURNS nvarchar(Max)
AS
BEGIN
    DECLARE @SettingValue nvarchar(Max);

	SELECT Top 1 @SettingValue = SettingValue 
	FROM CommonConfiguration.EnvironmentSettings With (NoLock) 
	Where SettingName in (@@SERVERNAME+'.'+DB_NAME()+'.'+@SettingName, @@SERVERNAME+'.'+@SettingName, @SettingName)
	order by Len(SettingName) desc;
    RETURN @SettingValue;
END
go

-- All applications will use this function, any users can query a setting.
grant execute on dbo.fn_GetEnvironmentSetting to public;
GO


--Extended Properties----------------------------------------------------------
Exec dbo.dev_AddExtendedPropertiesFromHeader @ObjectName = 'fn_GetEnvironmentSetting';
Go

