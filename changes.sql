
Go
Set ansi_nulls, quoted_identifier On
If Object_Id(N'dbo.util_GetDBPath') Is Not Null
Begin
	Drop Procedure dbo.util_GetDBPath
End

Go

/* Copyright (C)  2010-2012 Absolute Software Corporation,  All Rights Reserved.

<Name> util_GetDBPath </Name>
<Description>

Retrieve the real [server.]dbname for a given logical dbname, for a customer account.
If the account/ESN is not on this server, then call the redirection server to get the mapping

This routine has potential to do an RPC call - keep it outside transactions.
Execution speed and correctness is important for this procedure because its 
called multiple times from many processes.

This maps ccdata, ltrDB, ctdata, activationDB, etc to a physical database
according to settings in MapServer, MapAccount and EnvironmentSettings.

For valid @DBName, see dbo.DatabaseNames_fn().

</Description>

<Consumers>   Many stored procedures  </Consumers>
<Enterprise>  Yes       </Enterprise>
<SubSystem> CommonDB </SubSystem>
<Package> Make_RedirectionBase </Package>
<Version> 2.4 </Version>
<CreateOnDatabases>   all  </CreateOnDatabases>

<History>
Date          Release  Version#		Task #    Who Description
===================================================================================
09 Feb 2007							TMS11343  JEP US Data Center Setup
12 Feb 2007							TMS11368  JEP Use EnvironmentSettings for values
07 Mar 2007							TMS11538  JEP Add ESN as parameter, call activationDB if not found
							         change to util_GetDBPath instead of getdb Path_fn()
19 Apr 2007							TMS11538	 CRH Add CRM to the mix							         
20 Apr 2007										JEP PWSDB
10 May 2007										GJM If can't find account_id in mapAccount check redirectionDB
16 May 2007							TMS12080  JEP Requesting LTRDB with Account 0 gives back CCData 
                                     after account 0 added to mapaccount
08 Jun 2007							12414     DZ  OBE_insertDuplicateESN is not moving computer to a new Silo because util_GetDBPath doesn't return proper silo for newly created ESN (because of replication)
18 Jun 2007							12481     JEP Add LtrTrialDB as valid database 
05 Jul 2007							12587    JEP  Raise error if silocode for account is null, enhance error handling to include user who calls this procedure
28 May 2009   CommonDB6.4			WI27319 EL Added new logical database names for Monitoring Center 2.0.
09 July 2009  CommonDB6.4			WI30278 VT Add EnterpriseDB to list of DBs for compatibility with Enterprise
12 Jan 2010   Pulsar				22111     JEP Caller should not need explict select permissions on account_license.  
                                     Do not use "Execute As" feature to maintain caller context through different calls.
18 Jan 2010   Pulsar				21161     JEP Null dbname doesn't error out soon enough                                     
22 Jan 2010   CDB Pulsar			31395   JEP Commondb redirection: Util_getDbpathList errors out when no consumer silo defined :  
									util_getdbpathlist - Ignore errors as returned from util_getdbpath
									util_getdbpath - dont use account 12773 for consumer database unless it exists in mapAccount
04 Feb 2010   CDB					22111,34267 JEP util_getdbpath multiple try/catch blocks with xact_abort on generates errors
12 Feb 2010  CDB Pulsar				31395    JEP suppress not found error in being raised in util_getdbpath
25 Mar 2010  CDB6.8							GJM Added suport for 'HHDB' @DBPath
25 May 2010  CDB6.9					34100   VT Change grant to CommonDB_Role instead of Public.
25 May 2010  CDB6.9					35123   VT Add @EnquoteOutputFlag to allow surrounding of output variables with [] (needed for named instance support)
07 Jun 2010  CDB6.9					35168   VT Added handling for CallDataExtDB and CTData_Ext as valid logical/physical DB names
08 Jun 2010  CDB6.9					33749   VT Remove ltrTrialDB from DB names
24 Jun 2010  CDB6.9					35168   VT Added new CallDataExtDB checks under each section
											Removed handling of CTData_Ext as valid DB name - supporting only logical names going forward
09 Dec 2010  CDB6.11				39748   TM Remove PWSDB from DB names 
10-Apr-2012	  CDB6.16	     2.4 			WI44904   TN   Commondb Redirection: Update audit columns to use ORIGINAL_LOGIN() instead of system_user.
02-Feb-2017							ABS-18415	TaiPham	Not update Stolen Flag for moved device across silo
</History>

*/


Create Procedure dbo.util_GetDBPath 
  @DBName     nvarchar(255)                                 -- Logical Database name needed
, @Account_Id int = NULL                                    -- Account Id to find, Null = Unknown account, 0 = All accounts
, @ESN        varchar(20) = NULL
, @DBPath     nvarchar(255) OUTPUT
, @DBPath_Server nvarchar(255) = NULL OUTPUT
, @DBPath_Database nvarchar(255) = NULL OUTPUT
, @NewAccountSiloFlag bit = 0
, @CheckAllSilos bit = 1
, @NullPathAllowedFlag bit = 0   -- don't raise an error if the requested database is not found 
, @EnquoteOutputFlag Bit = 0  --Surround output variables with [] (needed for named instance support)
, @AllowCallRedirectionDB Bit = 1
, @DBPathUserType Varchar(64) = 'SQLInternal'

--WITH EXECUTE AS OWNER

As

Begin

Declare	
	 @RedirectionPath nvarchar(255)
	, @Cmd nvarchar(255)

--
-- Generates the @ErrorVariableXML and settings based on the procedure signature
-- Exec Dev_GetErrorVariablesXML 'util_GetDBPath' 
--
Declare @ErrorVariablesXML nVarChar(max)
Select @ErrorVariablesXML = 
	'<Var N="@DBName" V="' + IsNull(dbo.fn_xmlesc(@DBName), 'NULL') + '"/>' +
	'<Var N="@Account_Id" V="' + IsNull(Convert(VarChar(10), @Account_Id), 'NULL') + '"/>' +
	'<Var N="@ESN" V="' + IsNull(dbo.fn_xmlesc(@ESN), 'NULL') + '"/>' +
	'<Var N="@DBPath" V="' + IsNull(dbo.fn_xmlesc(@DBPath), 'NULL') + '"/>' +
	'<Var N="@DBPath_Server" V="' + IsNull(dbo.fn_xmlesc(@DBPath_Server), 'NULL') + '"/>' +
	'<Var N="@DBPath_Database" V="' + IsNull(dbo.fn_xmlesc(@DBPath_Database), 'NULL') + '"/>' +
	'<Var N="@System_user" V="' + IsNull(dbo.fn_xmlesc(Original_Login()), 'NULL') + '"/>'

--Print @ErrorVariablesXML

--
--  If the @DBName is not in the list of recognized names raise an error
--
If @DBName Is Null Or @DBName Not In (Select DBName from dbo.DatabaseNames_fn() )
 Begin
	-- 	'Invalid value [%s] for [%s].  Expected: [%s]'
 	Exec dbo.RaiseCustomError
 		@MessageID=80017, @ProcID=@@ProcID, @VariablesXML=@ErrorVariablesXML,
 		--@PublicMessageID,
 		@Argument1=@DBName,
 		@Argument2='@DBName',
 		@Argument3='Valid database name.'
 		--,@WriteToLog
	Return (-1)

End


--
-- If blank or set parameter passed, need to reset it to invalid value
--
Set @DBPath = Null

--
--  Databases which are not directed by account can be checked for in environmentSettings before checking
--  mapping tables to reduce the amount of I/O this function takes
--
If @DBName In ( N'EnterpriseDB', N'trmsdb', N'oridb', N'mailbotdb', N'ArchiveQuery', N'CRM',  N'emsdb', 
				N'RecoveryDB', N'OrderDB', N'RedirectionDB', N'ArchiveQueryDB', N'UserDB', N'HedgeHogDB') -- New logical database names
Begin
	Set @DBPath = dbo.fn_getEnvironmentSetting(@dbname)

	If @DBPath Is Null 
	Begin
		-- 	'Invalid value [%s] for [%s].  Expected: [%s]'
 		Exec dbo.RaiseCustomError
 			@MessageID=80017, @ProcID=@@ProcID, @VariablesXML=@ErrorVariablesXML,
 			--@PublicMessageID,
 			@Argument1='Null',
 			@Argument2='DBPATH',
 			@Argument3='Valid environment setting'
 			--,@WriteToLog
		Return (-1)
	End

End

--
-- An account id or esn is needed for databases with multiple instances
--
If @DBPath Is Null
Begin
	If @Account_ID Is Null And @ESN Is Null and @NewAccountSiloFlag <> 1
	Begin
		-- 	'Invalid value [%s] for [%s].  Expected: [%s]'
 		Exec dbo.RaiseCustomError
 			@MessageID=80017, @ProcID=@@ProcID, @VariablesXML=@ErrorVariablesXML,
 			--@PublicMessageID,
 			@Argument1='Null',
 			@Argument2='account_id, ESN, NewAccountSiloFlag',
 			@Argument3='Account or ESN must be passed if NewSiloFlag is not 1.'
 			--,@WriteToLog
		Return (-1)

	End
End


--
--  If only an ESN is passed,  try and find the account_id, if on a database which supports esn redirection. 
--
If @DBPath Is Null
Begin
	If @Account_ID Is Null And @ESN Is Not Null 
	Begin		
		-- Only get account_id by ESN on RedirectionDB
		Set @RedirectionPath = dbo.fn_getEnvironmentSetting('RedirectionDB') 
		
		
		--select @RedirectionPath,Object_id('dbo.account_license'),@RedirectionPath
		--,Left(@RedirectionPath,  Left(@RedirectionPath, (Case When charIndex('.', @RedirectionPath) > 1 Then charIndex('.', @RedirectionPath)-1 Else 0 End ))--, Substring(@RedirectionPath, (Case When charIndex('.', @RedirectionPath) > 1 Then charIndex('.', @RedirectionPath)+1 Else 1 End ), 255)
		
		If  Object_id('dbo.account_license') Is Not Null AND 		
		( @RedirectionPath Is Not Null 
		--Tony Sun And	( Left(@RedirectionPath, (Case When charIndex('.', @RedirectionPath) > 1 Then charIndex('.', @RedirectionPath)-1 Else 0 End )) = @@SERVERNAME 
		And	( Replace(Replace(Left(@RedirectionPath, (Case When charIndex('.', @RedirectionPath) > 1 Then charIndex('.', @RedirectionPath)-1 Else 0 End )),'[',''),']','') = @@SERVERNAME 
			AND Substring(@RedirectionPath, (Case When charIndex('.', @RedirectionPath) > 1 Then charIndex('.', @RedirectionPath)+1 Else 1 End ), 255) = DB_NAME()
		))	
		Begin
		
			-- This format has different(more efficient) execution plan rather than using Set @Account_Id format			
			Select Top 1 @Account_Id = AL.account_Id
			From dbo.Account_License AL With (Nolock)
			Where AL.ESN = @ESN
			
		End			
	End
End



--
--  Special cases -- New account silo, Ltr datatabases
--
If @DBPath Is Null
Begin
	--   If we are looking were to create a new account - then DONT look for account 0 location
	If IsNull(@NewAccountSiloFlag,0) = 1 And ( @Account_Id = 0 or @Account_Id Is Null )
	Begin
	
	
	
		If @DBName In ('CTData', 'LTRCTData',
						'CallDataDB', 'ConsumerCallDataDB') -- New logical database names
		Begin
		
		
		
			Select @DBPath = Rtrim(IsNull(MS.Server + '.', ''))  +  Rtrim(Isnull(MS.DBName, '' ))
				 , @DBPath_Server = IsNull(MS.Server, '')
				 , @DBPath_Database = IsNull(MS.DBName, '')
			From dbo.MapSilo Silo With (Nolock) 
				Inner Join dbo.MapServer MS (Nolock) on Silo.CallDataServerId = MS.MapServerId
			Where 
				Silo.DefaultNewSiloFlag = 1
		End
		Else If @DBName in ('CallDataExtDB') -- New logical database names
		Begin
			Select Distinct @DBPath = Rtrim(IsNull(MS.Server + '.', ''))  +  Rtrim(Isnull(MS.DBName, '' ))
				 , @DBPath_Server = IsNull(MS.Server, '')
				 , @DBPath_Database = IsNull(MS.DBName, '')
  		      From dbo.MapSilo Silo With (Nolock) 
			 Inner Join dbo.MapServer MS (Nolock) on Silo.SiloCode = MS.SiloCode
			   And MS.DBClassInd = 'CTX'
               And MS.DBType = 2
			Where 
				Silo.DefaultNewSiloFlag = 1
        End
		Else If @DBName in ('LtrDB', 'CCData',
							'ConsumerDB', 'CorporateDB') -- New logical database names
		Begin
			Select @DBPath = Rtrim(IsNull(MS.Server + '.', ''))  +  Rtrim(Isnull(MS.DBName, '' ))
				 , @DBPath_Server = IsNull(MS.Server, '')
				 , @DBPath_Database = IsNull(MS.DBName, '')
			From dbo.MapSilo Silo With (Nolock) 
				Inner Join dbo.MapServer MS (Nolock) on Silo.CCDataServerId = MS.MapServerId
			Where 
				Silo.DefaultNewSiloFlag = 1
		End
		Else
		Begin
			Select @DBPath = NULL
				 , @DBPath_Server = NULL
				 , @DBPath_Database = NULL
		End
	End

	-- Use the default LTR account id to find the correct set of databases
	-- Currently only one LoJack database - so if we are looking for ltrdb/ltrctdata for any loJack account 
	-- we can find it as the default
	-- TODO: Multiple LoJack Silos will need calls with account_id = 0 reviewed and this case removed from this procedure
	If @DBName In ( 'Ltrctdata', 'ltrdb',
					'ConsumerCallDataDB', 'ConsumerDB' ) and (@Account_Id Is Null or @Account_Id = 0)
	Begin
		If Exists ( Select 1 from dbo.MapAccount MA With (Nolock) where MA.Account_Id = 12773 )
		Begin
			Set @Account_Id = 12773
		End
	End

End


--
-- Either Account Id is passed or the account for the ESN was found on this silo
--  Check if a specific server is specified for this account
--
If @DBPath Is Null
Begin

	If @Account_Id Is Not Null
	Begin
		-- the account is setup in MapAccount - but for some reason it is not assigned to a silo  ( manual update, 
		If Exists ( Select 1 from dbo.MapAccount MA (Nolock)  where MA.SiloCode Is Null  and MA.account_id = @Account_Id)
		Begin
		-- 	'Invalid value [%s] for [%s].  Expected: [%s]'
			Exec dbo.RaiseCustomError
			@MessageID=80017, @ProcID=@@ProcID, @VariablesXML=@ErrorVariablesXML,
			--@PublicMessageID,
			@Argument1='Null',
			@Argument2='MapAccount.SiloCode',
			@Argument3='CDEF,LDEF, etc'
			--,@WriteToLog
			Return (-1)
		End

		
		Set @DBPath = 
			Case When @DBName In ('CTData', 'ltrctdata',
									'CallDataDB', 'ConsumerCallDataDB') Then
			( Select		Rtrim(IsNull(MS.Server + '.', ''))  +  Rtrim(Isnull(MS.DBName, '' ))
				from	dbo.MapAccount MA (Nolock) 
				Inner Join dbo.MapSilo Silo With (Nolock) on MA.SiloCode = Silo.SiloCode
				Inner Join dbo.MapServer MS (Nolock) on Silo.CallDataServerId = MS.MapServerId
				Where	MA.Account_Id = @Account_Id
			)
			When @DBName in ('CallDataExtDB') Then
			(
			 Select	Distinct Rtrim(IsNull(MS.Server + '.', ''))  +  Rtrim(Isnull(MS.DBName, '' ))
				from	dbo.MapAccount MA (Nolock) 
				Inner Join dbo.MapSilo Silo With (Nolock) on MA.SiloCode = Silo.SiloCode
				Inner Join dbo.MapServer MS (Nolock) on Silo.SiloCode = MS.SiloCode
                  And MS.DBClassInd = 'CTX'
                  And MS.DBType = 2
				Where	MA.Account_Id = @Account_Id
			)
			When @DBName in ( 'CCData', 'ltrdb',
								'CorporateDB', 'ConsumerDB') Then
			(
			 Select		Rtrim(IsNull(MS.Server + '.', ''))  +  Rtrim(Isnull(MS.DBName, '' ))
				from	dbo.MapAccount MA (Nolock) 
				Inner Join dbo.MapSilo Silo With (Nolock) on MA.SiloCode = Silo.SiloCode
				Inner Join dbo.MapServer MS (Nolock) on Silo.CCDataServerId = MS.MapServerId
				Where	MA.Account_Id = @Account_Id
			)
			Else
				Null
			End
			
			If IsNull(@DBPathUserType,'') = 'SQLInternal'
			Begin
				--TODO :Add this line to fix bug  	
				Set @DBPath = Replace(@DBPath,dbo.fn_getEnvironmentSetting('RDS_ENDPOINTNAME'),'[' + @@ServerName + ']')
			End
			
	End
End


--
-- Esn not in account_license or account_id not in mapAccount call the redirection database to try and find account/esn
--
If @DBPath Is Null AND @AllowCallRedirectionDB = 1
Begin
	Set @RedirectionPath = dbo.fn_getEnvironmentSetting('RedirectionDB') 
	
	--print('TODO: Add this line')
	--set @RedirectionPath = REPLACE(REPLACE(@RedirectionPath,'[',''),']','') 

	--select @RedirectionPath
	
	If @RedirectionPath Is Not Null And
		--
		-- Redirection is to a different database than the current database
		-- Otherwise we are running this check from an instance of util_GetDBPath on a different ctdata/ccdata
		--		
		--Tony Sun RDS ( Left(@RedirectionPath, (Case When charIndex('.', @RedirectionPath) > 1 Then charIndex('.', @RedirectionPath)-1 Else 0 End )) <> @@SERVERNAME Or
		(Replace(Replace( Left(@RedirectionPath, (Case When charIndex('.', @RedirectionPath) > 1 Then charIndex('.', @RedirectionPath)-1 Else 0 End )),'[',''),']','') <> @@SERVERNAME Or
		   Substring(@RedirectionPath, (Case When charIndex('.', @RedirectionPath) > 1 Then charIndex('.', @RedirectionPath)+1 Else 1 End ), 255) <> DB_NAME()
		)
	Begin
		
		
		Set @Cmd = @RedirectionPath +'.dbo.util_GetDBPath'
		If @Cmd Is Not Null
		Begin
			
			--select @dbname,@Account_Id,@esn,@DBPath,@NewAccountSiloFlag,@NullPathAllowedFlag
			
			Exec @Cmd @dbname = @dbname,  @Account_Id = @Account_Id, @ESN = @ESN, @DBPath = @DBPath OUTPUT
						, @NewAccountSiloFlag = @NewAccountSiloFlag, @NullPathAllowedFlag = @NullPathAllowedFlag
			
			
			Set @DBPath_Server =  Left(@DBPath, (Case When charIndex('.', @DBPath) > 1 Then charIndex('.', @DBPath)-1 Else 0 End )) 
			Set @DBPath_Database =  Substring(@DBPath, (Case When charIndex('.', @DBPath) > 1 Then charIndex('.', @DBPath)+1 Else 1 End ), 255)
			
			Return(1)
		End
	End
End


--
-- Didn't find the account setup in MapAccount, check for a New accounts Silo flag
--
If @DBPath Is NULL 
Begin
	If IsNull(@NewAccountSiloFlag,0) = 1
	Begin
		If @DBName In ('CTData', 'LTRCTData',
						'CallDataDB', 'ConsumerCallDataDB')
		Begin
			Select @DBPath = Rtrim(IsNull(MS.Server + '.', ''))  +  Rtrim(Isnull(MS.DBName, '' ))
				 , @DBPath_Server = IsNull(MS.Server, '')
				 , @DBPath_Database = IsNull(MS.DBName, '')
			From dbo.MapSilo Silo With (Nolock) 
				Inner Join dbo.MapServer MS (Nolock) on Silo.CallDataServerId = MS.MapServerId
			Where 
				Silo.DefaultNewSiloFlag = 1
			
			
		End
		Else If @DBName in ( 'CallDataExtDB')
		Begin
			Select Distinct @DBPath = Rtrim(IsNull(MS.Server + '.', ''))  +  Rtrim(Isnull(MS.DBName, '' ))
				 , @DBPath_Server = IsNull(MS.Server, '')
				 , @DBPath_Database = IsNull(MS.DBName, '')
			From dbo.MapSilo Silo With (Nolock) 
				--Inner Join dbo.MapServer MS (Nolock) on Silo.CCDataServerId = MS.MapServerId
			Inner Join dbo.MapServer MS (Nolock) on Silo.SiloCode = MS.SiloCode
              And MS.DBClassInd = 'CTX'
              And MS.DBType = 2
			Where 
				Silo.DefaultNewSiloFlag = 1
		End		
		Else If @DBName in ( 'LtrDB', 'CCData',
							'ConsumerDB', 'CorporateDB')
		Begin
		
		
			Select @DBPath = Rtrim(IsNull(MS.Server + '.', ''))  +  Rtrim(Isnull(MS.DBName, '' ))
				 , @DBPath_Server = IsNull(MS.Server, '')
				 , @DBPath_Database = IsNull(MS.DBName, '')
			From dbo.MapSilo Silo With (Nolock) 
				Inner Join dbo.MapServer MS (Nolock) on Silo.CCDataServerId = MS.MapServerId
			Where 
				Silo.DefaultNewSiloFlag = 1
				
				
		End
		Else
		Begin
			Select @DBPath = NULL
				 , @DBPath_Server = NULL
				 , @DBPath_Database = NULL
		End
		
		If IsNull(@DBPathUserType,'') = 'SQLInternal'
		Begin
			--TODO :Add this line to fix bug  	
			Set @DBPath = Replace(@DBPath,dbo.fn_getEnvironmentSetting('RDS_ENDPOINTNAME'),'[' + @@ServerName + ']')
		End
		
	End
End

If @DBPath Is NULL and @CheckAllSilos = 1
Begin

	If @DBName In ('CTData', 'LTRCTData',
	               'CallDataExtDB',
					'CallDataDB', 'ConsumerCallDataDB')
	Begin
		Declare @IdTbl Table ( SiloCode char(4) , Counter INT identity(1,1) )
		Declare @Counter integer, @SiloCode char(4)
		
		Insert Into @IdTbl 
		Select SiloCode from dbo.MapSilo Silo With (Nolock)  
		
		Set @Counter = 1
		
		While Exists ( Select 1 from @IdTbl where Counter =  @Counter)
		Begin 
			
			Set @SiloCode = (Select SiloCode from @IdTbl where Counter =  @Counter)
			
			Select @DBPath = Rtrim(IsNull(MS.Server + '.', ''))  +  Rtrim(Isnull(MS.DBName, '' ))
				 , @DBPath_Server = IsNull(MS.Server, '')
				 , @DBPath_Database = IsNull(MS.DBName, '')
			From dbo.MapSilo Silo With (Nolock) 
				Inner Join dbo.MapServer MS (Nolock) on Silo.CallDataServerId = MS.MapServerId
				and Silo.SiloCode = @SiloCode
						
			If IsNull(@DBPathUserType,'') = 'SQLInternal'
			Begin
				--TODO :Add this line to fix bug  	
				Set @DBPath = Replace(@DBPath,dbo.fn_getEnvironmentSetting('RDS_ENDPOINTNAME'),'[' + @@ServerName + ']')
			End
						
			Set @Cmd =  @DBPath +'.dbo.util_GetDBPath'
			If @Cmd Is Not Null
			Begin
			
			
				Exec @Cmd @dbname = @dbname,  @Account_Id = @Account_Id, @ESN = @ESN, @DBPath = @DBPath OUTPUT
				, @NewAccountSiloFlag = @NewAccountSiloFlag,  @CheckAllSilos=0, @NullPathAllowedFlag = @NullPathAllowedFlag, @AllowCallRedirectionDB = 0
			
			
				if @DBPath is not null
				Begin
					Set @DBPath_Server =  Left(@DBPath, (Case When charIndex('.', @DBPath) > 1 Then charIndex('.', @DBPath)-1 Else 0 End )) 
					Set @DBPath_Database =  Substring(@DBPath, (Case When charIndex('.', @DBPath) > 1 Then charIndex('.', @DBPath)+1 Else 1 End ), 255)
					Return(1)
				end
			End
			
			Set @Counter= @Counter + 1
		End		

	end

End



IF @DBPath Is Null
Begin
	Set @DBPath = 
		Case When @DBName In ('CTData', 'LTRCTData',
								'CallDataDB', 'ConsumerCallDataDB') Then
		 (Select	Rtrim(IsNull(MS.Server + '.', ''))  +  Rtrim(Isnull(MS.DBName, '' ))
			from	dbo.MapServer MS (Nolock) 
			Where	MS.DefaultServerFlag = 1
			And		MS.DBClassInd = 'CT'
		 )
		 When @DBName In ('CallDataExtDB') Then
		 (Select	Rtrim(IsNull(MS.Server + '.', ''))  +  Rtrim(Isnull(MS.DBName, '' ))
			from	dbo.MapServer MS (Nolock) 
			Where	MS.DefaultServerFlag = 1
			And		MS.DBClassInd = 'CTX'
		 )
		When @DBName in ( 'CCData',
							'CorporateDB') Then
		 (Select	Rtrim(IsNull(MS.Server + '.', ''))  +  Rtrim(Isnull(MS.DBName, '' ))
			from	dbo.MapServer MS (Nolock) 
			Where	MS.DefaultServerFlag = 1
			And		MS.DBClassInd = 'CC'
		  )
		When @DBName in ( 'LtrDB',
							'ConsumerDB') Then
		 (Select	Rtrim(IsNull(MS.Server + '.', ''))  +  Rtrim(Isnull(MS.DBName, '' ))
			from	dbo.MapServer MS (Nolock) 
			Where	MS.DefaultServerFlag = 1
			And		MS.DBClassInd = 'L4L'
		  )
		Else
			Null
		End
		
		If IsNull(@DBPathUserType,'') = 'SQLInternal'
		Begin
			--TODO :Add this line to fix bug  	
			Set @DBPath = Replace(@DBPath,dbo.fn_getEnvironmentSetting('RDS_ENDPOINTNAME'),'[' + @@ServerName + ']')
		End		
End


--
-- If a MapServer setting was not found for the database, look to EnvironmentSettings
--
If @DBPath Is Null
Begin
	Set @DBPath = dbo.fn_getEnvironmentSetting(@dbname)
End


--
-- If the EnvironmentSetting is not set and the database name exists on this server then just use the database name
-- This is a probable path for Enterprise
--
If @DBPath Is Null 
Begin
	If DB_ID(@DbName) Is Not Null
	Begin
		Set @DBPath = @DbName
	End
End



--
-- Null is not an acceptable return value
--   
--
If @DBPath Is Null And @NullPathAllowedFlag = 0
Begin
	-- 	'Invalid value [%s] for [%s].  Expected: [%s]'
 	Exec dbo.RaiseCustomError
 		@MessageID=80017, @ProcID=@@ProcID, @VariablesXML=@ErrorVariablesXML,
 		--@PublicMessageID,
 		@Argument1='Null', @Argument2='@DBPath', @Argument3 = 'Not Null'
 		--,@WriteToLog
	
	Set @DBPath = Isnull(@DBPath, '')
	
	Return (-1)


End

--
--	Parse ServerName and DatabaseName from DBPAth
--
Set @DBPath_Server =  Left(@DBPath, (Case When charIndex('.', @DBPath) > 1 Then charIndex('.', @DBPath)-1 Else 0 End )) 
Set @DBPath_Database =  Substring(@DBPath, (Case When charIndex('.', @DBPath) > 1 Then charIndex('.', @DBPath)+1 Else 1 End ), 255)

--
-- Surround output variables with [] if requested
--
If IsNull(@EnquoteOutputFlag, 0) = 1
Begin
   Set @DBPath =  dbo.Enquote_fn(@DBPath)
   Set @DBPath_Server = dbo.Enquote_fn(@DBPath_Server)
   Set @DBPath_Database = dbo.Enquote_fn(@DBPath_Database)
End


If IsNull(@DBPathUserType,'') = 'SQLInternal'
Begin
	--TODO :Add this line to fix bug  	
	Set @DBPath = Replace(@DBPath,dbo.fn_getEnvironmentSetting('RDS_ENDPOINTNAME'),'[' + @@ServerName + ']')
End

return	(1)

end
go

-- all applications will need this function
grant execute on dbo.util_GetDBPath to CommonDB_Role
go

Exec dbo.dev_AddExtendedPropertiesFromHeader @ObjectName = N'util_GetDBPath', @Debug = 0

GO

