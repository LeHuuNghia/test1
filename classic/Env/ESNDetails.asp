<!-- #include file = "TopInclude2.asp" -->
<!-- #include file = "adovbs.inc" -->
<!-- #include file = "conn_exp.asp" -->
<!-- #include file = "includes/constants.asp" -->
<!-- #include file = "includes/Utilities.asp" -->
<!-- #include file = "includes/Actions.asp" -->
<!-- #include file = "includes/FastContact.asp" -->
<!-- #include file = "includes/Fips.asp" -->
<!-- #include file = "includes/CRCATPhase2Rem.asp" -->
<!-- #include file = "includes/SmartCall.asp" -->
<!-- #include file = "includes/DeviceLockOfflineFreeze.asp" -->

<!-- #include file = "includes/LimitedAccess.asp" -->

<%
'file: ESNDetails.asp
'use: This file is used to view a list of all of the ESNs associated with an order

Dim strEsn, strIMEI, strDeviceUid, intOrderId, intAccountId, intDspAccountId, strAccountName, strStatus, intTestInd, intStolenFlag, bStolen, bAlert, AccountUID, strProc
Dim intIPPeriodDays, intIPPeriodMinutes, intAction, strNewEsn
Dim strNewUrl, strNewIpAddress, intNewIpPort, strProduct, strItemNo, dtLastCallTS, intVersionNumber, dtLastUpdateDate
Dim strLastUpdateUser, bCtcFlag, dtCreationDate, strSerial, strAssetNumber, strMake, strModel, strUserName
Dim strHddSerialNumber0, strBiosEnabled, strMessage, intIndex, intTemp
Dim strIP, strFileLogPath, strDetails, strExpectedDDStatusCode, intSilentMode, strSilentErrorMsg
Dim rs, rs2, rs3, cmd, intReturnCode
Dim strDefConnString, strCCConnString, strCTConnString
Dim cnnDef, cnnCC, cnnCT
Dim strMoveStatus, dtCreatedTS, strCreatedBy
Dim licenseChecksum
Dim bFastContactAvailable,bEnableFcFlag, bEnableFastContact, intFcPeriodSeconds, strNewFcUrl, strNewFcIpAddress, intNewFcIpPort
Dim	intSCPeriodTime, bEnableSCFlag,  bEnableSCLocation, bEnableSCSoftware, bEnableSCLoggedOn, bEnableSCHardware, bEnableSCNetwork, intBits, bSCSupportOSFlag
Dim bEnableFastContactOriginal, intOnlineStatus, strTimestamp
Dim intCRCATPhase2Rem
Dim strExpectedDDStatusCode2
Dim resp, intResp, strXmlParameters
Dim bOfflineFreeze
dim  strCallDataServer, strCCDataServer, strSiloName, intCallDataMapServerId, intCCDataMapServerId
dim cnnRdir, strRdirConnString, dataCenter

Dim test_ctcflag, test_return 

intAccountID = NULL
strESN = NULL
intBits = 0
bSCSupportOSFlag = 0
bEnableFcFlag = 0
intSCPeriodTime = CONST_MINIMUM_PERIOD_TIME
bEnableSCLocation = 0
bEnableSCSoftware = 0
bEnableSCLoggedOn = 0
bEnableSCHardware = 0
bEnableSCNetwork = 0
bOfflineFreeze = true 	


'get AccountID from query string - used when AccountID comes from other pages
if Request.QueryString("AccID") <> "" then
	intAccountId = Request.QueryString("AccID")

'or get AccountID from form - used when AccountID comes from an update from this page
elseif Request.form("AccountID") <> "" then
	intAccountId = Request.form("AccountID")
end if

if Request.QueryString("ESN") <> "" then
	strESN = trim(Request.QueryString("ESN"))
elseif Request.Form("ESN") <> "" then
	strESN = trim(Request.Form("ESN"))
else
	response.redirect "NoSearchResult.asp"
end if

if Request.QueryString("IMEI") <> "" then
	strIMEI = trim(Request.QueryString("IMEI"))
else
    strIMEI = "N/A"
end if

debug "intAccountID", intAccountID
debug "strESN", strESN

' DD Request status must be in the Launched (DDEL) state before it can be allowed
' to go to the Completed (DONE) state.
strExpectedDDStatusCode = "DDEL"
'DD Request status can be in "PEND" before it can set to complete
strExpectedDDStatusCode2 = "PEND"

intSilentMode = 1
strMessage = ""
strIP = "192.168.0.1"
strFileLogPath = NULL
strDetails = "Request forced Complete by Absolute Tech Support."

Set rs = Server.CreateObject("ADODB.RecordSet")
Set rs2 = Server.CreateObject("ADODB.RecordSet")
Set rs3 = Server.CreateObject("ADODB.RecordSet")

' Set up database connections
Set cnnDef = Server.CreateObject("ADODB.Connection")
Set cnnCC = Server.CreateObject("ADODB.Connection")
Set cnnCT = Server.CreateObject("ADODB.Connection")
Set cnnRdir = Server.CreateObject("ADODB.Connection")

cnnDef.ConnectionTimeout = 120
cnnCC.ConnectionTimeout = 120
cnnCT.ConnectionTimeout = 120
cnnRdir.ConnectionTimeout = 120

strDefConnString = GetDefaultDBConnectionString()

' Always pass a NULL Account ID when an ESN is defined.
strCCConnString = GetCCDataConnectionString(NULL, strESN)
strCTConnString = GetCTDataConnectionString(NULL, strESN)
strRdirConnString = GetRedirectionDBConnectionString()

Debug "GetDefaultDBConnectionString()", strDefConnString
Debug "GetCCDataConnectionString()", strCCConnString
Debug "GetCTDataConnectionString()", strCTConnString


Call cnnDef.Open(strDefConnString)
Call cnnCC.Open(strCCConnString)
Call cnnCT.Open(strCTConnString)
Call cnnRdir.Open(strRdirConnString)

Set cmd = Server.CreateObject("ADODB.Command")
With cmd
	.CommandTimeout = 120
	.CommandType = adCmdStoredProc
	.CommandText = "dbo.ems_confirmAccess"
	.NamedParameters = True
	.Parameters.Append .CreateParameter("@ReturnValue", adInteger, adParamReturnValue)
	.Parameters.Append .CreateParameter("@AccountId", adInteger, adParamInput, 255, intAccountId)
	.Parameters.Append .CreateParameter("@AmsUser", adVarWChar, adParamInput, 50, session("ssl_login"))
	.Parameters.Append .CreateParameter("@pageName", adVarWChar, adParamInput, 255, getCurrentASPPageName())

	Set .ActiveConnection = cnnDef
	rs.CursorLocation = adUseClient

	Call rs.Open(cmd, , adOpenForwardOnly, adLockReadOnly)
	intReturnCode = .Parameters("@ReturnValue").Value
	Set .ActiveConnection = Nothing
End With

If rs.State = adStateOpen Then
	rs.Close
End If

Select Case intReturnCode
	Case 1: 'Success
		'do nothing
	Case Else: 'access denied
		set rs = nothing
		Response.Redirect "NoAccess.asp"
End Select


if Request.Form("DeleteInventoryRecordFlag") = "1" then
	Set cmd = Server.CreateObject("ADODB.Command")
	With cmd
		.CommandTimeout = 120
		.CommandType = adCmdStoredProc
		.CommandText = "dbo.ems_deleteComputer"
		.NamedParameters = True
		.Parameters.Append .CreateParameter("@ReturnValue", adInteger, adParamReturnValue)
		.Parameters.Append .CreateParameter("@esn", adVarChar, adParamInput, 20, strEsn)
		.Parameters.Append .CreateParameter("@last_update_user", adVarWChar, adParamInput, 30, session("ssl_login"))
		Set .ActiveConnection = cnnCT
		rs.CursorLocation = adUseClient

		Call rs.Open(cmd, , adOpenForwardOnly, adLockReadOnly)
		intReturnCode = .Parameters("@ReturnValue").Value
		Set .ActiveConnection = Nothing
	End With

	Select Case intReturnCode
		Case 1: 'Success
			strMessage = strMessage & "<font class=success>Inventory record deleted.</font><br>"
            If IsMobileDevice(strEsn) = true Then
                'Tell CTM Server to set the record as "Deleted" (only on success of computer table delete)
			    strXmlParameters = MakeXmlTagValue("Esn", strEsn)
			    intResp = CallSoap11WebService(strCTMDeviceAdminWebServiceURL, "http://ctmobile.absolute.com/IDeviceAdmin/", "http://ctmobile.absolute.com", "DeleteDeviceRecordByESN", strXmlParameters, resp)
                Select Case intResp
                    Case 200: 'Success
                        strMessage = strMessage & "<font class=success>CTM Server record marked as deleted.</font><br>"
                    Case Else: 'Error
                        strMessage = strMessage & "<font class=success>CTM Server record delete failed: ("& intResp &") </font><br>"
                End Select
            End If
		Case -1 'Error
			strMessage = strMessage & "<font class=failure>" & rs("err_desc") & "</font><br>"
		Case Else: 'Unknown failure
			strMessage = strMessage & "The operation [ems_deleteComputer] failed unexpectedly ("& intReturnCode &").  Please contact website administrator"
	End Select

	If rs.State = adStateOpen Then
		rs.Close
	End If
	
	session("msg") =  strMessage
	Response.Redirect "ESNDetails.asp?ESN=" + strESN
	
end if

if Request.Form("UpdateStolenFlag") = "1" and Session("EnterpriseType") = "Partner" then

    Response.Write("<br />Update Stolen Flag<br />")

	if Request.Form("StolenFlag") = "" then
		intStolenFlag = 0
	else
		intStolenFlag = 1
	end if

	Response.Write("<br />Request.Form(""StolenFlag""): " & Request.Form("StolenFlag"))
	Response.Write("<br />intStolenFlag: " & intStolenFlag)

	Set cmd = Server.CreateObject("ADODB.Command")
	With cmd
		.CommandTimeout = 120
		.CommandType = adCmdStoredProc
		.CommandText = "dbo.ems_setStolenFlag"
		.NamedParameters = True
		.Parameters.Append .CreateParameter("@ReturnValue", adInteger, adParamReturnValue)
		.Parameters.Append .CreateParameter("@esn", adVarChar, adParamInput, 20, strEsn)
		.Parameters.Append .CreateParameter("@ams_login", adVarWChar, adParamInput, 50, session("ssl_login"))
		.Parameters.Append .CreateParameter("@StolenFlag", adInteger, adParamInput, , intStolenFlag)
		Set .ActiveConnection = cnnCT
		rs.CursorLocation = adUseClient

		Call rs.Open(cmd, , adOpenForwardOnly, adLockReadOnly)
		intReturnCode = .Parameters("@ReturnValue").Value
		Set .ActiveConnection = Nothing
	End With

	If rs.State = adStateOpen Then
		rs.Close
	End If

    response.Write("<br />intReturnCode: " & intReturnCode)
    Response.Write("<br />")

	Select Case intReturnCode
		Case 1: 'Success
			'do nothing
		Case -1 'Error
			strMessage = strMessage & "<font class=""failure"">ESN not found in License table.</font><br>"
		Case -2 'Error
			strMessage = strMessage & "<font class=""failure"">ESN is Disabled. Cannot set stolen flag.</font><br>"
		Case -3 'Error
			strMessage = strMessage & "<font class=""failure"">You are not allowed to change the stolen flag.</font><br>"
		Case -4 'Error
			strMessage = strMessage & "<font class=""failure"">Update to License table failed.</font><br>"
		Case -5 'Error
			strMessage = strMessage & "<font class=""failure"">StolenFlag can only be 1 or 0.</font><br>"
		Case Else: 'Unknown failure
			strMessage = strMessage & "The operation [ems_setStolenFlag] failed unexpectedly ("& intReturnCode &").  Please contact website administrator"
	End Select

end if

if Request.Form("SetDdToCompletedFlag") = "1" then

    ' Validate the ESN for Data Delete operation.
    Set cmd = Server.CreateObject("ADODB.Command")
    With cmd
	    .CommandTimeout = 120
	    .CommandType = adCmdStoredProc
	    .CommandText = "dbo.adm_DDValidateESN"
	    .NamedParameters = True
	    .Parameters.Append .CreateParameter("@ReturnValue", adInteger, adParamReturnValue)
	    .Parameters.Append .CreateParameter("@esn", adVarChar, adParamInput, 20, strEsn)
	    .Parameters.Append .CreateParameter("@ExpectedDDStatusCode", adVarChar, adParamInput, 4, strExpectedDDStatusCode)
	    .Parameters.Append .CreateParameter("@ExpectedDDStatusCode2", adVarChar, adParamInput, 4, strExpectedDDStatusCode2)
	    .Parameters.Append .CreateParameter("@SilentMode", adInteger, adParamInput, , intSilentMode)
	    .Parameters.Append .CreateParameter("@SilentError_out", adVarWChar, adParamOutput, 2000, strSilentErrorMsg)
	    Set .ActiveConnection = cnnCC
	    rs.CursorLocation = adUseClient

	    Call rs.Open(cmd, , adOpenForwardOnly, adLockReadOnly)
	    intReturnCode = .Parameters("@ReturnValue").Value
        strSilentErrorMsg = trim(.Parameters("@SilentError_out").Value)
	    Set .ActiveConnection = Nothing
    End With

    Select Case intReturnCode
	    Case 1: 'Success
		    strMessage = strMessage & "<font class=success>DD Request set to 'Completed'.</font><br>"
	    Case -1 'Error
		    strMessage = strMessage & "<font class=failure>"& strSilentErrorMsg &"</font><br>"
	    Case Else: 'Unknown failure
		    strMessage = strMessage & "The operation [adm_DDValidateESN] failed unexpectedly ("& intReturnCode &").  Please contact website administrator"
    End Select

    If rs.State = adStateOpen Then
	    rs.Close
    End If

    If intReturnCode = 1 Then
        ' Set the DD Request Status to Completed.
        Set cmd = Server.CreateObject("ADODB.Command")
        With cmd
	        .CommandTimeout = 120
	        .CommandType = adCmdStoredProc
	        .CommandText = "dbo.qcts_DDSetESNStatus_Complete"
	        .NamedParameters = True
	        .Parameters.Append .CreateParameter("@ReturnValue", adInteger, adParamReturnValue)
	        .Parameters.Append .CreateParameter("@esn", adVarChar, adParamInput, 20, strEsn)
	        .Parameters.Append .CreateParameter("@IP", adVarWChar, adParamInput, 15, strIP)
	        .Parameters.Append .CreateParameter("@FileLogPath", adVarWChar, adParamInput, 200, strFileLogPath)
	        .Parameters.Append .CreateParameter("@Details", adVarWChar, adParamInput, 2000, strDetails)
	        Set .ActiveConnection = cnnCT
	        rs.CursorLocation = adUseClient

	        Call rs.Open(cmd, , adOpenForwardOnly, adLockReadOnly)
	        intReturnCode = .Parameters("@ReturnValue").Value
	        Set .ActiveConnection = Nothing
        End With

        Select Case intReturnCode
	        Case 1: 'Success
	           'This doesn't need since upper validate stored procedure will display this msg
		       ' strMessage = strMessage & "<font class=success>DD Request set to 'Completed'.</font><br>"
	        Case -1 'Error
		        strMessage = strMessage & "<font class=failure>A general error has occurred.</font><br>"
	        Case Else: 'Unknown failure
		        strMessage = strMessage & "The operation [qcts_DDSetESNStatus_Complete] failed unexpectedly ("& intReturnCode &").  Please contact website administrator"
        End Select

        If rs.State = adStateOpen Then
	        rs.Close
        End If
    End If
    
    'After complete DD request, we can check whehter to call DDResetPerpetual to update flag    
    if Request.Form("SetDdperpetualFlag") = "1" and  intReturnCode = 1 then
      ' Remove perpetualDeleteFlag
        Set cmd = Server.CreateObject("ADODB.Command")
	    With cmd
		    .CommandTimeout = 120
		    .CommandType = adCmdStoredProc
		    .CommandText = "dbo.adm_DDResetPerpetual"
		    .NamedParameters = True
		    .Parameters.Append .CreateParameter("@esn", adVarChar, adParamInput, 20, strEsn)
		    .Parameters.Append .CreateParameter("@ReturnCodeOut", adInteger, adParamOutput)
		    Set .ActiveConnection = cnnCC
	        Call .Execute(, , adExecuteNoRecords)
		    Set .ActiveConnection = Nothing
            intReturnCode = .Parameters("@ReturnCodeOut").Value
	    End With
	    
	    Select Case intReturnCode
	        case 1: 'Success
                strMessage = strMessage & "<font class=success>Successfully reset Perpetual flag.</font><br>"
		    Case else: 'Error
			    strMessage = strMessage & "<font class=failure>Failed to Reset Perpetual flag.</font><br>"
	    End Select
	    
	End if
    
    
    
end if

if Request.Form("RequestForcedCallFlag") = "1" then
    intReturnCode = RequestForcedCallForEsnUsingFastContact(strEsn)

    Select Case intReturnCode
	Case 1: 'Success
		strMessage = strMessage & "<font class=success>Forced call requested.</font><br>"
	Case 0 'Error
		strMessage = strMessage & "<font class=failure>Unable to request a forced call at this time. Please try later.</font><br>"
	Case Else: 'Unknown failure
		strMessage = strMessage & "The operation [RequestForcedCallForEsnUsingFastContact] failed unexpectedly ("& intReturnCode &").  Please contact website administrator"
	End Select
end if

if Request.Form("RequestCTMLogFlag") = "1" then
    returncode = GetOSflagByEsn(strEsn,osflag)
    Set cmd = Server.CreateObject("ADODB.Command")
        With cmd
	        .CommandTimeout = 120
	        .CommandType = adCmdStoredProc
	        .CommandText = "dbo.ems_requestApsLogESN"
	        .NamedParameters = True
	        .Parameters.Append .CreateParameter("@ReturnValue", adInteger, adParamReturnValue)
	        .Parameters.Append .CreateParameter("@ESN", adVarChar, adParamInput, 20, strEsn)
	        .Parameters.Append .CreateParameter("@Feature", adVarWChar, adParamInput, 128, "DeviceLog")
	        .Parameters.Append .CreateParameter("@Comment", adVarWChar, adParamInput, 256, "CTM log requested via EMS")
	        .Parameters.Append .CreateParameter("@TypeDevice", adVarChar, adParamInput, 1, osflag)
        Set .ActiveConnection = cnnRdir
	        Call rs.Open(cmd, , adOpenForwardOnly, adLockReadOnly)
	        intReturnCode = .Parameters("@ReturnValue").Value
	        Set .ActiveConnection = Nothing
        End With

        Select Case intReturnCode
	        Case 1: 'Success
		        strMessage = strMessage & "<font class=success> Successfully requested. </font><br>"
            Case 2: 'Success
		        strMessage = strMessage & "<font class=success>DD Successfully requested.</font><br>"    
	        Case Else 'Error
		        strMessage = strMessage & "<font class=failure>The request failed unexpectedly. Please contact website administrator. </font><br>"
        End Select

        If rs.State = adStateOpen Then
	        rs.Close
        End If
   
End if

' Is the Fast Contact feature available for this account in this environment?
If IsNull(intAccountId) or intAccountId = 0 then
    ' If all we know is the ESN, look up what account it belongs to..
    intReturnCode = GetAccountIdForEsn(strEsn, intAccountId)
End If

bFastContactAvailable = IsFastContactSupportedForAccount(intAccountId)
If bFastContactAvailable = 1 Then
    ' if it's Mobile Device, make Fast Contact Feature unavailable   
    If IsMobileDevice(strEsn) = true Then
        bFastContactAvailable = 0
    End If
   
End If

bSCSupportOSFlag = IsSmartCallSupportedForAccount(intAccountId)
If ( bSCSupportOSFlag = 1) Then
    If IsMobileDevice(strEsn) = true Then    
     bSCSupportOSFlag = 0
    End If
End If

if Request.Form("SaveFlag") = "1" then

	intAction = 0

	'set intAction to the sum of the action flag values by looping through the actionIDs and adding them together
	for intIndex = 1 to Request.Form("ActionID").Count
		intTemp = Request.Form("ActionID")(intIndex)
		intAction = intAction OR intTemp
	next	

    Dim intDisableCmAndLd
    intDisableCmAndLd = Cint(Request.Form("DisableCmAndLd"))
	'When there is a change, current is 1: set bit 31, current is 0: clear bit 31
    If (intDisableCmAndLd = 1) Then 
        intAction = intAction or ACTION_DISABLE_CM_AND_LD
    Else            
        intAction = intAction And (Not ACTION_DISABLE_CM_AND_LD)
    End If             

	if Request.Form("ippdays") = "" then
		intIPPeriodDays = 0
	else
		intIPPeriodDays = Request.Form("ippdays")
	end if

	if Request.Form("ippminutes") = "" then
		intIPPeriodMinutes = 0
	else
		intIPPeriodMinutes = Request.Form("ippminutes")
	end if

	if Request.Form("newip") = "" then
		strNewIPAddress = null
	else
		strNewIPAddress = Request.Form("newip")
	end if

	if Request.Form("newipport") = "" then
		intNewIPPort = null
	else
		intNewIPPort = Request.Form("newipport")
	end if

	if Request.Form("newurl") = "" then
		strNewURL = null
	else
		strNewURL = Request.Form("newurl")
	end if

	if Request.Form("CTCFlag") = "" then
		bCTCFlag = 0
	else
		bCTCFlag = Request.Form("CTCFlag")
	end if

    if Request.Form("licenseChecksum") = "" then
        licenseChecksum = 0
    else
	    licenseChecksum = Request.Form("licenseChecksum")
    end if


	intTestInd = Request.Form("ESN_Type")
	strAssetNumber = Request.Form("assetno")
    
    if (intAction And ACTION_REMOVE_AGENT_FROM_COMPUTER ) = ACTION_REMOVE_AGENT_FROM_COMPUTER Then        
        Dim strOfflineReturn
        strOfflineReturn = ValidateESNIsOfflineFreeze(intAccountId,strEsn)	
        	
		if strOfflineReturn = "true" then
            bOfflineFreeze = false
			strMessage = strMessage & " <font class=failure>Unabe to remove Agent from this device due to it may be frozen by a Device Freeze offline policy.</font><br /> "		
		end if  

    end if

    'ems_updateEsnDetails
    if bOfflineFreeze = true then
	    Set cmd = Server.CreateObject("ADODB.Command")
	    With cmd
		    .CommandTimeout = 120
		    .CommandType = adCmdStoredProc
		    .CommandText = "dbo.ems_updateEsnDetails"
		    .NamedParameters = True
		    .Parameters.Append .CreateParameter("@ReturnValue", adInteger, adParamReturnValue)
		    .Parameters.Append .CreateParameter("@ams_login", adVarWChar, adParamInput, 50, session("ssl_login"))
		    .Parameters.Append .CreateParameter("@esn", adVarChar, adParamInput, 20, strEsn)
		    .Parameters.Append .CreateParameter("@PeriodDays", adInteger, adParamInput, , 0)
		    .Parameters.Append .CreateParameter("@PeriodMinutes", adInteger, adParamInput, , 0)
		    .Parameters.Append .CreateParameter("@IPPeriodDays", adInteger, adParamInput, , intIPPeriodDays)
		    .Parameters.Append .CreateParameter("@IPPeriodMinutes", adInteger, adParamInput, , intIPPeriodMinutes)
		    .Parameters.Append .CreateParameter("@TestInd", adInteger, adParamInput, , intTestInd)
		    .Parameters.Append .CreateParameter("@NewPhoneNumber", adVarWChar, adParamInput, 16, NULL)
		    .Parameters.Append .CreateParameter("@NewIPAddress", adVarWChar, adParamInput, 16, strNewIPAddress)
		    .Parameters.Append .CreateParameter("@NewIPPort", adVarChar, adParamInput, 16, intNewIPPort)
		    .Parameters.Append .CreateParameter("@NewURL", adVarWChar, adParamInput, 64, strNewURL)
		    .Parameters.Append .CreateParameter("@Action", adInteger, adParamInput, , intAction)
		    .Parameters.Append .CreateParameter("@CTCFlag", adBoolean, adParamInput, , bCTCFlag)
		    .Parameters.Append .CreateParameter("@asset_number", adVarWChar, adParamInput, 250, strAssetNumber)
		    .Parameters.Append .CreateParameter("@RowChecksum", adInteger, adParamInput, , licenseChecksum)

		    Set .ActiveConnection = cnnCT
		    rs.CursorLocation = adUseClient

		    Call rs.Open(cmd, , adOpenForwardOnly, adLockReadOnly)
		    intReturnCode = .Parameters("@ReturnValue").Value
		    Set .ActiveConnection = Nothing
	    End With

        Select Case intReturnCode
		    Case 1: 'Success
			    'do nothing
		    Case -1: 'Failure
			    strMessage = strMessage & "The operation [ems_updateEsnDetails] failed: " & rs("err_desc") & "<br>"
		    Case Else: 'Unknown failure
			    strMessage = strMessage & "The operation [ems_updateEsnDetails] failed unexpectedly ("& intReturnCode &").  Please contact website administrator<br>"
	    End Select

	    If rs.State = adStateOpen Then
			    rs.Close
	    End If

        If intReturnCode = 1 and ((intAction And ACTION_REMOVE_AGENT_FROM_COMPUTER ) = ACTION_REMOVE_AGENT_FROM_COMPUTER) Then
            Set cmd = Server.CreateObject("ADODB.Command")
	        With cmd
		        .CommandTimeout = 120
		        .CommandType = adCmdStoredProc
		        .CommandText = "DeviceLock.UpdateOfflineRequestByEsn"
		        .NamedParameters = True
		        .Parameters.Append .CreateParameter("@ReturnValue", adInteger, adParamReturnValue)
		        .Parameters.Append .CreateParameter("@ChangedBy", adVarWChar, adParamInput, 255, session("ssl_login"))
		        .Parameters.Append .CreateParameter("@esn", adVarChar, adParamInput, 20, strEsn)		        
		        .Parameters.Append .CreateParameter("@DeleteAction", adInteger, adParamInput, , 1)		        

		        Set .ActiveConnection = cnnCC
		        rs.CursorLocation = adUseClient

		        Call rs.Open(cmd, , adOpenForwardOnly, adLockReadOnly)
		        intReturnCode = .Parameters("@ReturnValue").Value
		        Set .ActiveConnection = Nothing
	        End With

            Select Case intReturnCode
		        Case 1: 'Success
			        'do nothing
		        Case -1: 'Failure
			        strMessage = strMessage & "The operation [UpdateOfflineRequestByEsn] failed: " & rs("err_desc") & "<br>"
		        Case Else: 'Unknown failure
			        strMessage = strMessage & "The operation [UpdateOfflineRequestByEsn] failed unexpectedly ("& intReturnCode &").  Please contact website administrator<br>"
	        End Select

	        If rs.State = adStateOpen Then
			        rs.Close
	        End If
        End If
   end if



    If request.Form("chkFipsEnabled") <> request.Form("hdnFipsEnabled") Then
         SetFipsEnabledForEsn trim(strEsn), trim(request.form("chkFipsEnabled"))
    End If
    
    If request.Form("chkDNSCacheEnabled") <> request.Form("hdnDNSCacheEnabled") Then
         SetDNSCacheEnabledForEsn trim(strEsn), trim(request.form("chkDNSCacheEnabled"))
    End If

    If request.Form("chkDEATIIEnabled") <> request.Form("hdnDEATIIEnabled") Then
         SetDEATIIEnabledForEsn trim(strEsn), trim(request.form("chkDEATIIEnabled"))
    End If

    if bFastContactAvailable = 1 then
        ' Save Fast Contact parameters
        bEnableFastContactOriginal = bEnableFastContact

        if Request.Form("chkEnableFastContact") = "" then
		    bEnableFastContact = 0
	    else
		    bEnableFastContact = Request.Form("chkEnableFastContact")
	    end if

        if Request.Form("fcPeriod") = "" then
		    intFcPeriodSeconds = 0
	    else
		    intFcPeriodSeconds = Request.Form("fcPeriod")
	    end if

	    if Request.Form("fcNewIp") = "" then
		    strNewFcIpAddress = null
	    else
		    strNewFcIpAddress = Request.Form("fcNewIp")
	    end if

	    if Request.Form("fcNewIpPort") = "" then
		    intNewFcIpPort = null
	    else
		    intNewFcIpPort = Request.Form("fcNewIpPort")
	    end if

	    if Request.Form("fcNewUrl") = "" then
		    strNewFcUrl = null
	    else
		    strNewFcUrl = Request.Form("fcNewUrl")
	    end if

'TODO - What about stolen ESN's - Only allow disable? Don't allow change & assume will be turned off when flagged stolen?

        intReturnCode = SetFastContactSettingsForEsn(strEsn, bEnableFastContact, intFcPeriodSeconds, strNewFcIpAddress, intNewFcIpPort, strNewFcUrl)
        Select Case intReturnCode
		    Case 1: 'Success
			    'do nothing
		    Case -1: 'Failure
			    strMessage = strMessage & "<font class=failure>The operation [SetFastContactSettingsForEsn] failed: " & rs("err_desc") & "</font><br>"
		    Case Else: 'Unknown failure
			    strMessage = strMessage & "<font class=failure>The operation [SetFastContactSettingsForEsn] failed unexpectedly ("& intReturnCode &").  Please contact website administrator</font><br>"
	    End Select

		
        ' If the ESN is active and has FC already enabled, notify it of the change via an FC server
        If Request.Form("EsnStatus") = "A" Then 'TODO - and (bEnableFastContactOriginal = "on" or bEnableFastContactOriginal = "1") then
            intReturnCode = UpdateSettingsOnFastContactServerForEsn(strEsn, bEnableFastContact, intIPPeriodDays, intIPPeriodMinutes, strNewIPAddress, intNewIPPort, strNewURL, intFcPeriodSeconds, strNewFcIpAddress, intNewFcIpPort, strNewFcUrl)
	        Select Case intReturnCode
		        Case 1: 'Success
			        'do nothing
		        Case Else: 'Unknown failure
			        strMessage = strMessage & "<font class=failure>Failed to send Fast Contact update notifications. Please contact website administrator</font><br>"
	        End Select
        End If
    End If ' Fast Contact parameters
	
'Setting Smart Call for a device
			
If bSCSupportOSFlag = 1 Then

	If Request.Form("chkSmartCall") = "" Then		
		bEnableSCFlag = 0
		intSCPeriodTime = CONST_MINIMUM_PERIOD_TIME
		intBits = 0
		
	Else
		bEnableSCFlag = 1		
		intSCPeriodTime =  Request.Form("ddlSMFrequency")
		
		If Request.Form("chkSCLocation") <> "" Then 
			intBits = intBits + CONST_SMARTCALL_LOCATION		  
		End If
		
		If Request.Form("chkSCSoftware") <> "" Then 
		 intBits = intBits + CONST_SMARTCALL_SOFTWARE
		End If
		
		If Request.Form("chkSCLoggedOn") <> "" Then 
			intBits = intBits + CONST_SMARTCALL_LOGGEDON 
		End If
		
		If Request.Form("chkSCHardware") <> "" Then   
			intBits = intBits + CONST_SMARTCALL_HARDWARE 
		End If
		
		If Request.Form("chkSCNetwork") <> "" Then 
			intBits = intBits + CONST_SMARTCALL_NETWORK 
		End If
	End If
		
	intReturnCode = SetSmartCallSettingsForEsn(strEsn, bEnableSCFlag, intSCPeriodTime, intBits)
	Select Case intReturnCode
		Case 1: 'Success
			'do nothing
		Case -1: 'Failure
			strMessage = strMessage & "<font class=failure>The operation [SetSmartCallSettingsForEsn] failed: " & rs("err_desc") & "</font><br>"
		Case Else: 'Unknown failure
			strMessage = strMessage & "<font class=failure>The operation [SetSmartCallSettingsForEsn] failed unexpectedly ("& intReturnCode &").  Please contact website administrator</font><br>"
	End Select
	
End If
'The End of The Setting Smart Call 

Response.Redirect "ESNDetails.asp?ESN=" + strESN
	
end if

'Retrieve MoveLock data as appropriate
Set cmd = Server.CreateObject("ADODB.Command")
With cmd
	.CommandTimeout = 120
	.CommandType = adCmdStoredProc
	.CommandText = "dbo.ems_getMoveLockData"
	.NamedParameters = True
	.Parameters.Append .CreateParameter("@ReturnValue", adInteger, adParamReturnValue)
	.Parameters.Append .CreateParameter("@ESN", adVarChar, adParamInput, 20, strEsn)
	.Parameters.Append .CreateParameter("@AccountID", adInteger, adParamInput, , intAccountID)
    .Parameters.Append .CreateParameter("@Status", adVarWChar, adParamOutput, 30, strMoveStatus)
    .Parameters.Append .CreateParameter("@CreatedBy", adVarWChar, adParamOutput, 100, strCreatedBy)
    .Parameters.Append .CreateParameter("@CreatedTS", adDate, adParamOutput, , dtCreatedTS)

	Set .ActiveConnection = cnnCC
	rs.CursorLocation = adUseClient
	Call rs.Open(cmd, , adOpenForwardOnly, adLockReadOnly)	
	intReturnCode = .Parameters("@ReturnValue").Value
	Set .ActiveConnection = Nothing

End With

set cmd = nothing
set rs = nothing
'Get Web page data
Set rs = Server.CreateObject("ADODB.RecordSet")

Set cmd = Server.CreateObject("ADODB.Command")
With cmd
	.CommandTimeout = 120
	.CommandType = adCmdStoredProc
	.CommandText = "dbo.ems_getWeb_MapAccountDetails"
	.NamedParameters = True
	.Parameters.Append .CreateParameter("@ReturnValue", adInteger, adParamReturnValue)
	.Parameters.Append .CreateParameter("@ams_login", adVarWChar, adParamInput, 50, session("ssl_login"))
	.Parameters.Append .CreateParameter("@mapAccountId", adInteger, adParamInput, , intAccountId)

    .Parameters.Append .CreateParameter("@SiloName_out", adVarWChar, adParamOutput, 30, strSiloName)
    .Parameters.Append .CreateParameter("@CallDataServer_out", adVarWChar, adParamOutput, 128, strCallDataServer)
    .Parameters.Append .CreateParameter("@CCDataServer_out", adVarWChar, adParamOutput, 128, strCCDataServer)
    .Parameters.Append .CreateParameter("@CallDataMapServerId_out", adInteger, adParamOutput, , intCallDataMapServerId)
    .Parameters.Append .CreateParameter("@CCDataMapServerId_out", adInteger, adParamOutput, , intCCDataMapServerId)

    
	Set .ActiveConnection = cnnRdir
	rs.CursorLocation = adUseClient
		
	Call rs.Open(cmd, , adOpenForwardOnly, adLockReadOnly)	
	intReturnCode = .Parameters("@ReturnValue").Value
	
    strSiloName = .Parameters("@SiloName_out").Value
    strCallDataServer = .Parameters("@CallDataServer_out").Value
    strCCDataServer = .Parameters("@CCDataServer_out").Value
    intCallDataMapServerId = .Parameters("@CallDataMapServerId_out").Value
    intCCDataMapServerId = .Parameters("@CCDataMapServerId_out").Value

    Set .ActiveConnection = Nothing
	
	Select Case intReturnCode
		Case 1: 'Success
			'do nothing
		Case -1: 'Failure
			strMessage = strMessage & "<font class=""failure"">You are not allowed to view information about this account.</font>"		      			
		Case Else: 'Unknown failure
			strMessage = strMessage & "The operation failed unexpectedly ("& intReturnCode &"). Please contact website administrator."
	End Select

End With

If rs.State = adStateOpen Then
    rs.Close
End If

'Get Web page data
Set cmd = Server.CreateObject("ADODB.Command")
With cmd
	.CommandTimeout = 120
	.CommandType = adCmdStoredProc
	.CommandText = "dbo.ems_getWeb_EsnDetails"
	.NamedParameters = True
	.Parameters.Append .CreateParameter("@ReturnValue", adInteger, adParamReturnValue)
	.Parameters.Append .CreateParameter("@esn", adVarChar, adParamInput, 20, strEsn)
	Set .ActiveConnection = cnnCT
	rs.CursorLocation = adUseClient

	Call rs.Open(cmd, , adOpenForwardOnly, adLockReadOnly)
	intReturnCode = .Parameters("@ReturnValue").Value
	Set .ActiveConnection = Nothing

End With

Select Case intReturnCode
	Case 1: 'Success
		'set values
		intOrderId = rs("order_id")
		intDspAccountId = rs("account_id")
		strAccountName = rs("accountName")
		strStatus = rs("Status")
		intTestInd = rs("testInd")
		intStolenFlag = rs("StolenFlag")
		bStolen = rs("StolenBit")
		bAlert = rs("AlertBit")
		intIPPeriodDays = rs("IPPeriodDays")
		intIPPeriodMinutes = rs("IPPeriodMinutes")
		intAction = rs("Action")
		strNewEsn = rs("NewESN")
		strNewUrl = rs("NewUrl")
		strNewIpAddress = rs("NewIpAddress")
		intNewIpPort = rs("NewIpPort")
		strProduct = rs("product_desc")
		strItemNo = rs("item_no")
		dtLastCallTS = rs("LastCallTs")
		intVersionNumber = rs("VersionNumber")
		dtLastUpdateDate = rs("last_update_date")
		strLastUpdateUser = rs("last_update_user")
		dtCreationDate = rs("creation_date")
		strSerial = rs("serial")
		strAssetNumber = rs("asset_number")
		strMake = rs("computer_make_desc")
		strModel = rs("ParsedModel")
		strUserName = rs("user_name")
		strHddSerialNumber0 = rs("HddSerialNumber0")
		strBiosEnabled = rs("BiosEnabled")
	    licenseChecksum = rs("RowChecksum")
        intCRCATPhase2Rem = rs("CRCATPhase2Rem")
        strDeviceUid = rs("deviceUid")
        strDeviceUid = replace(strDeviceUid, "{", "")
        strDeviceUid = replace(strDeviceUid, "}", "")

        debug "intDspAccountId", intDspAccountId
        
        if IsNull(intAccountId) then
            intAccountId = intDspAccountId
        end if 

	Case -1 'cant find esn in license table
		If rs.State = adStateOpen Then
			rs.Close
		End If

		set rs = nothing
		set cmd = nothing
		set cnnCT = nothing

		response.redirect "NoSearchResult.asp"
	Case -2 'account_license.account_id is null
		If rs.State = adStateOpen Then
			rs.Close
		End If

		set cmd = nothing
		set cnnCT = nothing

		Response.Write "<br><b>account_license.account_id is null for esn.</b><br>"
		Response.End
	Case -3 'access denied
		If rs.State = adStateOpen Then
			rs.Close
		End If

		set cmd = nothing
		set cnnCT = nothing

		Response.Redirect "NoAccess.asp"

	Case Else: 'Unknown failure
		strMessage = strMessage & "The operation [ems_getWeb_EsnDetails] failed unexpectedly ("& intReturnCode &").  Please contact website administrator<br>"
End Select

If bFastContactAvailable = 1 then
    ' Get the Fast Contact parameters
    intReturnCode = GetFastContactSettingsForEsn(strEsn, bEnableFastContact, intFcPeriodSeconds, strNewFcIpAddress, intNewFcIpPort, strNewFcUrl)
    If intReturnCode <> 1 then
        strMessage = strMessage & "The operation [GetFastContactSettingsForEsn] failed unexpectedly ("& intReturnCode &").  Please contact website administrator<br>"
    End If
End If

'Get Smart Call for a Device

If bSCSupportOSFlag = 1 then
    ' Get the Smart Call parameters
    intReturnCode = GetSmartCallSettingsForEsn(intAccountId, strEsn, bEnableSCFlag, intSCPeriodTime, bEnableSCLocation, bEnableSCSoftware, bEnableSCLoggedOn, bEnableSCHardware, bEnableSCNetwork)
    If intReturnCode <> 1 then
       strMessage = strMessage & "The operation [GetSmartCallSettingsForEsn] failed unexpectedly ("& intReturnCode &").  Please contact website administrator<br>"
    End If
End If
'The End of the Getting Smart Call

' Get the CTC Flag value
Set cmd = Server.CreateObject("ADODB.Command")
With cmd
	.CommandTimeout = 120
	.CommandType = adCmdStoredProc
	.CommandText = "dbo.ems_getWeb_EsnServiceOptions"
	.NamedParameters = True
	.Parameters.Append .CreateParameter("@ReturnValue", adInteger, adParamReturnValue)
	.Parameters.Append .CreateParameter("@esn", adVarChar, adParamInput, 20, strEsn)
	.Parameters.Append .CreateParameter("@ctcflag", adInteger, adParamOutput, 0, bCtcFlag)
	Set .ActiveConnection = cnnCC
	rs2.CursorLocation = adUseClient

	Call rs2.Open(cmd, , adOpenForwardOnly, adLockReadOnly)
	intReturnCode = .Parameters("@ReturnValue").Value

	bCTCFlag = .Parameters("@ctcflag").Value	
	
	' start debug code 
	test_return = intReturnCode   	
	if isnull(bCTCFlag) then 
		test_ctcflag = "NULL"
	else 
		test_ctcflag = bCTCFlag
	end if 
	' end debug code 	
	
    if isnull(bCTCFlag) then bCTCFlag = 0
	Set .ActiveConnection = Nothing

End With

Select Case intReturnCode
	Case 1: 'Success
		'do nothing
	Case -1: 'Failure
		strMessage = strMessage & "The operation [ems_getWeb_EsnServiceOptions] failed: " & rs2("err_desc") & "<br>"
	Case Else: 'Unknown failure
		strMessage = strMessage & "The operation [ems_getWeb_EsnServiceOptions] failed unexpectedly ("& intReturnCode &").  Please contact website administrator<br>"
End Select

If rs2.State = adStateOpen Then
    rs2.Close
End If

debug "Session(""EnterpriseType"")", Session("EnterpriseType")

'run ems_getAccountDetails(account_id)

strProc = "exec dbo.ems_getAccountDetails @accountID="  & intDspAccountId
rs3.Open strProc, strCTConnString, adOpenForwardOnly, adLockReadOnly, adCmdText
If Not rs3.EOF Then
    AccountUID = rs3("AccountUId")
    AccountUID = replace(AccountUID, "{", "")
    AccountUID = replace(AccountUID, "}", "")
    
'If there is no account with the given account_id then print out error message
'If this ever happens there is a SERIOUS PROBLEM, because the accountID is forwarded from other pages
else
	Response.write "<br><font class=failure>Internal EMS error: Account " & intDspAccountId & " can not be found</font><br>"
End If

'close ems_getAccountDetails
rs3.close

dataCenter = GetAccountDataCenterByAccountId(intDspAccountId)

%>

<div class="ems-body-container">
          <div class="device-info row">
               <div class="col-md-12 col-sm-12 col-xs-12">
                   <div class="device-info__account-name"><a class="link" href="AccountDetails.asp?AccID=<%=intDspAccountId%>"> <b>Account <%= intDspAccountId %>: <%=strAccountName%> </b></a>
                    </div>
                   <div class="device-info__order-id"><a class="link" href="OrderDetails.asp?AccID=<%= intDspAccountId %>&OrdID=<%=intOrderId%>"><b>Device Container <%=intOrderId%>  </b></a>
              </div>
               </div>
              <div class="angular-block">
                 <div class="ems-widget" style="visible:hidden" device-report-init esn="<%=strESN%>" accountid="<%=intDspAccountId%>" deviceuid="<%=LCase(strDeviceUid)%>" accountuid="<%=AccountUID%>" datacenter=<%=datacenter%>></div>

              <div class="left-block col-md-7 col-sm-7">
              <div class="device-info__esn"><span> <b>ESN</b>: <span><%= strEsn %></span></span>
                
                
               <%if Session("EnterpriseType") = "Absolute" then %>
                  <%if bStolen = 1 then%>
                  <!--<div class="device-info__status device-info__status--stolen">Stolen
                   </div>-->
                  <%end if%>
                   <!--<div class="ems-widget" device-missing-status style="display: inline-block"></div>-->
                <%end if%>
              </div>
              <div class="device-info__device-uid"><span> <b>DeviceUID</b>: <span><%= strDeviceUid %></span></span>
              </div>
              <div class="ems-widget" esn-imei esn="<%= strEsn %>" accountuid="<%=AccountUID%>" deviceuid="<%= strDeviceUid %>" datacenter="<%=datacenter%>" serial="<%=strSerial%>"></div>
            </div>

                <div class="right-block col-md-5 col-sm-5 col-xs-12">
               <!-- widget here-->
              <div class="device-info__stolen-status">
                  <div style="margin-bottom: 6px;" class="ems-widget" device-stolen accountuid="<%=AccountUID%>" deviceuid="<%= strDeviceUid %>" esn="<%= strEsn %>"></div>
              </div>
              <div class="device-info__classic-status row col-md-12 col-sm-12 col-xs-12">
                <label class="col-md-6 col-sm-6 col-xs-6">Classic Status</label><span class="col-md-6 col-sm-6 col-xs-6" id="classicstatus"></span>
              </div>
              <div class="device-info__nextgen-status row col-md-12 col-sm-12 col-xs-12">
                  <div class="ems-widget" ng-status accountuid="<%=AccountUID%>" deviceuid="<%= strDeviceUid %>" datacenter="<%=datacenter%>"></div>
              </div>
              <div class="device-info__cms-status row col-md-12 col-sm-12 col-xs-12">
                  <div class="ems-widget" component-manager accountuid="<%=AccountUID%>" deviceuid="<%= strDeviceUid %>" datacenter="<%=datacenter%>"></div>
              </div>
                <% if Session("EnterpriseType") = "Absolute" then %>
                  <div class="device-info__silo-info row col-md-12 col-sm-12 col-xs-12">
                    <label class="col-md-6 col-sm-6 col-xs-6">Silo</label><span class="col-md-6 col-sm-6 col-xs-6"><%=strSiloName %></span>
                  </div>
                  <div class="device-info__ct-server-info row col-md-12 col-sm-12 col-xs-12">
                    <label class="col-md-6 col-sm-6 col-xs-6">CT Server</label><span class="col-md-6 col-sm-6 col-xs-6"><%
                        if intCallDataMapServerId <> -1 then %> 
                            <%= strCallDataServer%>
                        <% end if %></span>
                  </div>
                  <div class="device-info__cc-server-info row col-md-12 col-sm-12 col-xs-12">
                    <label class="col-md-6 col-sm-6 col-xs-6">CC Server</label><span class="col-md-6 col-sm-6 col-xs-6"><%
                        if intCCDataMapServerId <> -1 then %> 
                            <%= strCCDataServer %>
                        <% end if %></span>
                  </div>
                    
                    <%if datacenter = strEUDC then%>
						<div class="device-info__data-center-info row col-md-12 col-sm-12 col-xs-12">
                    <label class="col-md-6 col-sm-6 col-xs-6">Data Center</label><span class="col-md-6 col-sm-6 col-xs-6"><%= datacenter %></span>
                  </div>
					<% end if%>
                <% end if %>   
            </div>
                  </div>
          </div>
        <%if strMessage <> "" then Response.Write "<br><br><div class='message col-md-12 col-sm-12 col-xs-12' style='padding-left: 0; margin-top: 20px;'>" & strMessage & "</div>" end if%>
		<%if session("msg")<> "" then  Response.Write "<br><br>" & session("msg") end if%>
		<%if session("msg")<> "" then session("msg") ="" end if %>
          <div class="clearfix"></div>
          <br/>
          <br/>
          <div class="utility">
            <span class="btn btn-lg utility__ems-button-collapse collapsed" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                <span class="glyphicon glyphicon-menu-hamburger"></span> Utility
            </span>
            <div class="group-button">
                <div class="collapse" id="collapseExample">
                    <button class="utility__ems-button btn" type="button" title="View entries in the MonitorEvent table for this ESN" name="ViewCallsMeButton" onclick='javascript:document.location = "MonitoringReports.asp?AccID=<%=intDspAccountId%>&OrderID=<%=intOrderID%>&ESN=<%=strESN%>";'>View calls (ME)
                    </button>
                    <button class="utility__ems-button btn" type="button" name="CDFBtn" title="Custom Defined Fields" onclick='javascript:document.location = "CDF.asp?AccID=<%=intDspAccountId%>&OrderID=<%=intOrderID%>&ESN=<%=strESN%>&deviceUID=<%=LCase(strDeviceUid)%>&accountuid=<%=AccountUID%>&datacenter=<%=datacenter%>"'>Custom Defined Fields
                    </button>
                    <button class="utility__ems-button btn" type="button" name="DeviceReportDetailsBtn" title="Device Report Details" onclick='javascript:document.location = "DeviceReportDetails.asp?AccID=<%=intDspAccountId%>&OrderID=<%=intOrderID%>&ESN=<%=strESN%>&AccUID=<%=AccountUID%>&deviceUID=<%=LCase(strDeviceUid)%>&datacenter=<%=datacenter%>"'>Device Report Details
                    </button>
                    <button class="utility__ems-button btn" type="button" name="ViewCallsMedButton" title="View entries in the MonitorEvent_Details table for this ESN" onclick='javascript:document.location = "MonitorEventDetails.asp?AccID=<%=intDspAccountId%>&OrderID=<%=intOrderID%>&ESN=<%=strESN%>"'>View calls (MED)
                    </button>
                    <button class="utility__ems-button btn" type="button" name="ClientScriptBtn" title="Enable/Disable Scripts" onclick='javascript:document.location = "ClientScript.asp?AccID=<%=intDspAccountId%>&OrderID=<%=intOrderID%>&ESN=<%=strESN%>"'>Enable/Disable Scripts
                    </button>
                    <button class="utility__ems-button btn" type="button" name="ViewBitlockerBtn" title="View Bitlocker Details of this ESN" onclick='javascript:document.location = "RnRDetails.asp?AccID=<%=intDspAccountId%>&OrderID=<%=intOrderID%>&ESN=<%=strESN%>&deviceuid=<%=LCase(strDeviceUid)%>&accountuid=<%=AccountUID%>&datacenter=<%=datacenter%>"'>Application Persistence
                    </button>
                    <button class="utility__ems-button btn" type="button" name="ViewESNHistoryBtn" title="View License History change for this ESN" onclick='javascript:document.location = "ESNHistoryDetails.asp?AccID=<%=intDspAccountId%>&ESN=<%=strESN%>"'>View ESN History
                    </button>
                    <button class="utility__ems-button btn" type="button" name="ViewScriptsResultsBtn" title="View Script Results" onclick='javascript:document.location = "AgentRemoteExecuteResult.asp?AccID=<%=intDspAccountId%>&OrderID=<%=intOrderID%>&ESN=<%=strESN%>"'>View Script Results
                    </button>
                    <button class="utility__ems-button btn" type="button" name="ViewESNAssignmentBtn" title="View ESN Assignment" onclick='javascript:document.location = "ESNAssignment.asp?AccID=<%=intDspAccountId%>&ESN=<%=strESN%>&OrderID=<%=intOrderID%>"'>ESN Assignment					
                    </button>
                    <button class="utility__ems-button btn" type="button" name="ViewPackageStatussBtn" title="View Package Status" onclick='javascript:document.location = "DeviceReportDetails.asp?AccID=<%=intDspAccountId%>&OrderID=<%=intOrderID%>&ESN=<%=strESN%>&deviceUID=<%=LCase(strDeviceUid)%>&isViewPackageStatus=True&datacenter=<%=datacenter%>&AccUID=<%=AccountUID%>"'>View Package Status					
                    </button>
                    <button class="utility__ems-button btn" type="button" name="ViewMoveESNHistoryBtn" title="View Device Move History for this ESN" onclick='javascript:document.location = "DeviceMoveESNHistory.asp?AccID=<%=intDspAccountId%>&ESN=<%=strESN%>"'>View Device Move History					
                    </button>
                    <button class="utility__ems-button btn" type="button" name="ViewPersistentCallsBtn" title="View Persistent Calls History" onclick='javascript:document.location = "PersistentCalls.asp?AccID=<%=intDspAccountId%>&ESN=<%=strESN%>&OrderID=<%=intOrderID%>"'>View Persistent Calls History					
                    </button>
                    <button class="utility__ems-button btn" type="button" name="ViewGeolocationHistoryBtn" title="View Geolocation History" onclick='javascript:document.location = "GeolocationHistory.asp?AccID=<%=intDspAccountId%>&ESN=<%=strESN%>&OrderID=<%=intOrderID%>&deviceUID=<%=LCase(strDeviceUid)%>&accountuid=<%=AccountUID%>"'>View Geolocation History					
                    </button>
                    <button class="utility__ems-button btn" type="button" name="DeviceActionsBtn" title="Device Actions" onclick='javascript:document.location = "DeviceActions.asp?AccID=<%=intDspAccountId%>&AccUID=<%=AccountUID%>&OrderID=<%=intOrderID%>&ESN=<%=strESN%>&deviceUID=<%=LCase(strDeviceUid)%>&datacenter=<%=datacenter%>"'>Device Actions					
                    </button>
              </div>
            </div>
          </div>
           <br>

<script type="text/javascript">
var actcmindex = -1;
var actldindex = -1;


////////////////////////////
<%
'stolen update is only for partners
if Session("EnterpriseType") = "Partner" then
%>
//Sets the stolen flag and submits the form - called when the user checks the "Stolen" checkbox
function SetStolenFlag()
{

	var frm = document.AMSESNDetails;
	var index;
	var RemoveAgent;

	//is action flag "Remove agent from computer." set
	RemoveAgent = false;
	for (index = 0; index < frm.ActionID.length; index++) {
		if (frm.ActionID[index].checked && frm.ActionID[index].value == 65536) {
			RemoveAgent = true;
		}
	}

	//check to make sure the stolen flag is not set for a Disabled esn
	if (frm.StolenFlag.checked == true && (frm.EsnStatus.value == 'D' || RemoveAgent  )) {
        alert("This ESN is DISABLED/REMOVED, you can't set it to stolen.");
		frm.StolenFlag.checked = false;
		return false;
	}
	//set flag
	if (frm.StolenFlag.checked == true) {
		if (!confirm("Setting this device as stolen will:\n1. Change the callback time for Modem and IP to 15 minutes.\n2. Remove \"Update agent modem call-back settings after a successful agent IP call.\" flag.\n3. Generate an alert to Recovery on every call from this device.\nAre you sure you want to do this?")) {
			frm.StolenFlag.checked = false;
			return false;
		}
	}


	//remove flag
	else {
		if(!confirm("If you remove the stolen flag from this device Recovery will not be alerted when the device calls.\nAre you sure you want to do this?")) {
			frm.StolenFlag.checked = true;
			return false;
		}
	}

    //submit form
	frm.UpdateStolenFlag.value = "1";
	frm.submit();
	return true;
}

<%
end if
%>

function GetDDRequestDetails(DDRequestId, AccId) {
    var frm = document.AMSESNDetails;
	window.open("DDRequestDetails.asp?RId="+DDRequestId+"&AccId="+AccId,"");
}

function DeleteInventoryRecord() {
	if(confirm("You are about to delete a computer table record.\nAre you sure you want to do this?")) {
		document.AMSESNDetails.DeleteInventoryRecordFlag.value = 1;
		document.AMSESNDetails.submit();
		return true;
	}
} //DeleteInventoryRecord

function RequestForcedCall() {
	var frm = document.AMSESNDetails;
	if(confirm("You are about to request the computer to make a forced call.\nPlease note that the computer will not call until the next time it contacts our Fast Contact servers\nAre you sure you want to continue?")) {
		frm.RequestForcedCallFlag.value = 1;
		frm.submit();
		return true;
	}
} //RequestForcedCall

function RequestCTMLog() {
	var frm = document.AMSESNDetails;
	if(confirm("The log will be uploaded at \\\\vcr1fs1\\CTM_DEVICE_LOGS")) {
		frm.RequestCTMLogFlag.value = 1;
		frm.submit();
		return true;
	}
} //RequestCTMLog

function SetDdToCompleted(ESN) {
	var frm = document.AMSESNDetails;

	if(confirm("This may result in an email sent to the customer. Set DD status for ESN " + ESN + " to 'Completed'?")) {
	
	    frm.SetDdperpetualFlag.value = 0;
	    if(confirm("Do you want to remove the perpetual delete flag for ESN " + ESN + "?"))
            frm.SetDdperpetualFlag.value = 1;
	    
		frm.SetDdToCompletedFlag.value = 1;
		frm.submit();
		return true;
	}
	else
	    return false;
} //SetDdToCompleted

//open new window to show details about given action flag
function OpenActionWindow(ActionFlag) {
	window.open("ActionFlagDescription.asp?AId=" + ActionFlag, "","toolbar=no,menubar=0,resizable=1,scrollbars=yes,width=550,height=400");
}

//open new window to show details about given action flag
function OpenCustomActionWindow(description, extendedDescription, platform, version, flagType) {
	window.open("ActionFlagDescription.asp?D=" + encodeURIComponent(description) 
        + "&ED=" + encodeURIComponent(extendedDescription)
        + "&P=" + encodeURIComponent(platform)
        + "&V=" + encodeURIComponent(version)
        + "&F=" + encodeURIComponent(flagType), "","toolbar=no,menubar=0,resizable=1,scrollbars=yes,width=550,height=400");
}



//Check to make sure a field is a number, and not null and greater than zero
function CheckField(fld)
{
	//If field is a non-number character then alert the user
	if (isNaN(fld.value)) {
		alert("Please enter a valid number.");
		fld.focus();
		return true
	}
	//If the field is a number less than zero then alert the user
	if ( parseInt( fld.value ) < 0 ){
		alert("Specify field > 0");
		fld.focus();
		return true
	}
	//If the field is null then alert the user
	if (fld.value == "") {
		alert("Please enter a value");
		fld.focus();
		return true
	}
}

//Check to make sure a field is a number, and not null and greater than -29
function CheckFieldMinutes(fld)
{
	//If field is a non-number character then alert the user
	if (isNaN(fld.value)) {
		alert("Please enter a valid number.");
		fld.focus();
		return true
	}
	//If the field is a number less than zero then alert the user
	if ( parseInt( fld.value ) < -29 ){
		alert("Specify field > -30");
		fld.focus();
		return true
	}
	//If the field is null then alert the user
	if (fld.value == "") {
		alert("Please enter a value");
		fld.focus();
		return true
	}
}

//Check to make sure a field is not null
function CheckValue(fld)
{
	//If the field is null then alert the user
	if (fld.value == "") {
		alert("Please enter a value");
		fld.focus();
		return true
	}
}

// Check to make sure field is a valid IP address
function CheckIpAddress(fld)
{
    var ip = fld.value;

	//remove all "." from ip
	for (i=0;i<ip.length;i += 1) {
		if (ip.charAt(i) == ".") {
			ip = ip.substr(0,i) + ip.substr(i+1);
			i--;
		}
	}

	//If ip is null or not a number then alert user
	if (ip=="" || isNaN(ip)) {
		alert("Please enter a valid IP address");
		fld.focus();
        return  true;
	}
}

//Check to make sure field is a valid IP port
function CheckIpPort(fld)
{
	//If is not a number or > 0 then alert user
	if (CheckField(fld)){
		return true;
	}

	if (fld.value>32767){
		alert("Please use an ip port < 32768");
		fld.focus();
		return true;
	}
}



//verifies fields and sets
function UpdateData()
{

	var frm = document.AMSESNDetails;

    //Set data for action checkbox list

    $('.action-type.cloneItem').find('input').each(function(index,item){
        var selectCon;
        if($.isNumeric($(item).attr('value'))){
            selectCon = $('.action-type.hiddenItem').find('input[value="'+$(item).attr('value')+'"]');
        }else{
            selectCon = $('.action-type.hiddenItem').find('input[name="'+$(item).attr('name')+'"]');
        }
        selectCon.prop('checked', $(item).prop('checked'));
        $(this).removeAttr('name');
    })

	//If ipdays, ippminutes are not numbers or < 0 then alert user
	if (
	   CheckField(frm.ippdays)
	|| CheckFieldMinutes(frm.ippminutes)
	){
		return false;
	}

	//If ippdays and ippminutes are both 0 then alert user
	if (frm.ippdays.value==0 && frm.ippminutes.value==0) {
		alert("IP Period Days and IP Period Minutes can not both be 0");
		frm.ippminutes.focus();
		return false;
	}
	//If ippminutes < 15 confirm with user
	if (frm.ippdays.value==0 && frm.ippminutes.value < 15) {
		if (!confirm("You are about to set the IP callback time for less than 15 minutes\nAre you sure you want to do this?")){
			frm.ippminutes.focus();
			return false;
		}
	}
	//If ippminutes > 1440 alert user
	if (frm.ippminutes.value >= 1440) {
		alert("IP Period Minutes must be less than 1440 (24 hours)");
		frm.ippminutes.focus();
		return false;
	}
	//If ippday > 14 confirm with user
	if (frm.ippdays.value > 14) {
		if (!confirm("You are about to set the IP callback time for more than 14 days\nAre you sure you want to do this?")){
			frm.ippdays.focus();
			return false;
		}
	}
	//If newipport has information
	if (frm.newipport.value != "") {
        if (CheckIpPort(frm.newipport)){
            return false;
        }
		//If a new IP address was not defined while a new port is defined then alert user
		if (frm.newip.value == "") {
			alert("Please enter an IP Address.");
			frm.newip.focus();
			return false;
		}
	}

	//If newip has information
	if (frm.newip.value != ""){
        if (CheckIpAddress(frm.newip)){
            return false;
        }
	}

<% if bFastContactAvailable = 1 then %>
	//Validate fcNewIpPort
	if (frm.fcNewIpPort.value!=""){
        if (CheckIpPort(frm.fcNewIpPort)){
            return false;
        }
		//If a new IP address was not defined while a new port is defined then alert user
		if (frm.fcNewIp.value == "") {
			alert("Please enter an IP Address.");
			frm.fcNewIp.focus();
			return false;
		}
	}

	//Validate fcNewIp
	if (frm.fcNewIp.value!=""){
        if (CheckIpAddress(frm.fcNewIp)){
            return false;
        }
	}

    if (CheckField(frm.fcPeriod))
	{
		return false;
	}

	//If Fast Contact period < 5 min confirm with user
	if (frm.fcPeriod.value<5*60 && frm.chkEnableFastContact.checked) {
		if (!confirm("You are about to set the Fast Contact callback time to less than 5 minutes\nAre you sure you want to do this?")){
			frm.fcPeriod.focus();
			return false;
		}
	}
	
	frm.RequestForcedCallFlag.value = 0;
<% end if %>

//Validate smart call
<% if bSCSupportOSFlag = 1 then %>

if(frm.chkSmartCall.disabled)
{
 frm.chkSmartCall.checked = false;
}

if(frm.chkSCLocation.disabled)
{
 frm.chkSCLocation.checked = false;
}

if(frm.chkSCSoftware.disabled)
{
 frm.chkSCSoftware.checked = false;
}

if(frm.chkSCLoggedOn.disabled)
{
 frm.chkSCLoggedOn.checked = false;
}

if(frm.chkSCHardware.disabled)
{
 frm.chkSCHardware.checked = false;
}

if(frm.chkSCNetwork.disabled)
{
 frm.chkSCNetwork.checked = false;
}

if(frm.chkSmartCall.checked)
{ 
	if(!frm.chkSCLocation.checked && !frm.chkSCSoftware.checked && !frm.chkSCLoggedOn.checked && !frm.chkSCHardware.checked && !frm.chkSCNetwork.checked)
	{
		alert("At least one call settings option must be selected");
		return false;
	}
}

<% end if %>

/////////////////////////
<%
'these fields will not be shown unless absolute or Partner user. no need to verify data
if session("EnterpriseType") = "Absolute" or Session("EnterpriseType") = "Partner" then
%>
	//stolen flag is set can't set ACTION_REMOVE_AGENT flag
	for(var i=0;i<frm.ActionID.length;i++) {
		if ((frm.ActionID[i].checked && frm.ActionID[i].value == 65536) && frm.StolenFlag.checked) {
			alert("This ESN is STOLEN, you can't set it to Disabled.");
			return false;
		}
	}

<%
End if
%>
////////////////////////
    var bactcm = false;
	var bactld = false;
	
    if(actcmindex >=0)
    {
        bactcm = frm.ActionID[actcmindex].checked;
        if (bactcm == false)
            frm.DisableCmAndLd.value = "1";
    }

    if(actldindex >=0)
    {
        bactld = frm.ActionID[actldindex].checked;
        if(bactld == false)
            frm.DisableCmAndLd.value = "1";
    }

     
    if( bactcm && bactld)
        frm.DisableCmAndLd.value = "0";
       
    if( confirm("Are you sure you want to save these changes?")) {
			//submit form
	        frm.SaveFlag.value = "1";
	        document.AMSESNDetails.submit();
	        return true;
	}
    else {
				return false;
	}    

	
}
</script>

<table cellpadding="5" cellspacing="0" border="0" width="97%">
<%
If strMoveStatus <> "" Then
%>
<tr>
	<td colspan="4">
		<b>This ESN is being moved!</b><br>
		<hr>
	</td>
</tr>
<tr class=coloredRow>
	<td><b>Move Status:</b>
	<td colspan="3" width="85%"><%=strMoveStatus%>
</td>
</tr>
<tr>
	<td><b>Created By:</b>
	<td><%=strCreatedBy%>
	<td width="25%"><b>Created Timestamp:</b>
	<td width="25%"><%=dtCreatedTS%>
</tr>
<tr>
	<td colspan="4">
		<hr>
	</td>
</tr>

<%
End If
%>
</table>

<FORM name="AMSESNDetails" ACTION="ESNDetails.asp?ESN=<% =strEsn %>" METHOD="post">

<input type="hidden" name="DeleteInventoryRecordFlag" value="0" />
<input type="hidden" name="SetDdToCompletedFlag" value="0" />
<input type="hidden" name="SetDdperpetualFlag" value="0" />
<input type="hidden" name="SaveFlag" value="0" />
<input type="hidden" name="UpdateStolenFlag" value="0" />
<input type="hidden" name="EsnStatus" value="<% =strStatus %>" />
<input type="hidden" name="AccountID" value="<% =intAccountId %>" />
<input type="hidden" name="licenseChecksum" value="<% =licenseChecksum %>" />
<input type="hidden" name="RequestForcedCallFlag" value="0" />
<input type="hidden" name="RequestCTMLogFlag" value="0" />

<!--start debug -->
<input type="hidden" name="test_return" value="<%= test_return %>" />
<input type="hidden" name="test_ctcflag" value="<%= test_ctcflag %>" /> 
<input type="hidden" name="test_enterprisetype" value="<%= Session("EnterpriseType") %>" />  
<input type="hidden" name="test_bCTCFlag" value="<%=bCTCFlag%>" />
<!--end debug -->

    <div class="angular-block">
        <div class="dds5-detail-block">
        <h4 class="block-label">DDS5 Details</h4>
            <div class="dds5-detail-block__content-block col-md-12 col-sm-12 col-xs-12 row">
                <div class="table-responsive col-md-6 col-sm-6 col-xs-12 no-padding-left">
                    <table class="table table-hover table-bordered">
                    <tr class="first-col-label">
                        <td class="col-md-6 col-sm-6 col-xs-6">Computer Make</td>
                        <td class="col-md-6 col-sm-6 col-xs-6"><%=strMake%></td>
                    </tr>
                    <tr class="first-col-label">
                        <td class="col-md-6 col-sm-6 col-xs-6">Computer Model</td>
                        <td class="col-md-6 col-sm-6 col-xs-6"><%=strModel%></td>
                    </tr>
                    <tr class="first-col-label">
                        <td class="col-md-6 col-sm-6 col-xs-6">HDD Serial Number</td>
                        <td class="col-md-6 col-sm-6 col-xs-6"><%=strHddSerialNumber0%></td>
                    </tr>
                    <tr class="first-col-label">
                        <td class="col-md-6 col-sm-6 col-xs-6">User Name</td>
                        <td class="col-md-6 col-sm-6 col-xs-6"><%=strUserName%></td>
                    </tr>
                    <tr class="first-col-label">
                        <td class="col-md-6 col-sm-6 col-xs-6">Version</td>
                        <td class="col-md-6 col-sm-6 col-xs-6"><%=intVersionNumber%></td>
                    </tr>
                    <tr class="first-col-label">
                        <td class="col-md-6 col-sm-6 col-xs-6">BIOS Enabled</td>
                        <td class="col-md-6 col-sm-6 col-xs-6"><%=strBiosEnabled%></td>
                    </tr>
                    <tr class="first-col-label">
                        <td class="col-md-6 col-sm-6 col-xs-6">Asset Number</td>
                        <td class="col-md-6 col-sm-6 col-xs-6">
                        <input type="text" class="form-control" name="assetno" value="<%=strAssetNumber%>" maxlength="250">
                        </td>
                    </tr>
                    </table>
                </div>
                <div class="table-responsive col-md-6 col-sm-6 col-xs-12 no-padding">
                    <table class="table table-hover table-bordered">
                    <tr class="first-col-label">
                        <td class="col-md-6 col-sm-6 col-xs-6">Product</td>
                        <td class="col-md-6 col-sm-6 col-xs-6"><%=strProduct%></td>
                    </tr>
                    <tr class="first-col-label">
                        <td class="col-md-6 col-sm-6 col-xs-6">Item_no</td>
                        <td class="col-md-6 col-sm-6 col-xs-6"><%=strItemNo%></td>
                    </tr>
                    <tr class="first-col-label">
                        <td class="col-md-6 col-sm-6 col-xs-6">Creation Date</td>
                        <td class="col-md-6 col-sm-6 col-xs-6"><%=dtCreationDate%></td>
                    </tr>
                    <tr class="first-col-label">
                        <td class="col-md-6 col-sm-6 col-xs-6">Last Call Date</td>
                        <td class="col-md-6 col-sm-6 col-xs-6"><%=dtLastCallTS%></td>
                    </tr>
                    <tr class="first-col-label">
                        <td class="col-md-6 col-sm-6 col-xs-6">Last Update Date</td>
                        <td class="col-md-6 col-sm-6 col-xs-6"><%=dtLastUpdateDate%></td>
                    </tr>
                    <tr class="first-col-label">
                        <td class="col-md-6 col-sm-6 col-xs-6">Last Update User</td>
                        <td class="col-md-6 col-sm-6 col-xs-6"><%=strLastUpdateUser%></td>
                    </tr>
                    <%
		            set rs = rs.NextRecordset

		            if rs("DDRequestId") <> -1 then
		            %>
                        <tr class="first-col-label">
                            <td class="col-md-6 col-sm-6 col-xs-6"><a href="javascript:GetDDRequestDetails(<%=rs("DDRequestId")%>,<%=intDspAccountId%>)">DD Status:</a></td>
                            <td class="col-md-6 col-sm-6 col-xs-6"><a title="<%=rs("DDStatusDescription")%>"><%=rs("DDStatusName")%></a></td>
                        </tr>
		            <%
		            end if
		            %>

                    <%
                      ' Online status only relevant to active devices with FC enabled
                      If bFastContactAvailable and strStatus = "A" Then 
                        intOnlineStatus = GetOnlineStatusForEsn(strESN, intFcPeriodSeconds, strTimestamp)%>                
                    <tr class="first-col-label">
                        <td class="col-md-6 col-sm-6 col-xs-6">Fast Contact Status</td>
                        <td class="col-md-6 col-sm-6 col-xs-6"><img src="Images/<%=GetOnlineStatusImageRefForStatus(intOnlineStatus)%>" />&nbsp;<%=GetOnlineStatusTextForCode(intOnlineStatus)%></td>
                    </tr>
                    <tr class="first-col-label">
                        <td class="col-md-6 col-sm-6 col-xs-6">Time Last FC Call</td>
                        <td class="col-md-6 col-sm-6 col-xs-6"><%=strTimestamp%></td>
                    </tr>
                    <% End If %>
                    </table>
                </div>
            </div>
            <div class="col-md-9 col-sm-9 col-xs-9 row">
                <button class="dds5-detail-block__ems-button btn" type="button" name="DelInventoryButton" onClick="DeleteInventoryRecord()">Delete Computer Table Record
        </button>
        <button class="dds5-detail-block__ems-button btn" type="button" name="ConvertToLojackEsnButton" disabled onClick="">Convert ESN to LoJack ESN
        </button>
        <button class="dds5-detail-block__ems-button btn" type="button" name="DDRequestCompletedButton" onClick="SetDdToCompleted('<%=strESN %>')">Set DD Request to 'Completed'
        </button>
        <% if bFastContactAvailable and strStatus = "A" then %>
        <button class="dds5-detail-block__ems-button btn" name="RequestForcedCallButton" type="button" <% if bEnableFastContact=0 then response.write "disabled" %> onClick="RequestForcedCall()">Request Forced Call
        </button>
        <% end if %>
        
        <%
            Dim returncode, osflag
            returncode = GetOSflagByEsn(strEsn,osflag)
             If intReturnCode > 0 Then
                If len(osflag)> 0 and (osflag = "C" or osflag = "A") Then
        %>

        <button class="dds5-detail-block__ems-button btn" onClick="RequestCTMLog()"  id="RequestCTMLogButton" name="RequestForcedCallButton" type="button" >Request CTM Log
        </button>
        <% 
                End If
            End If    
        %>
          </div>
        <div class="col-md-3 col-sm-3 col-xs-3 no-padding"><button class="dds5-detail-block__ems-button dds5-detail-block__ems-button--save btn" type="button" name="update1" onClick="UpdateData()">Save Changes
        </button></div>
        
        </div>
<div class="clearfix"></div>
    
<div class="dds6-detail-block">
    <div class="ems-widget" style="visible:hidden" device-report-init esn="<%=strESN%>" accountid="<%=intDspAccountId%>" deviceuid="<%=LCase(strDeviceUid)%>" accountuid="<%=AccountUID%>" datacenter=<%=datacenter%>></div>


    <div class="ems-widget" esn-details esn="<%=strESN%>" account-id="<%=intDspAccountId%>" device-uid="<%=LCase(strDeviceUid)%>" account-uid="<%=AccountUID%>" datacenter=<%=datacenter%>></div>

    </div>
    <div class="clearfix"></div>
    
    <!-- end next gen block-->

    </div>
<table style="display:none">
<!--next gen block-->
<tr>
	<td colspan="4" style="display:none"><div class="ems-widget" device-report-init esn="<%=strESN%>" accountid="<%=intDspAccountId%>" deviceuid="<%=LCase(strDeviceUid)%>" accountuid="<%=AccountUID%>" datacenter=<%=datacenter%>></div></td>
</tr>
<tr>
	<td colspan="4"><div class="ems-widget" esn-details esn="<%=strESN%>" account-id="<%=intDspAccountId%>" device-uid="<%=LCase(strDeviceUid)%>" account-uid="<%=AccountUID%>" datacenter=<%=datacenter%>></div></td>
</tr>

<tr>
	<td valign="top" colspan="2">
		<input type="button" name="update1" value="Save Changes" onClick="UpdateData()">
	</td>
	<td colspan="2">     
	</td>
</tr>
</table>

<% 

dim strIntelAtpReturnMessage

if Request.Form("hdnSetIntelAtpActiveFlag") = "1" then
    dim blnAtpStatusChange
    blnAtpStatusChange = SetAtpStatusActive(strEsn, intAccountId)
    if blnAtpStatusChange then
        strIntelAtpReturnMessage = "State changed to Active."
    else
        strIntelAtpReturnMessage = "State update failed."
    end if
end if

if Request.Form("hdnRequestRemoteUnlockFlag") = "1" then
    
    dim strServerRecoveryToken
    strServerRecoveryToken = AtpServerRecoveryToken(strEsn, request.Form("txtUnlockCode") )
    strIntelAtpReturnMessage = strServerRecoveryToken

end if

dim oAtpDevice
set oAtpDevice = GetAtpDeviceByEsn(strEsn)
if NOT (oAtpDevice is nothing) then 
    dim intEnrollmentStatus, intChipType, intTimerAction, intLockAction, intStateCurrent, intStateNew
    dim intTimerDays, strIntelATMessage, strIntelATState, strEnrollmentStatusDescription
    dim intCurrentATState, intNewATState, intLockActionNew, intTimerActionNew, strIntelATMessageNew, intTimerDaysNew
    
    intChipType = CInt(oAtpDevice.AtpChipType)
    intEnrollmentStatus = CInt(oAtpDevice.AtpEnrollmentNew)
    intTimerAction = CInt(oAtpDevice.AtpTimerActionCurrent)
    intTimerActionNew = CInt(oAtpDevice.AtpTimerActionNew)
    intLockAction = CInt(oAtpDevice.AtpTheftActionCurrent)
    intLockActionNew = CInt(oAtpDevice.AtpTheftActionNew)
    
    intStateCurrent = CInt(oAtpDevice.AtState)
debug "AtpStateNew", oAtpDevice.AtpStateNew     
    intStateNew = CInt(oAtpDevice.AtpStateNew)
    intTimerDays = CLng(oAtpDevice.AtpTimerValueCurrent)/86400
    intTimerDaysNew = CLng(oAtpDevice.AtpTimerValueNew)/86400
    
    strIntelATMessage = oAtpDevice.AtpMessageCurrent
    strIntelATMessageNew = oAtpDevice.AtpMessageNew
    intCurrentATState = CInt(oAtpDevice.ATState)

    strIntelATState = GetIntelATState(oAtpDevice.ATState)
    strEnrollmentStatusDescription = GetEnrollmentStatusDescription(intStateNew, intStateCurrent, intEnrollmentStatus)
    set oAtpDevice = nothing
%>
<script type="text/javascript" language="javascript">

function SetIntelAtpActive() {

	var frm = document.AMSESNDetails;
	frm.hdnSetIntelAtpActiveFlag.value = 1;
	frm.submit();
	return true;
}

function RequestRemoteUnlock() {
	var frm = document.AMSESNDetails;
	frm.hdnRequestRemoteUnlockFlag.value = 1;
	frm.submit();
	return true;
}

</script>
<table>
<tr>
    <td colspan="4">
        <b>Intel AT</b> <% =strIntelAtpReturnMessage %><br />
		<hr />
    </td>
</tr>
<tr>
    <td colspan="4">
        <table cellpadding="5" cellspacing="0" border="1" width="97%">
            <tr>
                <th align="left"></th>
                <th align="left">Current</th>
                <th align="left">... And After Next Call Will Be</th>
            </tr>
            <tr class="coloredRow">
                <td><b>State:</b></td>
                <td><% =GetIntelATState(intCurrentATState) %>
                </td>
                <td><% =GetIntelATPState(intStateNew) %>
                </td>
            </tr>
            <tr>
                <td><b>Lock Request Action:</b></td>
                <td><%=GetAtpActionDescription(intLockAction) %></td>
                <td><% =GetAtpActionDescription(intLockActionNew) %> 
                </td>
            </tr>
            <tr class="coloredRow">
                <td><b>Timer Action:</b></td>
                <td><% =GetAtpActionDescription(intTimerAction) %>
                </td>
                <td><% =GetAtpActionDescription(intTimerActionNew) %>
                </td>
            </tr>
            <tr>
                <td><b>Timer (Days):</b></td>
                <td><%=intTimerDays%>
                </td>
                <td><%=intTimerDaysNew%>
                </td>
            </tr>
            <tr class="coloredRow">
                <td><b>Message:</b></td>
                <td><%=Server.HTMLEncode(strIntelATMessage) %>&nbsp;</td>
                <td><%=Server.HTMLEncode(strIntelATMessageNew) %>&nbsp;
                </td>
            </tr>
        </table>
    </td>

<tr >
    <td colspan="4"><b>Chipset:</b> <% =GetChipTypeDescription(intChipType) %></td>
</tr>
<tr>
    <td colspan="4"><hr /></td>
</tr>
<tr>
    <td colspan="2">&nbsp;</td>
    <td><input type="button" name="btnSetIntelAtpActive" value="Set Intel AT State to 'Active'" onClick="SetIntelAtpActive()" /> </td>
</tr>
<tr>
    <td colspan="2">&nbsp;</td>
    <td colspan="2">

             Unlock Code (nonce):<br />
             <input name="txtUnlockCode" style="WIDTH: 150px" />
             <input type="button" name="btnUnlockDevice" value="Generate Server Recovery Token" onClick="RequestRemoteUnlock()" /> 

    </td>
</tr>
</table>
<%

end if

%>
<input type="hidden" name="hdnSetIntelAtpActiveFlag" value="0" />
<input type="hidden" name="hdnRequestRemoteUnlockFlag" value="0" />
<input type="checkbox" style="display: none;" name="StolenFlag" <%if bStolen = 1 then Response.Write("checked=""checked"" ") end if%> disabled="disabled">

<br><br>
<div class="clearfix"></div>
<div class="profile-action-flag-block">
                    <h4 class="block-label">Profile and action flags</h4>
                    <div class="profile-action-flag-block__content-block col-md-12 col-sm-12 col-xs-12 row">
                        <div class="profile-action-flag-block__left col-md-6 col-sm-6 col-xs-6 row">
                            <%
                            if session("EnterpriseType") = "Absolute" or Session("EnterpriseType") = "Partner" then
                            %>
                                <div class="col-md-12 col-sm-12 col-xs-12 row">
                                    <div class="col-md-8 col-sm-8 col-xs-8"><input name="ippdays" class="form-control col-md-8" type="text"  maxlength="4" value="<%=intIPPeriodDays%>"></div>
                                    <label class="col-md-4 col-sm-4 col-xs-4">IP Period Days:</label>  
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 row">
                                    <div class="col-md-8 col-sm-8 col-xs-8"><input class="form-control col-md-8" type="text" name="newip" maxlength="16" value="<%=strNewIPAddress%>"></div>
                                    <label class="col-md-4 col-sm-4 col-xs-4">New IP Address:</label>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 row">
                                    <div class="col-md-8 col-sm-8 col-xs-8"><input class="form-control col-md-8" type="text" name="newurl" maxlength="64" value="<%=strNewURL%>"></div>
                                    <label class="col-md-4 col-sm-4 col-xs-4">New URL:</label>
                                </div>
                            <%

                            else
                            %>      
                                <div class="col-md-12 col-sm-12 col-xs-12 row">
                                        <input type="hidden" name="CTCFlag" value="<%=bCTCFlag%>" />
                                        <div class="col-md-8 col-sm-8 col-xs-8"><input name="ippdays" class="form-control col-md-8" type="text"  maxlength="4" value="<%=intIPPeriodDays%>"></div>
                                        <label class="col-md-4 col-sm-4 col-xs-4">IP Period Days:</label>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 row">
                                        <div class="col-md-8 col-sm-8 col-xs-8"><input class="form-control col-md-8" type="text" name="newip" maxlength="16" value="<%=strNewIPAddress%>"></div>
                                        <label class="col-md-4 col-sm-4 col-xs-4">New IP Address:</label>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 row">
                                        <div class="col-md-8 col-sm-8 col-xs-8"><input class="form-control col-md-8" type="text" name="newurl" maxlength="64" value="<%=strNewURL%>"></div>
                                        <label class="col-md-4 col-sm-4 col-xs-4">New URL:</label>
                                </div>

                            <%

                            end if
                            %>
                        </div>
                        <div class="profile-action-flag-block__right col-md-6 col-sm-6 col-xs-6">
                            <div class="col-md-12 col-sm-12 col-xs-12 row">
                                
                                <div class="col-md-8 col-sm-8 col-xs-8">
                                    <select class="selectpicker form-control" name="ESN_Type">
                                        <option value="0" <%if intTestInd = 0 then Response.Write "selected=""selected""" end if%>>Normal</option>
                                        <option value="1" <%if intTestInd = 1 then Response.Write "selected=""selected""" end if%>>Test</option>
                                    </select>
                                </div>
                                <label class="col-md-4 col-sm-4 col-xs-4">ESN Type:</label>
                            </div>
                            <%
                            if session("EnterpriseType") = "Absolute" or Session("EnterpriseType") = "Partner" then
                            %>
                                <div class="col-md-12 col-sm-12 col-xs-12 row">
                                    <div class="col-md-8 col-sm-8 col-xs-8"><input name="ippminutes" class="form-control col-md-8" type="text"  maxlength="4" value="<%=intIPPeriodMinutes%>"></div>
                                    <label class="col-md-4 col-sm-4 col-xs-4">IP Period Minutes:</label>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 row">
                                    <div class="col-md-8 col-sm-8 col-xs-8"><input class="form-control col-md-8" type="text" name="newipport" maxlength="5" value="<%=intNewIpPort%>"></div>
                                    <label class="col-md-4 col-sm-4 col-xs-4">New IP Port:</label>
                                </div>
                                <%if Session("EnterpriseType") = "Absolute" then %>
	                            <div class="checkbox col-md-12 col-sm-12 col-xs-12 row">
                                    <label><input type="checkbox" name="CTCFlag" value="1" <%if bCTCFlag then Response.Write "checked=""checked"" " end if%> />CTC Flag</label>
                                </div>
                                <% end if %>
                            <%

                            else
                            %>
                                <div class="col-md-12 col-sm-12 col-xs-12 row">
                                    <div class="col-md-8 col-sm-8 col-xs-8"><input name="ippminutes" class="form-control col-md-8" type="text"  maxlength="4" value="<%=intIPPeriodMinutes%>"></div>
                                    <label class="col-md-4 col-sm-4 col-xs-4">IP Period Minutes:</label>
                                </div>

                            <%

                            end if
                            %>
                            
                        </div>
                    </div>
                    


<% if bFastContactAvailable = 1 then %>
<script type="text/javascript" language="javascript">

    function EnableDisableFcFields() {
        document.AMSESNDetails.fcPeriod.disabled = !(document.AMSESNDetails.chkEnableFastContact.checked);
        document.AMSESNDetails.fcNewUrl.disabled = !(document.AMSESNDetails.chkEnableFastContact.checked);
        document.AMSESNDetails.fcNewIp.disabled = !(document.AMSESNDetails.chkEnableFastContact.checked);
        document.AMSESNDetails.fcNewIpPort.disabled = !(document.AMSESNDetails.chkEnableFastContact.checked);
        $('.ems-quote-block.fast-contact').toggleClass('enabled'); 
    }
</script>

<br/>
    <div class="ems-quote-block fast-contact col-md-12 col-sm-12 col-xs-12 <%if bEnableFastContact then Response.Write("enabled") end if%>">
                                <div class="checkbox">
                                    <label><input type="checkbox" name="chkEnableFastContact" id="chkEnableFastContact" onClick="EnableDisableFcFields()" <%if bEnableFastContact then Response.Write("checked=""true"" ") end if%> />Enable Fast Contact</label>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 row">
                                    <div class="col-md-6 col-sm-6 col-xs-6 row">
                                        <div class="col-md-12 col-sm-12 col-xs-12 row">
                                            <div class="col-md-7 col-sm-7 col-xs-7">
                                                <% WriteFastContactCallPeriodComboControlWithClass "fcPeriod", bEnableFastContact, intFcPeriodSeconds, "", "form-control" %>
                                            </div>
                                            <label class="col-md-5 col-sm-5 col-xs-5">FC Period:</label>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12 row">
                                            <div class="col-md-7 col-sm-7 col-xs-7"><input class="form-control col-md-8" type="text" name="fcNewIp" maxlength="16" value="<%=strNewFcIpAddress%>" <%if bEnableFastContact = 0 then Response.Write("disabled=""true"" ") end if%>/></div>
                                            <label class="col-md-5 col-sm-5 col-xs-5">New FC IP Address:</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 row">
                                        <div class="col-md-12 col-sm-12 col-xs-12 row">
                                            <div class="col-md-8 col-sm-8 col-xs-8"><input class="form-control col-md-8" type="text" name="fcNewUrl" maxlength="64" value="<%=strNewFcUrl%>" <%if bEnableFastContact = 0 then Response.Write("disabled=""true"" ") end if%>></div>
                                            <label class="col-sm-4 col-xs-4">New FC URL:</label>
                                        </div>
                                        
                                        <div class="col-md-12 col-sm-12 col-xs-12 row">
                                            <div class="col-md-8 col-sm-8 col-xs-8"><input class="form-control col-md-8" type="text" name="fcNewIpPort" maxlength="5" value="<%=intNewFcIpPort%>" <%if bEnableFastContact = 0 then Response.Write("disabled=""true"" ") end if%>/></div>
                                            <label class="col-sm-4 col-xs-4">New FC IP Port:</label>
                                        </div>
                                    </div>
                                </div>
                </div>
    <div class="clearfix"></div>
<% end if %>

<!-- Smart Call -->
<% If(bSCSupportOSFlag = 1) Then %>
<script type="text/javascript" language="javascript">
 function EnableDisableSmartCall() {
 		var frm =document.AMSESNDetails;
 		var status = frm.chkSmartCall.checked;

 		frm.ddlSMFrequency.disabled = !frm.chkSmartCall.checked;
 		frm.chkSCLocation.disabled = !frm.chkSmartCall.checked;
 		frm.chkSCSoftware.disabled = !frm.chkSmartCall.checked;
 		frm.chkSCLoggedOn.disabled = !frm.chkSmartCall.checked;
 		frm.chkSCHardware.disabled = !frm.chkSmartCall.checked;
 		frm.chkSCNetwork.disabled = !frm.chkSmartCall.checked;

 		if (frm.chkSCLocation.checked == !status && frm.chkSCSoftware.checked == !status && frm.chkSCLoggedOn.checked == !status && frm.chkSCHardware.checked == !status && frm.chkSCNetwork.checked == !status) {
 		    frm.chkSCLocation.checked = status;
 		    frm.chkSCSoftware.checked = status;
 		    frm.chkSCLoggedOn.checked = status;
 		    frm.chkSCHardware.checked = status;
 		    frm.chkSCNetwork.checked = status;
 		}
        
        $('.ems-quote-block.smart-call').toggleClass('enabled'); 	
    }	

</script>

    <div class="ems-quote-block smart-call col-md-12 col-sm-12 col-xs-12 <%if (bEnableSCFlag = "True") then Response.Write("enabled") end if%>">
                                <div class="checkbox">
                                    <label><input id="chkSmartCall" type="checkbox" name="chkSmartCall"  onClick="EnableDisableSmartCall()"							
							        <%if (bEnableSCFlag = "True") then Response.Write(" checked=""checked"" ")  end if%>>Enable Event Calling</label>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 row">
                                    <div>
                                        <div class="col-md-8 col-sm-8 col-xs-8 row">
                                        <div class="col-md-7 col-sm-7 col-xs-7">
                                             <% WriteSmartCallPeriodControlWithClass "ddlSMFrequency", bEnableSCFlag, intSCPeriodTime, "", "form-control", false %> 
                                        </div>
                                        <label class="col-md-5 col-sm-5 col-xs-5">Minimum Event Call Period:</label>
                                    </div>
                                    </div>
                                    
                                </div>
                                <label class="block-label">Configuration Options</label>
        <div class="col-md-12 row" id="pnSCConfig">
        <div class="col-md-6 row">
            <input id="chkSCLocation" type="checkbox" name="chkSCLocation"
												<%if (bEnableSCLocation > 0 and bEnableSCFlag = "True") then Response.Write(" checked=""checked"" ") end if%>											
                                                <%if bEnableSCFlag <> "True" then Response.Write(" disabled=""true"" ") end if%>
												><label
                                                    for="chkSCLocation">Location changes</label>
													
                                                <div style="padding: 5px 0;">
                                                    <input id="chkSCSoftware" type="checkbox" name="chkSCSoftware"
													<%if (bEnableSCSoftware > 0 and bEnableSCFlag = "True")  then Response.Write(" checked=""checked"" ") end if%>
                                                    <%if bEnableSCFlag <> "True" then Response.Write(" disabled=""true"" ") end if%>
													><label
                                                        for="chkSCSoftware">Software changes</label>
                                                </div>
                                                <input id="chkSCLoggedOn" type="checkbox" name="chkSCLoggedOn"
												<%if (bEnableSCLoggedOn > 0 and bEnableSCFlag = "True")  then Response.Write(" checked=""checked"" ") end if%>	
                                                <%if bEnableSCFlag <> "True" then Response.Write(" disabled=""true"" ") end if%>											
												><label
                                                    for="chkSCLoggedOn">Logged in user changes</label></div>
        <div class="col-md-6 row"><input id="chkSCHardware" type="checkbox" name="chkSCHardware"
												<%if (bEnableSCHardware > 0 and bEnableSCFlag = "True") then Response.Write(" checked=""checked"" ") end if%>	
                                                <%if bEnableSCFlag <> "True" then Response.Write(" disabled=""true"" ") end if%>											
												><label
                                                    for="chkSCHardware">Hardware changes </label>
                                                <div style="padding-top: 5px;">
                                                    <input id="chkSCNetwork" type="checkbox" name="chkSCNetwork"
													<%if (bEnableSCNetwork > 0 and bEnableSCFlag = "True") then Response.Write(" checked=""checked"" ") end if%>	
                                                    <%if bEnableSCFlag <> "True" then Response.Write(" disabled=""true"" ") end if%>												
													><label
                                                        for="chkSCNetwork">Network changes</label>
                                                </div></div>
    </div>
                </div>
    <div class="clearfix"></div>
    
<% End If %>
<!--The End of The Smart Call -->

<%
set rs = rs.NextRecordset

Dim currentActionIdIndex

currentActionIdIndex = 0

Do While Not rs.EOF
    if rs("Id") = ACTION_ENABLE_CM_AND_CELLMODEM_AT then
        Response.Write "<script> var originalValueCM = " + CStr(intAction AND rs("Id")) + "; actcmindex = " + cstr(currentActionIdIndex) + ";</script>"
    end if

    if rs("Id") = ACTION_ALLOW_LOCATION_DETERMINATION then
        Response.Write "<script> var originalValueLD = " + CStr(intAction AND rs("Id"))  + "; actldindex = " + cstr(currentActionIdIndex) + ";</script>"
    end if
    
    if rs("Id") = ACTION_DISABLE_CM_AND_LD then
         Response.Write "<script> var originalValueDisableCMAndLD = " + CStr(intAction AND rs("Id")) + ";</script>"
    else
        'keep the same logic as below displaying action
        currentActionIdIndex =  currentActionIdIndex + 1                     
    end if
    
    rs.MoveNext
loop

rs.MoveFirst
%>
<div class="clearfix"></div>
<hr/>
<div class="esn-detail-action-list col-md-12 row" style="display: none">
	   <%
		currentActionIdIndex = 0

		Do While Not rs.EOF
        %>
        
            <%
		    if rs("ID") = ACTION_DISABLE_CM_AND_LD then
		     If ((intAction AND ACTION_DISABLE_CM_AND_LD) <> 0) Then
				    Response.Write "<div class=""action-type""><input type=""hidden"" name=""DisableCmAndLd"" value=""1"" /></div>"
				Else
				    Response.Write "<div class=""action-type""><input type=""hidden"" name=""DisableCmAndLd"" value=""0"" /></div>"
			    End if

		    else
		%>
			    <div class="action-type"><INPUT TYPE=CHECKBOX NAME=ActionID VALUE=<%=rs("Id")%>
			    <%
                if rs("Id") = ACTION_DISABLE_MODEM_CALLOUT then
                  'If Disable Modem Callout is clicked then uncheck Enable Modem Callout
                  ' Assumes that the following checkbox is for Enable Modem Callout
                  WriteSetPairedActionIdOnClick currentActionIdIndex, currentActionIdIndex+1
			    end if

                if rs("Id") = ACTION_ENABLE_MODEM_CALLOUT then
                  'If Enable Modem Callout is clicked then uncheck Disable Modem Callout
			      ' Assumes that the previous checkbox was for Disable Location Determination              
                  WriteSetPairedActionIdOnClick currentActionIdIndex, currentActionIdIndex-1
			    end if

             '  Not Necessary
             '   if rs("Id") = ACTION_ENABLE_CM_AND_CELLMODEM_AT then
             '     'If this feature has been disabled, set the flag to uninstall it on the client
             '     ' Assumes that the following checkbox is for Enable Location Determination
             '     WriteSetDisableActionIdOnClick currentActionIdIndex, currentActionIdIndex+1
			 '   end if

             '  Not Necessary
              '  if rs("Id") = ACTION_ALLOW_LOCATION_DETERMINATION then
              '    'If this feature has been disabled, set the flag to uninstall it on the client
              '    ' Assumes that the previous checkbox is for Enable MCIC
              '    WriteSetDisableActionIdOnClick currentActionIdIndex-1, currentActionIdIndex
			  '	  
			  '   end if			
			
			    if (( intAction AND rs("Id") ) > 0) then
			        'If the Action bit is set for this action, then default the checkbox to being checked
				    Response.Write " checked=""checked"" "
			    end if
    			
			    'Keep track of the index of the ActionID definition we are currently writing
			    currentActionIdIndex = currentActionIdIndex + 1
			    %>> <a href="javascript:OpenActionWindow(<%=rs("Id")%>)"><%=rs("Description")%></a></div>
			    <%
            
			end if


			rs.MoveNext
		LOOP
		
		If rs.State = adStateOpen Then
			rs.Close
		End If

		set rs = nothing
		set rs2 = nothing
		set cmd = nothing
		set cnnDef = nothing
		set cnnCC = nothing
		set cnnCT = nothing
        set cnnRdir = nothing


Dim strFipsEnabled, strHdnFipsEnabled
strHdnFipsEnabled = ""
If IsFipsEnabledInCRCATPhase2Rem(intCRCATPhase2Rem) = 1 Then
    strFipsEnabled = " checked=""checked"" "
    strHdnFipsEnabled = "on"
End If

Dim strDNSCacheEnabled, strHdnDNSCacheEnabled
strDNSCacheEnabled = ""
strHdnDNSCacheEnabled = ""
If IsDNSCacheEnabledInCRCATPhase2Rem(intCRCATPhase2Rem) = 1   Then
    strDNSCacheEnabled = " checked=""checked"" "
    strHdnDNSCacheEnabled = "on"
End If

Dim strDEATIIEnabled, strHdnDEATIIEnabled
strDEATIIEnabled = ""
strHdnDEATIIEnabled = ""
If IsDEATIIEnabledInCRCATPhase2Rem(intCRCATPhase2Rem) = 1   Then
    strDEATIIEnabled = " checked=""checked"" "
    strHdnDEATIIEnabled = "on"
End If


		%>
        <div class="col-md-12 row"><div class="col-md-6 button-cloned-block row"></div>
        <div class="col-md-6 button-cloned-block row"></div></div>
        <div class="clearfix"></div>
        <hr>
        <div class="col-md-6 button-cloned-block row"></div>
        <div class="action-type">
            <input type="hidden" name="hdnFipsEnabled" value="<%=strHdnFipsEnabled %>" />
        </div>
        
        <div class="action-type">
            <input type="checkbox" <% =strFipsEnabled %> name="chkFipsEnabled"/>
            <a href="javascript:OpenCustomActionWindow('<%=FIPS_DESCRIPTION %>', '<%=FIPS_EXTENDED_DESCRIPTION %>', '<%=FIPS_PLATFORM %>', '<%=FIPS_VERSION %>', '<%=FIPS_FLAG_TYPE %>')"><%=FIPS_DESCRIPTION %>.</a>
        </div>
        
		<div class="action-type">
            <input type="hidden" name="hdnDNSCacheEnabled" value="<%=strHdnDNSCacheEnabled %>" />
        </div>
        <div class="action-type">
            <input type="checkbox" <% =strDNSCacheEnabled %> name="chkDNSCacheEnabled"/>
            <a href="javascript:OpenCustomActionWindow('<%=DNSCACHE_DESCRIPTION %>', '<%=DNSCACHE_EXTENDED_DESCRIPTION %>', '<%=DNSCACHE_PLATFORM %>', '<%=DNSCACHE_VERSION %>', '<%=DNSCACHE_FLAG_TYPE %>')"><%=DNSCACHE_DESCRIPTION %>.</a>
        </div>
        <div class="action-type">
            <input type="hidden" name="hdnDEATIIEnabled" value="<%=strHdnDEATIIEnabled %>" />
        </div>
        <div class="action-type">
            <input type="checkbox" <% =strDEATIIEnabled %> name="chkDEATIIEnabled" />
            <a href="javascript:OpenCustomActionWindow('<%=DEATII_DESCRIPTION %>', '<%=DEATII_EXTENDED_DESCRIPTION %>', '<%=DEATII_PLATFORM %>', '<%=DEATII_VERSION %>', '<%=DEATII_FLAG_TYPE %>')"><%=DEATII_DESCRIPTION %>.</a>
        </div>
		<br>		
		
        
		</div>
        <div class="clearfix"></div>
        <hr>
        <div class="col-md-12 row">
            <input type="button" class="button-submit btn" value="Save changes" name="update2" onClick="UpdateData()">
        </div>
		<div class="clearfix"></div>
</FORM>
</div>
<script type="text/javascript" language="javascript">

    var actionListToOrder = [
        { id: 128,
            description: 'Enable Basic Asset Tracking (ATI)'
        },
        { id: 4096,
            description: 'Enable Advanced Asset Tracking (ATII)'
        },
        { id: 'chkDEATIIEnabled',
            description: 'Delayed ATII Collection'
        },
        { id: 256,
            description: 'Upgrade to the latest RPCNET agent'
        },
        { id: 1024,
            description: 'Security Action Requested (RPCNET)'
        },
        { id: 1073741824,
            description: 'Allow location determination'
        },
        { id: 'chkFipsEnabled',
            description: 'Enable FIPS Certified Encryption'
        },
        { id: 'chkDNSCacheEnabled',
            description: 'Enable DNS Cache'
        },
        { id: '536870912',
            description: 'Enables MC-Initiated Calling and Cellular Modem Asset Tracking.'
        },
        { id: 4194304,
            description: 'Disable persistence'
        },
        { id: 65536,
            description: 'Remove agent'
        },
    ];
    var actionListHidden = ["4", "512", "2048", "32768", "131072", "8388608"];


    waitingJquery(modifyActionList);

    function modifyActionList() {
        var actionWrapper = $('.esn-detail-action-list');
        var actionList = actionWrapper.find('.action-type').toArray();

        actionList.forEach(function (item) {
            if (actionListHidden.includes($(item).find('input').val())) {
                $(item).attr("hidden", "true");
           }
        })



        actionListToOrder.reverse();
        var order = 2;

        actionListToOrder.forEach(function (item) {
            var selector;
            if ($.isNumeric(item.id)) {
                selector = actionWrapper.find("input[value=" + item.id + "]").parent();
            } else {
                selector = actionWrapper.find("input[name=" + item.id + "]").parent();
            }

            var cloneItem = selector.clone();
            selector.addClass('hiddenItem');
            selector.attr("hidden", "true");
            cloneItem.find('a').text(item.description);
            cloneItem.addClass('cloneItem');
            $('.button-cloned-block').eq(order).prepend(cloneItem);
            if (item.id == 4194304 || item.id == 1073741824) {
                order -= 1;
                //actionWrapper.prepend("<hr style=\"width: 362px;margin-left: 0px;border-top-style: solid;border-top-color: rgb(228, 234, 236);border-top-width: 0px;box-sizing: content-box\">");
            }
        })
        actionWrapper.css("display", "block");

        function bindingText() {
            var seperator = $('.show-more-separator');
            var downIcon = "glyphicon ";

            seperator.bind('click', function () {
                if ($(this).text() == "Show more ") {
                    $(this).html("Show less <span class='glyphicon glyphicon-menu-up'></span>")
                } else {
                    $(this).html("Show more <span class='glyphicon glyphicon-menu-down'></span>")
                }


            })

            seperator.delegate('span', 'click', function (e) {
                e.stopPropagation();
                $(this).parent().click();
            })
        }

        waitingJquery(bindingText);

        waitingJquery(statusLabel)

    function statusLabel (){
       var status = '<% =strStatus %>'
      status  = status ? status.toLowerCase() : status;
      switch (status){
        case 'a':
          status = 'Active';
          break;
        case 'i' :
          status = 'Inactive';
          break;
        case 'd' :
          status = 'Disabled';
          break;
        default:
          status = 'N/A';
      }
      $('#classicstatus').text(status);
    }

    }
</script>

<!-- #INCLUDE FILE = "BottomInclude2.asp" -->