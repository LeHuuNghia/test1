<%
'file: Constants.asp
'use:  Contains the constants used for email and database connection

'email variables
const constTechsupportEMail = "tech_support@pd.abt"
const constOrderToEMail = "gmcdougall@pd.abt" 'in AMS = "Fulfillment@absolute.com"
const constOrderCCEMail = "dbeames@pd.abt" 'in AMS = "vvolodarets@absolute.com"
const constLogEmail = "dbeames@pd.abt" 'in AMS = "emaillog@absolute.com"
const constSystemStatusEmail = "tech_support@pd.abt" 'in EMS = "administrator@absolute.com"
const constEMSApi = "https://dv2int1ems1api.dv.absolute.com"
const constEMSApiEUDC = "https://eks2.dv.absolute.com/dds-ems-api"

'database connection
'mssql-persistance.cdwswjp7nuvv.ca-central-1.rds.amazonaws.com
'User ID=rds_ems;Password=Absolute0225$

'const strConn = "Provider=SQLNCLI11;Initial Catalog=CTDATA;Integrated Security=SSPI;Data 'Source=mssql-persistance.cdwswjp7nuvv.ca-central-1.rds.amazonaws.com;DataTypeCompatibility=80;UID=rds_ems;PWD=Absolute0225$"

'const strConn_RedirectionDB = "Provider=SQLNCLI11;Initial Catalog=ACTIVATIONDB;Integrated Security=SSPI;Data 'Source=mssql-persistance.cdwswjp7nuvv.ca-central-1.rds.amazonaws.com;DataTypeCompatibility=80;UID=rds_ems;PWD=Absolute0225$"

const strConn = "Provider=SQLNCLI11;Initial Catalog=CTDATA;Data Source=mssql-persistance.cdwswjp7nuvv.ca-central-1.rds.amazonaws.com;DataTypeCompatibility=80;UID=rds_ems;PWD=Absolute0225$"

const strConn_RedirectionDB = "Provider=SQLNCLI11;Initial Catalog=ACTIVATIONDB;Data Source=mssql-persistance.cdwswjp7nuvv.ca-central-1.rds.amazonaws.com;DataTypeCompatibility=80;UID=rds_ems;PWD=Absolute0225$"

const strSmtpServer = "pdsmail1.absolute.com"
const intSmtpServerPort = 25

const blnUseRsaServerAuthentication = false

'fast contact settings
const bEnableFastContactFeaturesInEms = 1   ' Set to '1' if Fast Contact features should be shown, '0' to prevent this
const strFastContactWebServiceUrl = "http://dv2corp1-fc1:8081/FastContact"    ' The URL of the http interface for the Fast Contact consolidation server. e.g."http://localhost:8081/FastContact", or "" to disable
const strCTMDeviceAdminWebServiceURL = "http://dv2corp1.ctm.service.absolute.com/DeviceAdmin.svc/DeviceAdmin"    ' The URL of the http interface for the CTM Server DeviceAdmin Admin e.g."http://dv2internals2.ctm.server.com/DeviceAdmin.svc/DeviceAdmin"
const intMaxNumDevicesForFcNotification = 100   ' The maximum number of device notifications that we can send before the user should be pre-warned that it may take a long time
const intFastContactNotificationBatchSize = 10 ' The maximum number of device notifications that can be sent as a batch to the consolidation server
const strConsumerAutoLoginUrl = "http://dv2corp1my.absolute.com/home/StaffAutoLogin"     'StaffAutoLogin URL ex: http://dv2internals2my.absolute.com/home/StaffAutoLogin

const strConsMobileAutoLoginUrl="http://dv2corp1my.absolute.com/home/StaffAutoLoginMobile" 'StaffAutoLogin to Mobile URL ex: http://dv2internals2my.absolute.com/home/StaffAutoLoginMobile

const strRSAServiceURL = "http://dv2corp1apiservices2008.absolute.com/RSAServices_1.2/BllServiceHost/RsaAuthenticator.svc/RsaAuthenticator"     ' The URL of the http interface for the RSA Service e.g.http://dv2corp1apiservices2008.absolute.com/RsaServices/BllServiceHost/RsaAuthenticator.svc/RsaAuthenticator"

const strOfflineFreezeBEServiceURL = "http://dv2corp1apiservices2008.absolute.com/DeviceLockServices_1.19/BllServiceHost/OfflineFreezeBEService.svc/OfflineFreezeBEService"     ' The URL of the http interface for the Device Lock Offline Freeze Service e.g.http://dv2corp1apiservices2008.absolute.com/DeviceLockServices_1.18/BllServiceHost/OfflineFreezeBEService.svc/OfflineFreezeBEService"
'UpperCase Mobile OS Flags
const strMobileOSFlags = "I,S,A,B,C"

const bEnableSmartCallFeaturesInEms = 1        'Set to 1 If SmartCalling Features should be shown

const strInternalDomainWebServiceURL = "http://dv2corp3apiservices2008.absolute.com/InternalDomainServices_1.20/BllServiceHost/"

'Display paging information
const intQueryPageSize = 15

const intMaxNumberOfEsnsForRealTimeUpdate = 10000

const bRequireSecondDDSigningOfficer = 0   'Set to 0 if we don't need signing officer

' Dual-User Authentication
const strDualUserAuthenticationForDataDelete = "DUAL_USER_AUTHENTICATION_FOR_DATADELETE"
const strDualUserAuthenticationForDeviceFreeze = "DUAL_USER_AUTHENTICATION_FOR_DEVICEFREEZE"
const strDualUserAuthenticationForAgentRemoval = "DUAL_USER_AUTHENTICATION_FOR_AGENTREMOVAL"
const intMaxNumberOfDualUserAuthentication = 3000
const intAtleastNumberApprovers = 2
'Display EMS current build version format ex: 1.2.3.4
const strEMSVersion = "6.8.0.93"

'Congfiguration of ftp
const strFPTEncryptedLoginID = "ZGV2ZWxvcG1lbnQ="
const strFPTEncryptedPassword = "ZGV2dGVzdDtwYXNz"

'Congfiguration of Hashed Password Login
const strHashedPassword = "True"

'Congfiguration of Account NextGent Url
const strAccountNextGenUrl = "https://eks2api.dv.absolute.com/ctes/"

'const strCADC = "CADC"
const strEUDC = "EUDC"

%>