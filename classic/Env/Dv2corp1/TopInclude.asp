<%@ Language=VBScript %>
<%OPTION EXPLICIT%>
<%
'file: TopInclude.asp
'use:  This page includes the style sheet and the information at the top of all of the pages
'comment

response.buffer = true
Response.ExpiresAbsolute = #May 21, 2000 12:00:00#

'response.Write("Session(""EnterpriseType"") " & Session("EnterpriseType") & "<br />")

if Session("EnterpriseType") = "" then
    if SetEnterpriseType() <> 1 then
        response.Write("<br /><br />The EnterpriseType Environment setting is not set. Please talk to your DBA.<br />" )
        response.End
    end if
end if
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
	<title>EMS - ESN Management System</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<base href="/">
<!-- Set style using CSS -->
<!-- #include file= "cssStyles.asp" -->
<%
'note removed: <!-- #INCLUDE FILE = "includes/Utilities.asp" --> %>
</head>
<body bgcolor="6641a4" background="Images/admin_back.gif" marginwidth=0 leftmargin=0 topmargin=0 marginheight=0>
   <script id="ems-widget-script" src="https://dv2int1ems1app.dv.absolute.com/widget/ems-widget.js"></script>

   <script type="text/javascript" language="javascript">
       var timer;
       function waitingJquery(callback, param) {
           if (typeof $ == 'undefined') {
               timer = setTimeout(function () {
                   waitingJquery(callback, param)
               }, 200)

           } else {
               if (!!param) {
                   callback(param);
               } else {
                   callback();
               }

           }
       }


       function lowerCaseText() {
           $.expr[':'].icontains = function (a, i, m) {
               return jQuery(a).text().toUpperCase()
                             .indexOf(m[3].toUpperCase()) >= 0;
           };
           var accountuidTag = $(".headerPurple:icontains('accountuid')");
           var accountuidText = $(".headerPurple:icontains('accountuid')").text();
           accountuidText = accountuidText ? accountuidText.split(':') : accountuidText;
           accountuidTag.text(accountuidText.length > 1 ? accountuidText[0] + ': ' + accountuidText[1].toLowerCase() : accountuidTag.text());

           var deviceuidTag = $(".headerPurple:icontains('deviceuid')");
           var deviceuidText = $(".headerPurple:icontains('deviceuid')").text();
           deviceuidText = deviceuidText ? deviceuidText.split(':') : deviceuidText;
           deviceuidTag.text(deviceuidText.length > 1 ? deviceuidText[0] + ': ' + deviceuidText[1].toLowerCase() : deviceuidTag.text());
       }

       //This fix Array.prototype.includes does not support in IE11 
       if (!Array.prototype.includes) {
           Object.defineProperty(Array.prototype, "includes", {
               enumerable: false,
               writable: true,
               value: function (searchElement /*, fromIndex*/) {
                   'use strict';
                   var O = Object(this);
                   var len = parseInt(O.length) || 0;
                   if (len === 0) {
                       return false;
                   }
                   var n = parseInt(arguments[1]) || 0;
                   var k;
                   if (n >= 0) {
                       k = n;
                   } else {
                       k = len + n;
                       if (k < 0) { k = 0; }
                   }
                   var currentElement;
                   while (k < len) {
                       currentElement = O[k];
                       if (searchElement === currentElement ||
                        (searchElement !== searchElement && currentElement !== currentElement)) { // NaN !== NaN
                           return true;
                       }
                       k++;
                   }
                   return false;
               }
           });
       }

       document.addEventListener("DOMContentLoaded", function () {
           waitingJquery(lowerCaseText);
       });

       function checkToken() {
           $.ajax({
               url: "<%=constEMSApi%>/v1/authenticate/check",
               xhrFields: {
                   withCredentials: true
               }
           }).success(function () {
               console.log('done')
           }).error(function (e) {
               alert('Token has been expired! Please re-login');
               deleteASPSession();
               location.reload();
           })
       }

       function signin(param) {
           $.ajax({
               method: 'POST',
               url: "<%=constEMSApi%>/v1/authenticate/signin",
               data: { 'username': param.username, 'pwd': param.pwd },
               xhrFields: {
                   withCredentials: true
               }
           }).success(function (data) {
               window.location.href = param.redirectPage;
           }).error(function(error){
                alert(error.responseJSON.data);
                deleteASPSession();
                window.location.href = './EntryAdmin.asp'
           })
       }

       function deleteASPSession() {
           document.cookie.split(";").forEach(function (c) {
               if (c.includes('ASPSESSIONID') || c.includes('token')) {
                   document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/");
               }

           });
       }



    </script>
<table cellpadding="0" cellspacing="0" border="0" width="760">
<tr>
	<td><img src="IMAGES/admin_0.gif" width="23" height="52" alt="" border="0"></td>
	<td background="IMAGES/admin_1.gif" width="543" valign="bottom">
		<%
		if session("EnterpriseType") = "Absolute" then%>		
		<img src="IMAGES/Abs_Horz.gif" height="21" width="146" alt="" border="0"><br>
		<% 
		end if
		
		'If the current page is the Accounts List page then do not link the title to the Accounts List page
		If (InStr(LCase(Request.ServerVariables("PATH_INFO")), "searchlist.asp") > 0 and request.QueryString("SearchForm") = "") or Session("ssl_login") = "" then 
		%>
			&nbsp;<font class=title>ESN Management System</font>
		<% 
		'If the current page is not the Accounts List page then link the title to the Accounts List page
		Else %>
			&nbsp;<a href="SearchList.asp"><font class=TitleUnderline>ESN Management System</font></a>
		<% End If %>
	</td>
	<td background="IMAGES/admin_1.gif" width="50" valign="middle">	
	<img src="IMAGES/powered_by_computrace_logo.gif" width="41" height="43" alt="" border="0">
	</td>
	<form name="search_logout">
	<td background="IMAGES/admin_1.gif" width="1000" align="right">
	<%
	if Session("ssl_login") <> "" then
	%>	
		<font style="line-height:23px">User ID: <%= Session("ssl_login") %><br></font>
		<input type="Button" name="logout_button" value="Log out" style="color:ff0000" onclick="document.location='Logout.asp'">
        <script>
                   document.addEventListener("DOMContentLoaded", function () {
                       waitingJquery(checkToken);
                   });

        </script>
	<%
	else
		response.write "&nbsp;"
	end if
	%>
	</td>
	</form>
	<td><img src="IMAGES/admin_2.gif" width="44" height="52" alt="" border="0"></td>
	</tr><tr>
	<td colspan="5"><img src="Images/admin_5.gif" width="760" height="13" alt="" border="0"></td>
</tr>
</table>

<table width="760" cellpadding="0" cellspacing="0"  border="0">
<tr valign="top">
	<td width="27"><img src="Images/admin_3.gif" width="27" height="489" border="0"></td>
	<td width="733" bgcolor="ffffff" valign="top" align="center">
	<!-- start Content -->
	

