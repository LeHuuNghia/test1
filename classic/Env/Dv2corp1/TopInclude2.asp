<%@ Language=VBScript %>
<%OPTION EXPLICIT%>
<%
'file: TopInclude2.asp
'use:  This page includes the style sheet and the information at the top of all of the pages
'comment

response.buffer = true
Response.ExpiresAbsolute = #May 21, 2000 12:00:00#

'response.Write("Session(""EnterpriseType"") " & Session("EnterpriseType") & "<br />")

if Session("EnterpriseType") = "" then
    if SetEnterpriseType() <> 1 then
        response.Write("<br /><br />The EnterpriseType Environment setting is not set. Please talk to your DBA.<br />" )
        response.End
    end if
end if
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
	<title>EMS - ESN Management System</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<base href="/">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        .hidden{
            visibility: hidden;
        }
    </style>
<!-- Set style using CSS -->
<!-- #include file= "cssStyles.asp" -->
<%
    
'note removed: <!-- #INCLUDE FILE = "includes/Utilities.asp" --> %>
</head>
<body class="hidden ems-ui">
    <script id="ems-widget-script" src="https://dv2int1ems1app.dv.absolute.com/widget/ems-widget.js"></script>

   <script type="text/javascript" language="javascript">
       var timer;
       function waitingJquery(callback, param) {
           if (typeof $ == 'undefined') {
               timer = setTimeout(function () {
                   waitingJquery(callback, param)
               }, 200)

           } else {
               if (!!param) {
                   callback(param);
               } else {
                   callback();
               }

           }
       }

       function lowerCaseText() {
           $.expr[':'].icontains = function (a, i, m) {
               return jQuery(a).text().toUpperCase()
                             .indexOf(m[3].toUpperCase()) >= 0;
           };
           var accountuidTag = $(".headerPurple:icontains('accountuid')");
           var accountuidText = $(".headerPurple:icontains('accountuid')").text();
           accountuidText = accountuidText ? accountuidText.split(':') : accountuidText;
           accountuidTag.text(accountuidText.length > 1 ? accountuidText[0] + ': ' + accountuidText[1].toLowerCase() : accountuidTag.text());

           var deviceuidTag = $(".headerPurple:icontains('deviceuid')");
           var deviceuidText = $(".headerPurple:icontains('deviceuid')").text();
           deviceuidText = deviceuidText ? deviceuidText.split(':') : deviceuidText;
           deviceuidTag.text(deviceuidText.length > 1 ? deviceuidText[0] + ': ' + deviceuidText[1].toLowerCase() : deviceuidTag.text());

           var newDeviceUidTag = $('.device-info__device-uid b').siblings();
           newDeviceUidTag.text(newDeviceUidTag.text().toLowerCase());

       }

       //This fix Array.prototype.includes does not support in IE11 
       if (!Array.prototype.includes) {
           Object.defineProperty(Array.prototype, "includes", {
               enumerable: false,
               writable: true,
               value: function (searchElement /*, fromIndex*/) {
                   'use strict';
                   var O = Object(this);
                   var len = parseInt(O.length) || 0;
                   if (len === 0) {
                       return false;
                   }
                   var n = parseInt(arguments[1]) || 0;
                   var k;
                   if (n >= 0) {
                       k = n;
                   } else {
                       k = len + n;
                       if (k < 0) { k = 0; }
                   }
                   var currentElement;
                   while (k < len) {
                       currentElement = O[k];
                       if (searchElement === currentElement ||
                        (searchElement !== searchElement && currentElement !== currentElement)) { // NaN !== NaN
                           return true;
                       }
                       k++;
                   }
                   return false;
               }
           });
       }

       document.addEventListener("DOMContentLoaded", function () {
           waitingJquery(lowerCaseText);
       });

       function checkToken() {
           $.ajax({
               url: "<%=constEMSApi%>/v1/authenticate/check",
               xhrFields: {
                   withCredentials: true
               }
           }).success(function () {
               console.log('done')
           }).error(function (e) {
               alert('Token has been expired! Please re-login');
               deleteASPSession();
               location.reload();
           })
       }

       function signin(param) {
           $.ajax({
               method: 'POST',
               url: "<%=constEMSApi%>/v1/authenticate/signin",
               data: { 'username': param.username, 'pwd': param.pwd },
               xhrFields: {
                   withCredentials: true
               }
           }).success(function (data) {
               window.location.href = param.redirectPage;
           }).error(function(error){
                alert(error.responseJSON.data);
                deleteASPSession();
                window.location.href = './EntryAdmin.asp'
           })
       }

       function deleteASPSession() {
           document.cookie.split(";").forEach(function (c) {
               if (c.includes('ASPSESSIONID') || c.includes('token')) {
                   document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/");
               }

           });
       }

       function removeHiddenPage(){
        $('body.hidden').removeClass('hidden');
       }

       function setSearchMenuWidth() {
            $('#myMenu').outerWidth($('.search_link').outerWidth());
       }



    </script>
<div class="ems-outer-container">
    <div class="ems-header">
        <% if session("EnterpriseType") = "Absolute" then%>	
        <div class="header-left">
                    <div class="logo"></div>

            <%
            'If the current page is the Accounts List page then do not link the title to the Accounts List page
		    If (InStr(LCase(Request.ServerVariables("PATH_INFO")), "searchlist.asp") > 0 and request.QueryString("SearchForm") = "") or Session("ssl_login") = "" then 
		    %>
			    &nbsp;<span class="title">ESN Management System</span>
		    <% 
		    'If the current page is not the Accounts List page then link the title to the Accounts List page
		    Else %>
			    &nbsp;<span class="title">ESN Management System</span>
		    <% End If %>
            
        </div>
        <% 
		end if %>
        <form name="search_logout" class="ams_user">
	<%
	if Session("ssl_login") <> "" then
	%>	
		<font style="line-height:23px"><label>User ID:</label> <%= Session("ssl_login") %><br></font>
		<div class="logout"><span  onclick="document.location='Logout.asp'">Log out</span></div>
        <script>
                   document.addEventListener("DOMContentLoaded", function () {
                       waitingJquery(checkToken);
                       waitingJquery(removeHiddenPage);
                   });

        </script>
	<%
	else
		response.write "&nbsp;"
	end if
	%>
	<script>
                   document.addEventListener("DOMContentLoaded", function () {
                       waitingJquery(removeHiddenPage);
                   });

        </script>
	</td>
	</form>
    </div>
    <div class="ems-container">
		<%
		if Session("ssl_login") <> "" then %>
        <div class="menu">
          <ul class="menu-list">
                <%
			    if session("EnterpriseType") <> "Customer" then
                %>
                    <li class="list-item" onClick="document.location.href='NewAccount.asp'"><a class="list-item__link" href="NewAccount.asp"><b>New Account</b></a>
                    </li>
			    <%
			    end if
                %>
                    <li class="list-item" onClick="document.location.href='ChangeSettings.asp'"><a class="list-item__link" href="ChangeSettings.asp"><b>Settings</b></a>
                    </li>
                <%
				if session("EnterpriseType") = "Absolute" then 
                %>
                    <li class="list-item" onClick="document.location.href='MapServerList.asp'"><a class="list-item__link" href="MapServerList.asp"><b>Server Map</b></a>
                    </li>
					<li class="list-item search_link" onMouseover="return !showMenuTest(event);">
                        <a class="list-item__link" href="SearchForm.asp"><b>Search</b></a>
                        <div id="myMenu" style="height: 52px; position:absolute; z-index:20; visibility:hidden" onmouseover="event.cancelBubble = true;">
							<table border="1" cellspacing="0" cellpadding="2" borderColor="9999ff" style="width: 100%">
								<tr bgcolor="#CCCCFF">
									<TD valign="top" align="left" onClick="document.location.href='SearchForm.asp'" onMouseOut="this.style.backgroundColor='CCCCFF'" onMouseOver="this.style.backgroundColor='9966FF'"><A href="SearchForm.asp" class="topmenu"><B>Account or ESN</B></A>
									</TD>
								</tr>	
								<tr bgcolor="#CCCCFF">
									<TD valign="top" align="left" onClick="document.location.href='CtpSearch.asp'" onMouseOut="this.style.backgroundColor='CCCCFF'" onMouseOver="this.style.backgroundColor='9966FF'"><A href="CtpSearch.asp" class="topmenu"><B>Lojack</B></A>
									</TD>
								</tr>
                                <tr bgcolor="#CCCCFF">
									<TD valign="top" align="left" onClick="document.location.href='EmailAuditLogs.asp'" onMouseOut="this.style.backgroundColor='CCCCFF'" onMouseOver="this.style.backgroundColor='9966FF'"><A href="EmailAuditLogs.asp" class="topmenu"><B>Email Log</B></A>
									</TD>
								</tr>										
							</table>
							</div>
                    </li>
				<%
				else
				%>
				    <li class="list-item" onClick="document.location.href='SearchForm.asp'"><a class="list-item__link" href="SearchForm.asp"><b>Search</b></a>
                    </li>
                <%
				end if
				%>	

              
          </ul>
        </div>
		<%
				end if
				%>	
<script language="Javascript">
<!--

document.onmouseover = hideAllMenusTest;

function showMenuTest(eventObj) {
	if(document.layers) { // i.e. is this Netscape 4.x?
		myImg = getImage('img'); 
 		x = getImagePageLeft(myImg);
 		y = getImagePageTop(myImg);
 		menuTop = y + 10; // LAYER TOP POSITION
		document.layers["myMenu"].top = menuTop;
 		document.layers["myMenu"].left = x;
	}
	eventObj.cancelBubble = true;
	setSearchMenuWidth();
    if(changeObjectVisibility('myMenu', 'visible')) {
	return true;
    } else {
	return false;
    }
}

function hideAllMenusTest() {
	changeObjectVisibility('myMenu', 'hidden');
}

function hideMenuTest(eventObj) {
	eventObj.cancelBubble = true;
    if(changeObjectVisibility('myMenu', 'hidden')) {
	return true;
    } else {
	return false;
    }
}

function getStyleObject(objectId) {
    // cross-browser function to get an object's style object given its id
    if(document.getElementById && document.getElementById(objectId)) {
	// W3C DOM
	return document.getElementById(objectId).style;
    } else if (document.all && document.all(objectId)) {
	// MSIE 4 DOM
	return document.all(objectId).style;
    } else if (document.layers && document.layers[objectId]) {
	// NN 4 DOM.. note: this won't find nested layers
	return document.layers[objectId];
    } else {
	return false;
    }
} // getStyleObject

function changeObjectVisibility(objectId, newVisibility) {
    // get a reference to the cross-browser style object and make sure the object exists
    var styleObject = getStyleObject(objectId);
    if(styleObject) {
	styleObject.visibility = newVisibility;
	return true;
    } else {
	//we couldn't find the object, so we can't change its visibility
	return false;
    }
} // changeObjectVisibility


function getImagePageLeft(img) {
  var x, obj;
  if (document.layers) {
    if (img.container != null)
      return img.container.pageX + img.x;
    else
      return img.x;
  }
  return -1;
}

function getImagePageTop(img) {
  var y, obj;
  if (document.layers) {
    if (img.container != null)
      return img.container.pageY + img.y;
    else
      return img.y;
  }
  return -1;
}

//-->
</script>
	<!-- start Content -->
	


