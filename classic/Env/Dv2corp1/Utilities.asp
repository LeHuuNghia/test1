<%
'file: utilities.asp
'use:  This file contains some debugging methods and SendMail, used to send an email

'debugging flag, if set to true then display debugging info
dim bDebug
'bDebug = true
bDebug = false

%>
<script language="jscript" runat="server">
    function SortVBArray(arrVBArray) {
        return arrVBArray.toArray().sort().join('\b');
    }
</script>

<script language="javascript" type="text/javascript">

function validate_required(field, alerttxt) {
    with (field) {
        if (value == null || value == "") {
            // only alert if there is message
            if (alerttxt != null) {
                alert(alerttxt);
            }
            return false;
        }
        else {
            return true;
        }
    }
}

function validate_isNumeric(field, alerttxt) {
    with (field) {
        if (value == null || isNaN(value)) {
            // only alert if there is message
            if (alerttxt != null) {
                alert(alerttxt);
            }
            return false;
        }
        else {
            return true;
        }
    }
}

</script>
<%
Public Function SortArray(arrInput)
    SortArray = Split(SortVBArray(arrInput), Chr(8))
End Function


Public Function GetDefaultDBConnectionString()
    GetDefaultDBConnectionString = strConn
End Function

Public Function GetCCDataConnectionString(ByVal lngAccountId, ByVal strEsn)
    GetCCDataConnectionString = GetConnectionString("CCDATA", lngAccountId, strEsn)
End Function

Public Function GetCTDataConnectionString(ByVal lngAccountId, ByVal strEsn)
    GetCTDataConnectionString = GetConnectionString("CTDATA", lngAccountId, strEsn)
End Function

Public Function GetCurrentASPPageName()
   Dim StartTime, EndTime, currPage, intIndex

   If bDebug Then
     StartTime = Timer
   End If
   
   currPage = Request.ServerVariables("SCRIPT_NAME")
   intIndex = InStrRev(currPage, "/")
   
   'update /AccountDetails.asp or /EMS/AccountDetails.asp to AccountDetails.asp
   If intIndex > 0 Then
      currPage = Right(currPage, Len(currPage) - intIndex)
   End If
   
   GetCurrentASPPageName = currPage
   
   If bDebug Then
     EndTime = Timer
     Debug "GetCurrentASPPageName()Execution_Elapse_Time", EndTime - StartTime
   End If

End Function

Public Function GetLtrDBConnectionString()
    GetLtrDBConnectionString = GetConnectionString("LTRDB", 0, Null)
End Function

Public Function GetRedirectionDBConnectionString()
    GetRedirectionDBConnectionString = strConn_RedirectionDB
End Function

Public Function GetTrmsDBConnectionString()
    GetTrmsDBConnectionString = GetConnectionString("TRMSDB", 0, Null)
End Function

Public Function GetUserDBConnectionString()
    GetUserDBConnectionString = GetConnectionString("UserDB", 0, Null)
End Function

Public Function GetArchiveQueryConnectionString()
    GetArchiveQueryConnectionString = GetConnectionString("ARCHIVEQUERY", 0, Null)
End Function

Public Function GetOriDBConnectionString()
    GetOriDBConnectionString = GetConnectionString("OriDB", 0, Null)
End Function

Public Function GetPwsDBConnectionString()
    GetPwsDBConnectionString = GetConnectionString("PWSDB", 0, Null)
End Function

Public Function GetSciDBConnectionString()
    GetSciDBConnectionString = GetConnectionString("PWSDB", 0, Null)
End Function

Public Function GetAccountCreationConnectionString()
    GetAccountCreationConnectionString = GetSiloConnectionString("CTData",null, 1)
End Function

'*******************************************************************************************
' Helper methods for working with stored procs

' Build a connection object using the given database connection string
Private Function BuildConnectionObject(ByRef strConnectionString)
	Dim cnn

    Set cnn = Server.CreateObject("ADODB.Connection")
    With cnn
        .ConnectionTimeout = 120
        cnn.Open(strConnectionString)
    End With

    Set BuildConnectionObject = cnn
End Function


' Build a command object for the named stored procedure
Private Function BuildStoredProcObjectCommon(ByVal strName, ByRef connection)
	Dim cmd

	Set cmd = Server.CreateObject("ADODB.Command")
	With cmd
		.CommandTimeout = 120
		.CommandType = adCmdStoredProc
        .Prepared = True
		.CommandText = strName
		.NamedParameters = True

        Set .ActiveConnection = connection
    End With

    Set BuildStoredProcObjectCommon = cmd
End Function


' Build a command object for the named stored procedure
Private Function BuildStoredProcObjectForExecute(ByVal strName, ByRef connection)
'Private Function BuildStoredProcObject(ByVal strName, ByRef connection)
	Dim cmd

	Set cmd = BuildStoredProcObjectCommon(strName, connection)
	cmd.Parameters.Append cmd.CreateParameter("@ReturnValue", adInteger, adParamReturnValue)

    Set BuildStoredProcObjectForExecute = cmd
End Function


' Build a command object for the named stored procedure
Private Function BuildStoredProcObjectForOpen(ByVal strName, ByRef connection)
	Dim cmd

	Set cmd = BuildStoredProcObjectCommon(strName, connection)

    Set BuildStoredProcObjectForOpen = cmd
End Function


' Execute the given stored proc command object
Private Function ExecuteStoredProc(ByRef cmd)
	Dim intReturnCode

	intReturnCode = 0

    call cmd.Execute(, , adExecuteNoRecords)

	intReturnCode = cmd.Parameters("@ReturnValue").Value

    ExecuteStoredProc = intReturnCode
End Function


'*******************************************************************************************
'strDBName is required
'strSiloCode (optional) - precedence over bitDefaultSilo
'bitDefaultSilo (optional) - 1 (true) default; 0 (false)
'
'*******************************************************************************************
Public Function GetSiloConnectionString(strDBName, strSiloCode, bitDefaultSilo)

   If IsNull(strDBName) Then
        Err.Raise 450 _
            , "Invalid parameter value" _
            , "GetSiloConnectionString strDBName cannot be NULL"
    elseif (len(ltrim(strDBName)) = 0) then
        Err.Raise 450 _
            , "Invalid parameter value" _
            , "GetSiloConnectionString strDBname must have a value"
    End If

    Dim rsConn, strServer, strDatabase, intReturnCode, strConnectionString, strDBPath
    set rsConn = BuildConnectionObject(GetDefaultDBConnectionString())

    Debug "ConnectionString", GetDefaultDBConnectionString()

    Dim rsCmdOwner
    Set rsCmdOwner = BuildStoredProcObjectForExecute("dbo.util_GetSiloDBPath", rsConn)
    With rsCmdOwner
	    .Parameters.Append .CreateParameter("@DBName", adVarWChar, adParamInput, 255, strDBName)
	    .Parameters.Append .CreateParameter("@SiloCode", adChar, adParamInput, 4, strSiloCode)
	    .Parameters.Append .CreateParameter("@DefaultSiloFlag", adBoolean, adParamInput, , bitDefaultSilo)
	    .Parameters.Append .CreateParameter("@DBPath", adVarWChar, adParamOutput, 255)
	    .Parameters.Append .CreateParameter("@DBPath_Server", adVarWChar, adParamOutput, 255)
	    .Parameters.Append .CreateParameter("@DBPath_Database", adVarWChar, adParamOutput, 255)

        intReturnCode = ExecuteStoredProc(rsCmdOwner)
        strServer = .Parameters("@DBPath_Server").Value
        strDatabase  = .Parameters("@DBPath_Database").Value
        strDBPath  = .Parameters("@DBPath").Value

        Set .ActiveConnection = Nothing
    End With

    If rsConn.State = adStateOpen Then rsConn.Close

    set rsCmdOwner = nothing
    set rsConn = nothing

    If (CInt(intReturnCode) > 0 and InStr(strDbPath, ".") > 0) then

        'create connection string
        strConnectionString = "Provider=SQLNCLI11;Initial Catalog=" _
            & strDatabase _
            & ";Data Source=" _
            & strServer _
            & ";UID=rds_ems;PWD=Absolute0225$"

    Else 'unable to find connection string
        strConnectionString = -1

        Err.Raise 507 _
            , "Unable to find server" _
            , strDbPath
    End if

    GetSiloConnectionString = strConnectionString

End Function

Private Function GetConnectionString(ByVal strDataBaseName, ByVal lngAccountId, ByVal strEsn)

    Dim cmd, con, strDataBase, strServer, strDbPath, intReturnCode, strConnectionString ,strTest 

    'first parameter null
    If IsNull(strDataBaseName) Then
        Err.Raise 450 _
            , "Invalid parameter value" _
            , "GetConnectionString([NULL], lngAccountId, strEsn)"
    End If
    
    'Response.write("LeHuuNghia")
    
    'database name not valid
    If StrComp(strDataBaseName, "CallDataDB", vbTextCompare) <> 0 _
        And StrComp(strDataBaseName, "CTData", vbTextCompare) <> 0 _
        And StrComp(strDataBaseName, "LTRCTData", vbTextCompare) <> 0 _
        And StrComp(strDataBaseName, "LtrDB", vbTextCompare) <> 0 _
        And StrComp(strDataBaseName, "CCData", vbTextCompare) <> 0 _
        And StrComp(strDataBaseName, "RedirectionDB", vbTextCompare) <> 0 _
        And StrComp(strDataBaseName, "ArchiveQuery", vbTextCompare) <> 0 _
        And StrComp(strDataBaseName, "TrmsDB", vbTextCompare) <> 0 _
        And StrComp(strDataBaseName, "OriDB", vbTextCompare) <> 0 _
        And StrComp(strDataBaseName, "PwsDB", vbTextCompare) <> 0 _
        And StrComp(strDataBaseName, "SciDB", vbTextCompare) <> 0 _
        And StrComp(strDataBaseName, "UserDB", vbTextCompare) <> 0 _
     Then
        Err.Raise 450 _
            , "Invalid parameter value" _
            , "GetConnectionString([" & strDataBaseName & "], lngAccountId, strEsn)"
    End If

    'account and esn null
    If IsNull(lngAccountId) And IsNull(strEsn) Then
        Err.Raise 450 _
            , "Invalid parameter value" _
            , "GetConnectionString(strDataBaseName, [NULL], [NULL])"
    End If

    'account and esn both not null
    If Not(IsNull(lngAccountId) Or IsNull(strEsn)) Then
        Err.Raise 450 _
            , "Invalid parameter value" _
            , "GetConnectionString(strDataBaseName, [" & lngAccountId & "], [" & strEsn & "])"
    End If

    'account not numeric
    If Not(IsNumeric(lngAccountId) Or IsNull(lngAccountId)) Then
        Err.Raise 450 _
            , "Invalid parameter value" _
            , "GetConnectionString(strDataBaseName, [" & lngAccountId & "], strEsn)"
    End If

    'esn length between 10 and 20
    If Len(strEsn) < 10 Or Len(strEsn) > 20 Then
        Err.Raise 450 _
            , "Invalid parameter value" _
            , "GetConnectionString(strDataBaseName, lngAccountId, [" & strEsn & "])"
    End If

    strConnectionString = ""

    Set con = BuildConnectionObject(GetRedirectionDBConnectionString())
    
    strTest = GetRedirectionDBConnectionString()  
    
    'Response.write("</br>") 
    'Response.write("Utility " & strTest)   
    'Response.write("</br>")    
    'Response.write("DBname " & strDataBaseName)
    'Response.write("</br>") 
    'Response.write("ESN " & strEsn)
    'Response.write("</br>")
    'Response.write("Account " & " " & lngAccountId)
    
    
    
    Set cmd = BuildStoredProcObjectForExecute("dbo.util_getDBPath", con)
    With cmd
	    .Parameters.Append .CreateParameter("@account_id", adInteger, adParamInput, , lngAccountId)
	    .Parameters.Append .CreateParameter("@esn", adVarChar, adParamInput, 20, strEsn)
	    .Parameters.Append .CreateParameter("@DBName", adVarWChar, adParamInput, 255, strDataBaseName)
	    .Parameters.Append .CreateParameter("@DBPath", adVarWChar, adParamOutput, 255)

        intReturnCode = ExecuteStoredProc(cmd)
	    strDbPath = .Parameters("@DBPath").Value

	    Set .ActiveConnection = Nothing
    End With

    If con.State = adStateOpen Then con.Close()

    Set cmd = Nothing
    Set con = Nothing
    
    'Response.write("</br>")
    'Response.write("DataBasePath :" & strDbPath)
    'Response.write("</br>")
    'Response.write("-----------------------------------------------------------------------")
    
    If CInt(intReturnCode) > 0 and InStr(strDbPath, ".") > 0 then

        strDbPath = Trim(strDbPath)

        'create connection string
        'TODO Nghia ReModify
        'strConnectionString = "Provider=SQLNCLI11;Initial Catalog=" _
        '    & Right(strDbPath, Len(strDbPath) - InStr(strDbPath, ".")) _
        '    & ";Data Source=" _
        '    & Left(strDbPath, InStr(strDbPath, ".")-1)
        
        strConnectionString = "Provider=SQLNCLI11;Initial Catalog=" _
            & Right(strDbPath, Len(strDbPath) - InStr(strDbPath, ".")) _
            & ";Data Source=mssql-persistance.cdwswjp7nuvv.ca-central-1.rds.amazonaws.com" _
            & ";UID=rds_ems;PWD=Absolute0225$"
        
    Else 'unable to find connection string
        strConnectionString = -1

        Err.Raise 507 _
            , "Unable to find server" _
            , "GetConnectionString([" & strDataBaseName & "], [" & lngAccountId & "], [" & strEsn & "])"
    End if

    GetConnectionString = strConnectionString

End Function


Function SetEnterpriseType()

    dim cmd, con, strEnterpriseType, intReturnCode

    'get enterprise flag
    Set con = BuildConnectionObject(strConn)
    Set cmd = BuildStoredProcObjectForExecute("dbo.ems_getEnterpriseType", con)

    With cmd
	    .Parameters.Append .CreateParameter("@EnterpriseType_out", adVarWChar, adParamOutput, 50)

	    intReturnCode = ExecuteStoredProc(cmd)
	    strEnterpriseType = .Parameters("@EnterpriseType_out").Value

	    Set .ActiveConnection = Nothing
    End With

    If con.State = adStateOpen Then con.Close()

    Set cmd = Nothing
    Set con = Nothing

    if (intReturnCode = 1) then
	    Session("EnterpriseType") = strEnterpriseType
	    SetEnterpriseType = 1
	else
	    SetEnterpriseType = -1
    end if

End Function


'Return the user name of the currently logged in user
Function GetLoggedInUser()
    GetLoggedInUser = trim(session("ssl_login"))
End Function


'Return the string with any single quotes converted to two consecutive single quotes
'Used to prepare string field data for embedding in SQL queries
'NB: Would be better to pass values in through query parameters instead
Function DoubleUpQuotes(strSource)
    DoubleUpQuotes = replace(strSource, "'", "''")
End Function


' Return a string suitable for embedding in an SQL query string
'NB: Would be better to pass values in through query parameters instead
Function SanitizeStringForSQL(strSource)
    SanitizeStringForSQL = trim(replace(strSource, "'", "''"))
End Function


'takes a string and returns it, with all special characters changed to html values
Function MakeHTMLValue(szVal)
         Dim i
         Dim szRet, ch
         for i = 1 to Len(szVal)
            ch = Mid(szVal, i, 1)
            if ch = " " Then
               szRet = szRet & "%20"
            elseif ch = "&" Then
               szRet = szRet & "%26"
            elseif ch = "#" Then
               szRet = szRet & "%23"
            elseif ch = """" Then
               szRet = szRet & "%22"
            elseif ch = ";" Then
               szRet = szRet & "%3B"
            elseif ch = ":" Then
               szRet = szRet & "%3A"
            elseif ch = "'" Then
               szRet = szRet & "%27"
            elseif ch = "%" Then
               szRet = szRet & "%25"
            elseif ch = "+" Then
               szRet = szRet & "%2B"
            elseif ch = "^" Then
               szRet = szRet & "%5E"
            else
               szRet = szRet & Mid(szVal, i, 1)
            end if
         next
      MakeHTMLValue = szRet
End Function

'writes out a label and it's value
'writes out a label and it's value
Function Debug (strLabel, strVar)
	if bDebug then
		Response.write "<br>"& strLabel & ": " & strVar & "<br><br>"
	end if
End Function

'writes out an error message
Function DebugErr ()
	Response.write "<br>Error" & err.number & "-" & err.description & vbcrlf
End Function

Function DebugEnd ()
	Response.End
End Function

'writes out the flds in a record set

'writes out the flds in a record set
Function DebugObject (ByVal rs)
	if bDebug and not(rs.eof) then
			Dim fld, intRec

		'if there is only one record
		Response.Write "r count: " & rs.RecordCount & "<br>"

		if rs.RecordCount = 1 then

			Response.Write "one record or less<br>"
			for each fld in rs.fields
				Response.Write "FieldName: " & fld.Name & " Value: " & fld.Value & "<br>"
			next
		'if there is more than one record
		else
			Response.Write "More than one record<br>"
			For intRec = 1 to rs.RecordCount
				'print out all fields in record
				for each fld in rs.fields
					Response.Write "FieldName: " & fld.Name & " Value: " & fld.Value & "<br>"
				next
			'get next record
			rs.MoveNext
			Response.Write "<br>"
			next
			'move back to first record
			rs.MoveFirst
		end if
	end if
End Function


REM *** Generic function to send email from a page ***
Function SendMail( strFromAddr, strToAddr, strCCAddr, strBCCAddr, strSubject, strMessage )

	Dim objSendMail
	set objSendMail = server.CreateObject("CDO.Message")
	objSendMail.From = strFromAddr
	objSendMail.To = strToAddr
	objSendMail.Cc = strCCAddr
	objSendMail.Subject = strSubject
	objSendMail.TEXTBody = strMessage

	if objSendMail.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") <> 1 then
		objSendMail.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
		objSendMail.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = strSmtpServer
		objSendMail.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = intSmtpServerPort
		objSendMail.Configuration.Fields.Update
	end if

	on error resume next
	objSendMail.Send
	if Err.number <> 0 then
		SendMail = "send email failed: " & Err.Description
	else
		SendMail = 0
	end if
	set objSendMail = nothing

End Function

Public Function GetIntelATPState(intIntelAtpState)

        Select Case intIntelAtpState
          Case 0
            GetIntelATPState = "Intel AT Off"
          Case 1
            GetIntelATPState = "Active"
          Case 2
            GetIntelATPState = "Locked"
          Case Else
            GetIntelATPState = "N/A"
        End Select

End Function

Public Function GetIntelATState(intATState)

        Select Case intATState
          Case 0
            GetIntelATState = "Unknown"
          Case 1
            GetIntelATState = "Intel AT Off"
          Case 2
            GetIntelATState = "Intel AT On"
          Case 3
            GetIntelATState = "Activating Intel AT"
          Case 4
            GetIntelATState = "Deactivating Intel AT"
          Case 5
            GetIntelATState = "Lock Requested"
          Case 6
            GetIntelATState = "Locked"
          Case 7
            GetIntelATState = "Unlock Requested"
          Case Else
            GetIntelATState = "N/A"
        End Select

End Function


Public Function GetChipTypeDescription(ByVal intChipType)

        Select Case intChipType
          Case 0
            GetChipTypeDescription = "Unknown"
          Case 1
            GetChipTypeDescription = "Montevina"
          Case 2
            GetChipTypeDescription = "Calpella"
          Case 3
            GetChipTypeDescription = "Huron River"
		  Case 4
			GetChipTypeDescription = "Chief River"
          Case else
            GetChipTypeDescription = "N/A"
        End Select

End Function

Public Function GetAtpActionDescription(ByVal intAtpAction)

        Select Case intAtpAction
          Case 0
            GetAtpActionDescription = "Do nothing"
          Case 1
            GetAtpActionDescription = "Lock Immediately"
          Case 2
            GetAtpActionDescription = "Reboot Lock"
          Case else
            GetAtpActionDescription = "N/A"
        End Select

End Function

Public Function GetEnrollmentStatusDescription(intStateNew,intStateCurrent,intEnrollmentStatus)

    If intStateNew = 1 And intStateCurrent = 0 And intEnrollmentStatus = 1 Then
        GetEnrollmentStatusDescription = "Enrolling"
    ElseIf intStateCurrent = 0 And intEnrollmentStatus = 2 Then
        GetEnrollmentStatusDescription = "De-enrolled"
    Else
        GetEnrollmentStatusDescription = "Enrolled"
    End If

End Function

Public Function GetAtpDeviceByEsn(ByVal strEsn)

    On Error Resume Next

    Dim objComFactory, strError, result, args(0)
    set objComFactory = Server.CreateObject("AbsoluteSoftware.ComFactory")

    args(0) = strEsn

    debug "Function", "GetAtpDeviceByEsn"
    debug "strESN", strESN

    Dim v
    v = args

    set result = objComFactory.ExecuteMethod("BasicHttpBinding_IIntelAtpBE","GetAtpDeviceByEsn",v)

    debug "result", result

    If isEmpty(result) Then
        set GetAtpDeviceByEsn = Nothing
    Else
        set GetAtpDeviceByEsn = result
    End If

    If Err.Number <> 0 Then
        strError = objComFactory.ExceptionInformation
    End If

    On Error Goto 0

    If strError <> "" Then
        debug "BasicHttpBinding_IIntelAtpBE.GetAtpDeviceByEsn", strError
    End If

    Set objComFactory = Nothing

End Function

Public Function SetAtpStatusActive(ByVal strEsn, ByVal intAccountId)

    On Error Resume Next

    Dim objComFactory, strError, result, args(4), intStatus, esns(0)
    set objComFactory = Server.CreateObject("AbsoluteSoftware.ComFactory")
    intStatus = 1   'Active
    esns(0) = strEsn

    args(0) = CLng(intAccountId)
    args(1) = true
    args(2) = esns
    args(3) = intStatus
    args(4) = true

    debug "Function", "SetAtpStatusActive"
    debug "strESN", strESN
    debug "intAccountId", intAccountId
    debug "strDummyEmsUserUid", strDummyEmsUserUid
    debug "intStatus", intStatus

    Dim v
    v = args

    result = objComFactory.ExecuteMethod("BasicHttpBinding_IIntelAtpBE","SetAtpStatus",v)


    SetAtpStatusActive = result

    If Err.Number <> 0 Then
        strError = objComFactory.ExceptionInformation
    End If

    On Error Goto 0

    If strError <> "" Then
        debug "BasicHttpBinding_IIntelAtpBE.SetAtpStatus", strError
    End If

    Set objComFactory = Nothing

End Function

Public Function AtpServerRecoveryToken( ByVal strEsn, ByVal strUnlockCode)


    On Error Resume Next

    Dim objComFactory, strError, result, args(1)
    set objComFactory = Server.CreateObject("AbsoluteSoftware.ComFactory")

    args(0) = strEsn
    args(1) = strUnlockCode

    debug "Function", "AtpServerRecoveryToken"
    debug "strESN", strESN
    debug "strUnlockCode", strUnlockCode

    Dim v
    v = args

    result = objComFactory.ExecuteMethod("BasicHttpBinding_IIntelAtpBE","GetServerRecoveryToken",v)

    debug "result", result
    AtpServerRecoveryToken = result

    If Err.Number <> 0 Then
        strError = objComFactory.ExceptionInformation
    End If

    On Error Goto 0

    If strError <> "" Then
        debug "BasicHttpBinding_IIntelAtpBE.AtpServerRecoveryToken", strError
    End If

    Set objComFactory = Nothing

End Function

Public Function GetIntelAtpUserAuthentication(ByVal intAccountID)
   On Error Resume Next

    Dim objComFactory, strError, result, args(1)
    Set objComFactory = CreateObject("AbsoluteSoftware.ComFactory")

    args(0) = CLng(intAccountID)
    args(1) = true

    debug "Function", "GetIntelAtpUserAuthentication"
    debug "intAccountID", intAccountID

    Dim v
    v = args

    result = objComFactory.ExecuteMethod("CCAuthenticatorBE","GetUserAuthentication",v)

    debug "result", result
    GetIntelAtpUserAuthentication = result

    If Err.Number <> 0 Then
        strError = objComFactory.ExceptionInformation
    End If

    On Error Goto 0

    If strError <> "" Then
        debug "CCAuthenticatorBE.GetUserAuthentication", strError
    End If

    Set objComFactory = Nothing

End Function

Public Function SetIntelAtpUserAuthentication(ByVal intAccountID, ByVal intIntelAtpUserAuthentication)
   On Error Resume Next

    Dim objComFactory, strError, result, args(3)
    set objComFactory = Server.CreateObject("AbsoluteSoftware.ComFactory")

    args(0) = CLng(intAccountID)
    args(1) = true
    args(2) = CInt(intIntelAtpUserAuthentication)
    args(3) = true

    debug "Function", "SetIntelAtpUserAuthentication"
    debug "intAccountID", intAccountID
    debug "intIntelAtpUserAuthentication", intIntelAtpUserAuthentication

    Dim v
    v = args

    'public void SetUserAuthentication(int accountId, bool accountIdSpecified, IntelAtpUserAuthentication userAuthentication, bool userAuthenticationSpecified);
    result = objComFactory.ExecuteMethod("CCAuthenticatorBE","SetUserAuthentication",v)

    debug "result", result

    If Err.Number <> 0 Then
        strError = objComFactory.ExceptionInformation
    End If

    On Error Goto 0

    If strError <> "" Then
        'Err.Raise 999, "CCAuthenticatorBE.SetIntelAtpUserAuthentication", strError
        debug "CCAuthenticatorBE.SetIntelAtpUserAuthentication", strError
        SetIntelAtpUserAuthentication = -1
    End If

    Set objComFactory = Nothing
    SetIntelAtpUserAuthentication = 1

End Function

Public Function HasRemoteFileRetrievalSetting(ByVal intAccountID)
   On Error Resume Next

    Dim objComFactory, strError, result, args(2), strSettingValue, blnReturn
    Set objComFactory = CreateObject("AbsoluteSoftware.ComFactory")

    args(0) = CLng(intAccountID)
    args(1) = true
    args(2) = "FILERETRIEVALFLAG"

    debug "Function", "GetCorporateAccountSettingByName"
    debug "intAccountID", intAccountID

    Dim v
    v = args

    Set result = objComFactory.ExecuteMethod("CorporateAccountSettingBE","GetSettingByName",v)

    debug "result", result

    debug "result.SettingValue", result.settingValue
    debug "result.SettingName", result.SettingName
    debug "result.IsActive", result.IsActive
    debug "result.IsActiveSpecified", result.IsActiveSpecified

    strSettingValue = result.SettingValue
    debug "strSettingValue", strSettingValue

    Select Case strSettingValue
        Case "on"
            blnReturn = True
        Case "off"
            blnReturn = False
        Case Else
            blnReturn = True
     End Select

    HasRemoteFileRetrievalSetting = blnReturn

    If Err.Number <> 0 Then
        strError = objComFactory.ExceptionInformation
    End If

    On Error Goto 0

    If strError <> "" Then
        debug "CorporateAccountSettingBE.GetSettingByName", strError
    End If

    Set objComFactory = Nothing
    Set result = Nothing
    debug "end", "HasRemoteFileRetrievalSetting"

End Function

Function SetRemoteFileRetrievalSetting(ByVal intAccountID, ByVal strSettingValue)
   On Error Resume Next

    debug "Function", "SetRemoteFileRetrievalSetting"
    debug "intAccountID", intAccountID
    debug "strSettingValue", strSettingValue

    Dim objComFactory, strError, result, args(3), objCorpAccountSetting
    Set objComFactory = CreateObject("AbsoluteSoftware.ComFactory")
    Set objCorpAccountSetting = objComFactory.CreateParameter("CorporateAccountSettingBE","Save","corporateAccountSetting")

    debug "objCorpAccountSetting", objCorpAccountSetting

    objCorpAccountSetting.IsActive = True
    objCorpAccountSetting.IsActiveSpecified = True
    objCorpAccountSetting.SettingName = "FILERETRIEVALFLAG"
    objCorpAccountSetting.SettingValue = strSettingValue

    args(0) = CLng(intAccountID)
    args(1) = true
    Set args(2) = objCorpAccountSetting
    args(3) = "EMS"


    'dim parm
'set parm = obj.CreateParameter(?TheClassName?, ?TheMethodName?, ?TheParameterName?)



    Dim v
    v = args

    result = objComFactory.ExecuteMethod("CorporateAccountSettingBE","Save",v)


    If Err.Number <> 0 Then
        strError = objComFactory.ExceptionInformation
    End If

    On Error Goto 0

    If strError <> "" Then
        debug "CorporateAccountSettingBE.Save", strError
    End If

    Set objComFactory = Nothing

    debug "end", "SetRemoteFileRetrievalSetting"

End Function


' Return the given string padded with leading "0"'s up to the given maximum
Private Function PadWithLeadingZeros(ByRef strToPad, ByVal iMaxLen)
    PadWithLeadingZeros = Right(String(iMaxLen, "0") & strToPad, iMaxLen)
End Function


' Return the given server's current UTC date & time
Public Function GetServerDateTimeUTC(ByRef strConnection)
    Dim rsData

    Set rsData = Server.CreateObject("ADODB.Recordset")
    rsData.Open "select GetUtcDate() as ServerTimeUTC", strConnection, adOpenForwardOnly, adLockReadOnly, adCmdText

    GetServerDateTimeUTC = rsData("ServerTimeUTC")
End Function


' Writes out the HTML code to display a forward/back page control
Public Sub WritePagingControl(ByVal iCurPage, ByVal iPageCount, ByRef iPagesPerGroup, ByRef strPageName, ByRef strPageParams)

    Dim iStartPageCurrGroup, iPrevGroupPage, iNextGroupPage, iLastPageFirstGroup, iLastGroupStartPage, iGroupPage

    ' The first page for the group being shown
	iStartPageCurrGroup = Clng(iCurPage / iPagesPerGroup - 0.51) * iPagesPerGroup

    ' The first page for the group before this one (if any)
    iPrevGroupPage = iStartPageCurrGroup - (iPagesPerGroup - 1)
    if (iPrevGroupPage < 1) then iPrevGroupPage = 1

    ' The first page for the group after this one (if any)
    iNextGroupPage = iStartPageCurrGroup + (iPagesPerGroup + 1)
    if (iNextGroupPage > iPageCount) then iNextGroupPage = iStartPageCurrGroup + 1

    ' The last page for the first group
    iLastPageFirstGroup = iPagesPerGroup

    ' The first page of the last group
    iLastGroupStartPage = iPagesPerGroup * (iPageCount - 1)
    if (iLastGroupStartPage < 1) then iLastGroupStartPage = 1
    
    ' Write move to first '<<'
	If iCurPage = 1 Then
		Response.Write "&nbsp; <SPAN> &lt;&lt; </SPAN>"
	Else
		Response.Write "&nbsp;<A HREF=" & strPageName & "?PAGE=1" & strPageParams & "><b> &lt;&lt; </b></A>"
	End If

    ' No group movement if only 1 group..
    If iPageCount > iPagesPerGroup Then
        ' Write move to previous group '<'
	    If iCurPage <= iLastPageFirstGroup Then
		    Response.Write "&nbsp; <SPAN> &lt; </SPAN>"
	    Else
		    Response.Write "&nbsp; <A HREF=" & strPageName & "?PAGE=" & iPrevGroupPage & strPageParams & "><b> &lt; </b></A>"
	    End If
    End If

    ' Write each page in this group
	For iGroupPage = iStartPageCurrGroup + 1 to (iStartPageCurrGroup + iPagesPerGroup)

		if iGroupPage <= iPageCount Then
			If iGroupPage = iCurPage Then
				Response.Write "&nbsp; <SPAN class='curr_page'>" & iGroupPage & "</SPAN>"
			Else
				Response.Write "&nbsp; <A HREF=" & strPageName & "?PAGE=" & iGroupPage & strPageParams & ">" & iGroupPage & "</A>"
			End If
		End If
	Next

    ' No group movement if only 1 group..
    If iPageCount > iPagesPerGroup Then
        ' Write move to start of next group '>'
	    If iCurPage >= iLastGroupStartPage Then
		    Response.Write "&nbsp; <SPAN> &gt; </SPAN>"
	    Else
		    Response.Write "&nbsp;<A HREF=" & strPageName & "?PAGE=" & iNextGroupPage & strPageParams & "><b> &gt; </b></A>"
	    End If
    End If

    ' Write move to end '>>'
	If iCurPage >= iPageCount Then
		Response.Write "&nbsp; <SPAN> &gt;&gt; </SPAN>"
	Else
	    Response.Write "&nbsp; <A HREF=" & strPageName & "?PAGE=" & iPageCount & strPageParams & "><b> &gt;&gt; </b></A>"
	End If
End Sub

Function GetSiloCodes()

    Debug "GetSiloCodes()", "Start"




    Dim cmd, con, rs, intReturnCode

    Set cmd = Server.CreateObject("ADODB.Command")
    Set con = Server.CreateObject("ADODB.Connection")
    Set rs = Server.CreateObject("ADODB.RecordSet")




	Call con.Open(GetRedirectionDBConnectionString())	
	Set cmd = Server.CreateObject("ADODB.Command")
	With cmd
		.CommandTimeout = 120
		.CommandType = adCmdStoredProc
		.CommandText = "dbo.ems_getWeb_MapSiloList"
		.NamedParameters = True
		.Parameters.Append .CreateParameter("@ReturnValue", adInteger, adParamReturnValue)

		Set .ActiveConnection = con
		rs.CursorLocation = adUseClient
				
		Call rs.Open(cmd, , adOpenForwardOnly, adLockReadOnly)	
		intReturnCode = .Parameters("@ReturnValue").Value
		Set .ActiveConnection = Nothing

	End With

    Dim intRec, silos()

    ReDim silos(rs.RecordCount - 1)
    For intRec = 0 To rs.RecordCount - 1
        silos(intRec) = rs("SiloCode")
        rs.MoveNext
    Next

    GetSiloCodes = silos

	If rs.State = adStateOpen Then
		rs.Close
	End If 
	If con.State = adStateOpen Then
        con.Close
    End If
    set cmd = nothing	

    Debug "GetSiloCodes()", "End"

End Function




'-- Call dbo.GetEsnStateData to get PcMacFlag: P, M, L, I, A etc
'It uses global var: strCCConnString
Public Function GetOSflagByEsn(ByVal strEsn, ByRef osflag)

	Dim cnnCC, cmd, intReturnCode

	intReturnCode = 0
	
    If len(strCCConnString)=0 Then
        strCCConnString = GetCCDataConnectionString(NULL, strESN)
    End if

    Set cnnCC = BuildConnectionObject(strCCConnString)
	
    Set cmd = BuildStoredProcObjectForExecute("dbo.GetEsnStateData", cnnCC)
	With cmd
        .Parameters.Append .CreateParameter("@Esn", adVarChar, adParamInput, 20, strEsn)
		.Parameters.Append .CreateParameter("@PCMacFlag_out", adChar, adParamOutput, 1)

        intReturnCode = ExecuteStoredProc(cmd)
        osflag = cmd("@PCMacFlag_out")

		Set .ActiveConnection = Nothing
	End With

    If cnnCC.State = adStateOpen Then cnnCC.Close

    Set cmd = nothing
    Set cnnCC = nothing

	GetOSflagByEsn = intReturnCode
End Function


' Check whether current ESN is Mobile Device
' Note that it uses global var:strMobileOSFlags defined in constants.asp
Public Function IsMobileDevice(ByVal strEsn)
    Dim intReturnCode, osFlag
    
    Dim isMobile
    isMobile = false
    osFlag = ""
    
    intReturnCode = GetOSflagByEsn(strEsn,osflag)
    If intReturnCode >0 Then
        If len(osflag)>0 Then
            'if locates in Mobile OS Flags
            If Instr(strMobileOSFlags, Ucase(osflag))>0 Then   
                isMobile = true    
            End if 
        End if 
    End If
    
    IsMobileDevice = isMobile

End Function

'-- Call dbo.ems_getAccountDataCenterById to get nextGenUrl
Public Function GetAccountDataCenterByAccountId(ByVal accountId)

	Dim cnnCT, cmd, intReturnCode, strCTConnString, ngURL, dataCenter

	intReturnCode = 0
	
    dataCenter = ""

    strCTConnString = GetCTDataConnectionString(accountId, NULL)
   

    Set cnnCT = BuildConnectionObject(strCTConnString)
	
    Set cmd = BuildStoredProcObjectForExecute("dbo.ems_getAccountDataCenterById", cnnCT)
	With cmd
        .Parameters.Append .CreateParameter("@accountId", adInteger, adParamInput, 255, accountId)
		.Parameters.Append .CreateParameter("@ngURL",  adVarWChar, adParamOutput, 2000)

        intReturnCode = ExecuteStoredProc(cmd)
        ngURL = cmd("@ngURL")

		Set .ActiveConnection = Nothing
	End With

    If InStr(ngURL, strAccountNextGenUrl) And strAccountNextGenUrl <> "" Then
        dataCenter = strEUDC
    End If

    If cnnCT.State = adStateOpen Then cnnCT.Close

    Set cmd = nothing
    Set cnnCT = nothing

	GetAccountDataCenterByAccountId = dataCenter
End Function

%>


