<!-- #INCLUDE FILE = "TopInclude2.asp" -->
<!-- #INCLUDE FILE = "adovbs.inc" -->
<!-- #INCLUDE FILE = "conn_exp.asp" -->
<!-- #include file = "includes/constants.asp" -->
<!-- #include file = "includes/Utilities.asp" -->
<!-- #include file = "includes/Geolocation.asp" -->
<!-- #include file = "includes/FastContact.asp" -->
<!-- #include file = "includes/Fips.asp" -->
<!-- #include file = "includes/CRCATPhase2Rem.asp" -->
<!-- #include file = "includes/SmartCall.asp" -->
<!-- #include file = "includes/Actions.asp" -->
<!-- #include file = "includes/CCUsers.asp" -->
<!-- #include file = "includes/Emsapi.asp" -->

<!-- #include file = "includes/LimitedAccess.asp" -->


<%
'file: AccountDetails.asp
'use: This file is used to display the details for each account
Response.Charset = "utf-8" 
Session.CodePage = 65001
Dim AccountID, AccountUID, AccountName, AccountTypeID, rsData, rsData2, strProc, strQuote, AccountEmail, address1, address2
Dim PhoneAC, PhoneNumber, city, stateDescription, rs, cmd, intReturnCode, strMessage, intIsLojackSilo
Dim creationDate, lastUpdateDate, lastUpdateUser, FaxAC, FaxNumber, postalCode
Dim SpecialInstructions, RecoveryInstructions, ServiceID, state_id, country_id, ErrDesc, intDDAuthorizationId
Dim strServiceName, strAccountTypeDescription
Dim strDDAvailable, intDDAuthorizationId_out, intDDReturnCode, strGeoLocationEnabled, strFastContactEnabled
Dim strDefConnString, strCCConnString, strCTConnString
Dim cnnDef, cnnCC, cnnCT, intTimeZoneOffset, strLocaleCode, CountryCode
Dim strMoveStatus, dtCreatedTS, strCreatedBy, address3, address4, lastUpdateDateUTC, creationDateUTC
Dim strPhoneExt, strPhoneCountryCode, strFaxCountryCode, strSpecInst, strSpecInstUID, strRecInst, strRecInstUID
Dim uidContactName, bFastContactAvailable
Dim intAccountSF
Dim	intSCPeriodTime, bEnableSCFlag,  bEnableSCLocation, bEnableSCSoftware, bEnableSCLoggedOn, bEnableSCHardware, bEnableSCNetwork, intBits, bSCAvailableFlag, intSCApplyForAllDevices, strMessageSC
Dim  bSaveEnableSCFlag,  intSaveSCPeriodTime, bSaveSCFlag, intSaveBits
Dim strUserDBConnection
Dim intDDNumberForDualUser, intDFNumberForDualUser, intARNumberForDualUser, bEnableDDApproval, bEnableDFApproval, bEnableARApproval, bEnableApprovalFeauture, strValueDualUser, intApprovers
Dim strUpdateContactNameUID
dim intCallDataMapServerId, strCallDataName, strCallDataServer, strCallDataDBName
dim intCCDataMapServerId, strCCDataName, strCCDataServer, strCCDataDBName, strSiloName
dim cnnRdir, strRdirConnString
dim dataCenter, accnextGenURL, disabled
intBits = 0
bSCAvailableFlag = 0
intSCPeriodTime = CONST_MINIMUM_PERIOD_TIME
bEnableSCLocation = 0
bEnableSCSoftware = 0
bEnableSCLoggedOn = 0
bEnableSCHardware = 0
bEnableSCNetwork = 0 	
intSCApplyForAllDevices = CONST_SMARTCALL_ALL_DEVICES
bSaveSCFlag = false
intSaveBits = 0
intDDNumberForDualUser = 0
intDFNumberForDualUser = 0
intARNumberForDualUser = 0
bEnableDDApproval = false
bEnableDFApproval = false
bEnableARApproval = false
bEnableApprovalFeauture = false
intApprovers = 0

'get AccountID from query string - used when AccountID comes from other pages
if Request.QueryString("AccID") <> "" then
	AccountID = Request.QueryString("AccID")

'or get AccountID from form - used when AccountID comes from an update from this page
elseif Request.form("AccountID") <> "" then
	AccountID = Request.form("AccountID")
end if

'set up database conections
Set rsData = Server.CreateObject("ADODB.Recordset")
rsData.CursorLocation=adUseClient

Set cnnDef = Server.CreateObject("ADODB.Connection")
Set cnnCC = Server.CreateObject("ADODB.Connection")
Set cnnCT = Server.CreateObject("ADODB.Connection")
Set cnnRdir = Server.CreateObject("ADODB.Connection")

cnnDef.ConnectionTimeout = 120
cnnCC.ConnectionTimeout = 120
cnnCT.ConnectionTimeout = 120
cnnRdir.ConnectionTimeout = 120

strDefConnString = GetDefaultDBConnectionString()
strCCConnString = GetCCDataConnectionString(AccountID, NULL)
strCTConnString = GetCTDataConnectionString(AccountID, NULL)
strUserDBConnection = GetUserDBConnectionString()
strRdirConnString = GetRedirectionDBConnectionString()

Debug "GetDefaultDBConnectionString()", strDefConnString
Debug "GetCCDataConnectionString()", strCCConnString
Debug "GetCTDataConnectionString()", strCTConnString

'Call cnnDef.Open(strDefConnString) -- Uncomment this when connection command objects are implemented

'Response.write("ActivationDB: " &strDefConnString)
'Response.write("</br>")
'Response.write("CTDATA: " & strCTConnString)
'Response.write("</br>")
'Response.write("CCDATA: " &strCCConnString)



Call cnnCC.Open(strCCConnString)
'Call cnnCT.Open(strCTConnString) -- Uncomment this when connection command objects are implemented

'Check access level
strProc = "exec dbo.ems_confirmAccess @AmsUser = N'" & DoubleUpQuotes(GetLoggedInUser()) _
	& "', @AccountID=" & AccountID & ", @pageName = N'" & getCurrentASPPageName() & "'"
rsData.Open strProc, strDefConnString, adOpenForwardOnly, adLockReadOnly, adCmdText

ErrDesc = rsData("ErrDesc")

'close ems_confirmAccess
rsData.close

if ErrDesc <> "OK" then
	Response.Redirect "NoAccess.asp"
end if

Call cnnRdir.Open(strRdirConnString)

set rs = nothing
set cmd = nothing

Set rs = Server.CreateObject("ADODB.RecordSet")

Set cmd = Server.CreateObject("ADODB.Command")
With cmd
	.CommandTimeout = 120
	.CommandType = adCmdStoredProc
	.CommandText = "dbo.ems_getWeb_MapAccountDetails"
	.NamedParameters = True
	.Parameters.Append .CreateParameter("@ReturnValue", adInteger, adParamReturnValue)
	.Parameters.Append .CreateParameter("@ams_login", adVarWChar, adParamInput, 50, session("ssl_login"))
	.Parameters.Append .CreateParameter("@mapAccountId", adInteger, adParamInput, , AccountID)

    .Parameters.Append .CreateParameter("@SiloName_out", adVarWChar, adParamOutput, 30, strSiloName)
    .Parameters.Append .CreateParameter("@CallDataMapServerId_out", adInteger, adParamOutput, , intCallDataMapServerId)
    .Parameters.Append .CreateParameter("@CallDataServer_out", adVarWChar, adParamOutput, 128, strCallDataServer)
    .Parameters.Append .CreateParameter("@CCDataMapServerId_out", adInteger, adParamOutput, , intCCDataMapServerId)
    .Parameters.Append .CreateParameter("@CCDataServer_out", adVarWChar, adParamOutput, 128, strCCDataServer)

    
	Set .ActiveConnection = cnnRdir
	rs.CursorLocation = adUseClient
		
	Call rs.Open(cmd, , adOpenForwardOnly, adLockReadOnly)	
	intReturnCode = .Parameters("@ReturnValue").Value
	
    strSiloName = .Parameters("@SiloName_out").Value
    intCallDataMapServerId = .Parameters("@CallDataMapServerId_out").Value
    strCallDataServer = .Parameters("@CallDataServer_out").Value
    intCCDataMapServerId = .Parameters("@CCDataMapServerId_out").Value
    strCCDataServer = .Parameters("@CCDataServer_out").Value

    Set .ActiveConnection = Nothing
	
	Select Case intReturnCode
		Case 1: 'Success
			'do nothing
		Case -1: 'Failure
			strMessage = strMessage & "<font class=""failure"">You are not allowed to view information about this account.</font>"		      			
		Case Else: 'Unknown failure
			strMessage = strMessage & "The operation failed unexpectedly ("& intReturnCode &"). Please contact website administrator."
	End Select

End With

If rs.State = adStateOpen Then
    rs.Close
End If

'Get MoveLockStatus
'Retrieve MoveLock data as appropriate
Set cmd = Server.CreateObject("ADODB.Command")
With cmd
	.CommandTimeout = 120
	.CommandType = adCmdStoredProc
	.CommandText = "dbo.ems_getMoveLockData"
	.NamedParameters = True
	.Parameters.Append .CreateParameter("@ReturnValue", adInteger, adParamReturnValue)
	.Parameters.Append .CreateParameter("@ESN", adVarChar, adParamInput, 20, vbNull)
	.Parameters.Append .CreateParameter("@AccountID", adInteger, adParamInput, , AccountID)
    .Parameters.Append .CreateParameter("@Status", adVarChar, adParamOutput, 30, strMoveStatus)
    .Parameters.Append .CreateParameter("@CreatedBy", adVarWChar, adParamOutput, 100, strCreatedBy)	
    .Parameters.Append .CreateParameter("@CreatedTS", adDate, adParamOutput, , dtCreatedTS)	
  
	Set .ActiveConnection = cnnCC
	rsData.CursorLocation = adUseClient
	Call rsData.Open(cmd, , adOpenForwardOnly, adLockReadOnly)

	intReturnCode = .Parameters("@ReturnValue").Value
	Set .ActiveConnection = Nothing

End With
set cmd = nothing

'get account count
Set cmd = Server.CreateObject("ADODB.Command")
With cmd
	.CommandTimeout = 120
	.CommandType = adCmdStoredProc
	.CommandText = "dbo.ems_DDCheckForPreAuthorization"
	.NamedParameters = True
	.Parameters.Append .CreateParameter("@ReturnValue", adInteger, adParamReturnValue)
    .Parameters.Append .CreateParameter("@AccountId", adInteger, adParamInput, 255, AccountID)
	.Parameters.Append .CreateParameter("@DDAuthorizationId_out", adInteger, adParamOutput)

	Set .ActiveConnection = cnnCC
	rsData.CursorLocation = adUseClient

    Call rsData.Open(cmd, , adOpenForwardOnly, adLockReadOnly)

	intDDReturnCode = .Parameters("@ReturnValue").Value
	intDDAuthorizationId_out = .Parameters("@DDAuthorizationId_out").Value

    debug "intDDAuthorizationId_out", intDDAuthorizationId_out

	Set .ActiveConnection = Nothing
End With

If rsData.State = adStateOpen Then
	rsData.Close
End If

'Get DDS6 Device Freeze Setting
Dim DDS6FDSetting
'DDS6FDSetting = GetDeviceFreezeSetting(AccountID)

'run ems_getCCAccounts (account_id) to get the Customer Center accounts from ssl_user with the given account_id
Dim webloginDic ' Key:UserUID --Value:webloginID
intReturnCode = GetCCUsersInCCDB(webloginDic)
Select Case intReturnCode
	Case 1: 'Success
		'Do nothing. We are in an CCDATA database.
		intIsLojackSilo = 0
		
	Case -1 'Error
	    ' SSL_USER table does not exist in database, which implies we are in an LTRDB.
	    intIsLojackSilo = 1
	Case Else: 'Unknown failure
		intIsLojackSilo = -1
		strMessage = strMessage & "The operation [ems_getCCAccounts] failed unexpectedly ("& intReturnCode &").  Please contact website administrator"
End Select

'disable web accounts for this account if status has been set to disabled
if Request.Form("DisableCCAccounts") = "true" then

'	'run ems_disableCCAccounts
'	strProc = "exec dbo.ems_disableCCAccounts @accountID=" & request.form("AccountID") _
'	    & " , @ams_login = N'" & DoubleUpQuotes(GetLoggedInUser()) & "'"
'	rsData.Open strProc, strCCConnString, adOpenForwardOnly, adLockReadOnly, adCmdText
'
'	'close ems_disableCCAccounts
'	rsData.close
	
	'Add Disable each customer center users if this account has
	ChangeWebUsersStatus AccountID, 0

end if

' Is the fast contact feature available for this account in this environment?
bFastContactAvailable = IsFastContactSupportedForAccount(AccountID)

' Getting the smart call for account
bSCAvailableFlag = IsSmartCallSupportedForAccount(AccountID)

' Dual-User Authentication
strProc = "exec dbo.GetApprovers  @Account_Id="  & AccountID
rsData.Open strProc, strCCConnString, adOpenForwardOnly, adLockReadOnly, adCmdText
If Not rsData Is Nothing Then
    If rsData.recordcount > 0 Then
        rsData.MoveFirst
        While Not rsData.EOF And intApprovers <= intAtleastNumberApprovers
            intApprovers = intApprovers + 1                
            rsData.MoveNext
            wend
    End If
End If

'close GetApprovers
If rsData.State = adStateOpen Then
	rsData.Close
End If

If intApprovers >= intAtleastNumberApprovers Then
    bEnableApprovalFeauture = true
Else 
    bEnableApprovalFeauture = false
End If 

'If this page was submitted to "save changes" (ifupdate=true) then save the account changes
If request.form("ifupdate")="true" then

   'Enable custom center user status as well
    If (Request.Form("acc_type") = "1") And (Request.Form("OldAccountTypeID") <> "1") Then
    
    	ChangeWebUsersStatus AccountID, 1
    End If
     
    If (trim(request.Form("hdnUidContactName")) = "") Then
		strUpdateContactNameUID = ","
	Else
		strUpdateContactNameUID = " , @ContactNameUID='" & trim(request.Form("hdnUidContactName")) & "'," 
	End If


  
	'set stored procedure variables
	strProc = "exec dbo.ems_updateAccount" _
		& "   @AccountID=" & trim(request.form("AccountID")) _
		& ",  @AccountType=" & trim(request.form("acc_type")) _
		& ",  @AccountName=N'" & SanitizeStringForSQL(Trim(request.form("account_name"))) _
		& "', @Address1=N'" & SanitizeStringForSQL(request.form("Address1")) _
		& "', @Address2=N'" & SanitizeStringForSQL(request.form("Address2")) _
		& "', @City=N'" & SanitizeStringForSQL(request.form("city")) _
		& "', @state_id='" & SanitizeStringForSQL(request.form("state")) _
		& "', @countryCode='" & SanitizeStringForSQL(request.form("country")) _
		& "', @zip=N'" & SanitizeStringForSQL(request.form("zip")) _

		& "', @PhoneNumber=N'" & trim(request.form("phonenumber")) _

		& "', @Faxnumber=N'" & SanitizeStringForSQL(request.form("faxnumber")) _
		& "', @EMail=N'" & SanitizeStringForSQL(request.form("email")) _
		& "', @SpecialInstructions=N'" & SanitizeStringForSQL(Left(request.form("special_instr"),499)) _
		& "', @RecoveryInstructions=N'" & SanitizeStringForSQL(Left(request.form("recovery_instr"),499)) _
		& "', @LastUpdateUser='" & DoubleUpQuotes(GetLoggedInUser()) _
		& "', @Service_id=" & trim(request.form("service")) _
		& " , @Address3=N'" & trim(request.form("Address3")) _
		& "', @Address4=N'" & trim(request.form("Address4")) _
		& "', @LocaleCode='" & trim(request.form("localeCode")) _
		& "', @TimeZoneOffset=" & trim(request.form("timezone")) _
		& strUpdateContactNameUID _
		& "   @FaxCountryCode='" & trim(request.Form("faxcountrycode")) _ 
		& "', @PhoneCountryCode='" & trim(request.Form("phonecountrycode")) _ 
		& "', @PhoneExtension='" & trim(request.Form("phoneext")) & "'"
	'run ems_updateAccount (account_id, account_type_id, name, address1, address2, city, state,
		'country_id, zip, telephoneAC, telephone_no, faxAC, fax_no, email, special_instructions,
		'recovery_instrutions, last_update_user, service_id) to update the account information in the account table
	rsData.Open strProc, strCTConnString, adOpenForwardOnly, adLockReadOnly, adCmdText

	'close ems_updateAccount
	If rsData.State = adStateOpen Then
	    rsData.Close
    End If       

   'Save Dual User Authentication 
    If Request.Form("chkDDNumberForDualUser") = "1" Then
        bEnableDDApproval = true
    Else
        bEnableDDApproval = false 
    End If
      
    If Request.Form("chkDFNumberForDualUser") = "1" Then
        bEnableDFApproval = true
    Else
        bEnableDFApproval = false 
    End If
       
    If Request.Form("chkARNumberForDualUser") = "1" Then
        bEnableARApproval = true
    Else
        bEnableARApproval = false 
    End If  

    If (bEnableApprovalFeauture = true And Cbool(Request.Form("hdnEnableApproval")) = true) Or  _
    (bEnableDDApproval = false And bEnableDFApproval = false And bEnableARApproval = false And bEnableApprovalFeauture = false) Then  
        strValueDualUser = Request.form("txtDDNumberForDualUser")
        If strValueDualUser = "" Then
            intDDNumberForDualUser = 0
        Else
            intDDNumberForDualUser = Cint(strValueDualUser)
            If intDDNumberForDualUser < 0 Or intDDNumberForDualUser > intMaxNumberOfDualUserAuthentication Then
                intDDNumberForDualUser = 0
            End If
        End If
            
        strValueDualUser = Request.form("txtDFNumberForDualUser")
        If strValueDualUser = "" Then
            intDFNumberForDualUser = 0
        Else
            intDFNumberForDualUser = Cint(strValueDualUser)
            If intDFNumberForDualUser < 0 Or intDFNumberForDualUser > intMaxNumberOfDualUserAuthentication Then
                intDFNumberForDualUser = 0
            End If
        End If
        
        strValueDualUser = Request.form("txtARNumberForDualUser")
        If strValueDualUser = "" Then
            intARNumberForDualUser = 0
        Else
            intARNumberForDualUser = Cint(strValueDualUser)
            If intARNumberForDualUser < 0 Or intARNumberForDualUser > intMaxNumberOfDualUserAuthentication Then
                intARNumberForDualUser = 0
            End If
        End If        

        strProc = "exec dbo.SaveCCAccountSettingForDualUserAuthentication" _
		    & "   @Account_Id=" & trim(request.form("AccountID")) _
		    & ",  @SettingDDCode='" & strDualUserAuthenticationForDataDelete & "'" _
		    & ",  @SettingDDValue=" & Cstr(intDDNumberForDualUser) _
            & ",  @SettingDDActiveFlag=" & CByte(bEnableDDApproval) _
		    & ",  @SettingDFCode='" & strDualUserAuthenticationForDeviceFreeze & "'" _
		    & ",  @SettingDFValue=" & Cstr(intDFNumberForDualUser) _
            & ",  @SettingDFActiveFlag=" & CByte(bEnableDFApproval) _
            & ",  @SettingARCode='" & strDualUserAuthenticationForAgentRemoval & "'" _
		    & ",  @SettingARValue=" & Cstr(intARNumberForDualUser) _
            & ",  @SettingARActiveFlag=" & CByte(bEnableARApproval) _
            & ",  @CreatedBy='ems'" _
            & ",  @ChangedBy='ems'" 

	    rsData.Open strProc, strCCConnString, adOpenForwardOnly, adLockReadOnly, adCmdText
	
        'close SaveCCAccountSettingForDualUserAuthentication
	    If rsData.State = adStateOpen Then
	        rsData.Close
        End If
    End If

    If Request.Form("chkEnableGeoLocationTracking") <> Request.Form("hdnGeoLocationEnabled") Then
        'If there is a change, we shall Save setting of the Geolocation flag
        SetGeolocationEnabledInCC trim(request.form("AccountID")), trim(request.form("chkEnableGeoLocationTracking"))
	End If
    
    ' Save setting of the Fast Contact flag
    If bFastContactAvailable = 1 then
        SetFastContactEnabledInCcForAccount trim(request.form("AccountID")), _ 
                                            trim(request.form("chkEnableFastContact"))
    End If

     ' Save setting of Smart Calling     
    If bSCAvailableFlag = 1 then
       If Request.Form("chkSmartCall") = "" Then       		
		bSaveEnableSCFlag = 0
		intSaveSCPeriodTime = CONST_MINIMUM_PERIOD_TIME
		intSaveBits = 0		
	Else
		bSaveEnableSCFlag = 1		
		intSaveSCPeriodTime =  Request.Form("ddlSMFrequency")
		
		If Request.Form("chkSCLocation") <> "" Then 
			intSaveBits = intSaveBits + CONST_SMARTCALL_LOCATION		  
		End If
		
		If Request.Form("chkSCSoftware") <> "" Then 
		    intSaveBits = intSaveBits + CONST_SMARTCALL_SOFTWARE
		End If
		
		If Request.Form("chkSCLoggedOn") <> "" Then 
			intSaveBits = intSaveBits + CONST_SMARTCALL_LOGGEDON 
		End If
		
		If Request.Form("chkSCHardware") <> "" Then   
			intSaveBits = intSaveBits + CONST_SMARTCALL_HARDWARE 
		End If
		
		If Request.Form("chkSCNetwork") <> "" Then 
			intSaveBits = intSaveBits + CONST_SMARTCALL_NETWORK 
		End If

        If Request.Form("chkApplyForAllDevices") <> "" Then             
			intSCApplyForAllDevices = CONST_SMARTCALL_TURNED_ON_DEVICES
		End If
	End If

    If(Request.Form("hdEnableSCFlag") = "True") Then
        bEnableSCFlag = 1
    Else
        bEnableSCFlag = 0
    End If

     If(Request.Form("hdintSCPeriodTime") <> "") Then
        intSCPeriodTime = Request.Form("hdintSCPeriodTime")
    End If

     If(Request.Form("hdintBits") <> "") Then
        intBits = Request.Form("hdintBits")
    End If

    If (bSaveEnableSCFlag <> bEnableSCFlag Or Cint(intSCApplyForAllDevices) = CONST_SMARTCALL_TURNED_ON_DEVICES) Then
        bSaveSCFlag = true
    Else 
        If CStr(intSCPeriodTime) <> CStr(intSaveSCPeriodTime) Or CStr(intBits) <> CStr(intSaveBits)  Then
                bSaveSCFlag = true       
        End If
    End If

    If bSaveSCFlag = true Then    
	    intReturnCode = SetSmartCallSettingsForAccount(trim(request.form("AccountID")), bSaveEnableSCFlag, intSaveSCPeriodTime, intSaveBits, intSCApplyForAllDevices)  
	    Select Case (intReturnCode)
		    Case 1: 'Success
			    'do nothing  
            Case 2: 'Success with cmdqueue
			    strMessageSC = "<font class=failure> The change in Call Settings is being processed. It may take a few minutes to complete. </font><br>"
		    Case -1: 'Failure
			    strMessage = strMessage & "<font class=failure>The operation [SetSmartCallSettingsForAccount] failed: " & rs("err_desc") & "</font><br>"
		    Case Else: 'Unknown failure
			    strMessage = strMessage & "<font class=failure>The operation [SetSmartCallSettingsForAccount] failed unexpectedly ("& intReturnCode &").  Please contact website administrator</font><br>"
	    End Select
    End If

    End If  
   

    debug "ddlIntelATSecurityGating", request.Form("ddlIntelATSecurityGating")
    Dim intSetIntelAtpUserAuthenticationReturn
    
    If request.Form("ddlIntelATSecurityGating") <> request.Form("hdnIntelATSecurityGating") Then
        intSetIntelAtpUserAuthenticationReturn = SetIntelAtpUserAuthentication( trim(request.form("AccountID")), request.Form("ddlIntelATSecurityGating") )
    End If

    If request.Form("chkFipsEnabled") <> request.Form("hdnFipsEnabled") Then
         SetFipsEnabledForAccount trim(request.form("AccountID")), trim(request.form("chkFipsEnabled"))
         SetFipsEnabledForAccountEsns trim(request.form("AccountID")), trim(request.form("chkFipsEnabled"))
    End If
    
    
    If request.Form("chkDNSCacheEnabled") <> request.Form("hdnDNSCacheEnabled") Then
         SetDNSCacheEnabledForAccount trim(request.form("AccountID")), trim(request.Form("chkDNSCacheEnabled"))
    End If
    
    If request.Form("chkDEATIIEnabled") <> request.Form("hdnDEATIIEnabled") Then
         SetDEATIIEnabledForAccount trim(request.form("AccountID")), trim(request.Form("chkDEATIIEnabled"))
    End If
    
    
    
    debug "hdnRemoteFileRetrievalEnabled", request.Form("hdnRemoteFileRetrievalEnabled")
    debug "chkRFR", request.Form("chkRFR")
    If request.Form("chkRFR") <>request.Form("hdnRemoteFileRetrievalEnabled") Then
       Dim strRFRSettingValue, intSetRFRSettingValueReturn
        
       If request.Form("chkRFR") = "on" Then
            strRFRSettingValue = "on"
       Else
            strRFRSettingValue = "off"
       End If
       
       intSetRFRSettingValueReturn = SetRemoteFileRetrievalSetting(AccountID, strRFRSettingValue)
    
    End If
	
	Response.Redirect "AccountDetails.asp?AccID=" + AccountID
	
end if

'run ems_getAccountDetails(account_id)
strProc = "exec dbo.ems_getAccountDetails @accountID="  & AccountID
rsData.Open strProc, strCTConnString, adOpenForwardOnly, adLockReadOnly, adCmdText

'debugobject(rsData)

'If the record set is not empty then set variables to display account details
If Not rsData.EOF Then
	AccountName = rsData("account_name")
    AccountUID = rsData("AccountUId")
    AccountUID = replace(AccountUID, "{", "")
    AccountUID = replace(AccountUID, "}", "")
	AccountTypeID = rsData("account_type_id")
	state_id = rsData("state_id")
	'country_id = rsData("country_id")
	CountryCode = rsData("CountryCode")
	'creationDate = rsData("creation_date")
	creationDateUTC = rsData("creation_dateUTC")
	lastUpdateDateUTC = rsData("last_update_dateUTC")
'	lastUpdateDate = rsData("last_update_date")
	lastUpdateUser = rsData("last_update_user")
'	AccountEmail = rsData("email")
	'PhoneAC = trim(rsData("TelephoneAC"))
'	PhoneNumber = trim(rsData("telephone_no"))
'	'FaxAC = trim(rsData("FaxAC"))
'	FaxNumber = trim(rsData("fax_no"))
	address1 = rsData("address1")
	address2 = rsData("address2")
	address3 = rsData("address3")
	address4 = rsData("address4")
	city = rsData("city")
'	stateDescription = rsData("state_desc")
	postalCode = rsData("zip")
'	SpecialInstructions = rsData("SpecialInstructions")
'	RecoveryInstructions = rsData("RecoveryInstructions")
	ServiceID = rsData("service_id")
	strServiceName = rsData("ServiceName")
	strAccountTypeDescription = rsData("account_type_desc")
	strDDAvailable = rsData("DDAvailable")
	intTimeZoneOffset = rsData("TimeZoneOffset")
	strLocaleCode = rsData("LocaleCode")
	intAccountSF = rsData("AccountServiceFlags")
	
'If there is no account with the given account_id then print out error message
'If this ever happens there is a SERIOUS PROBLEM, because the accountID is forwarded from other pages
else
	Response.write "<br><font class=failure>Internal EMS error: Account " & AccountID & " can not be found</font><br>"
End If

'close ems_getAccountDetails
rsData.close

strProc = "exec dbo.ems_getPrimaryContact @account_id="  & AccountID
rsData.Open strProc, strCTConnString, adOpenForwardOnly, adLockReadOnly, adCmdText

If Not rsData.EOF Then

    AccountEmail = rsData("email")
    PhoneNumber = rsData("phone")
    FaxNumber = rsData("fax")
    strPhoneExt = rsData("phone_ext")
    strPhoneCountryCode = rsData("PhoneCountryCallingCode")
    strFaxCountryCode = rsData("FaxCountryCallingCode")
    uidContactName = rsData("ContactNameUID")

End If

If rsData.State = adStateOpen Then
	rsData.Close
End If

' Dual-User Authentication 

strProc = "exec dbo.GetCCAccountSettingForDualUserAuthentication @Account_Id="  & AccountID
rsData.Open strProc, strCCConnString, adOpenForwardOnly, adLockReadOnly, adCmdText
   If Not rsData Is Nothing Then
        If rsData.recordcount > 0 Then
            rsData.MoveFirst
            While Not rsData.EOF
                If(rsData("SettingCode") = strDualUserAuthenticationForDataDelete) Then
                    intDDNumberForDualUser = rsData("SettingValue")
                    bEnableDDApproval = CBool(rsData("ActiveFlag"))
                 End If

                If(rsData("SettingCode") = strDualUserAuthenticationForDeviceFreeze) Then
                    intDFNumberForDualUser = rsData("SettingValue")
                    bEnableDFApproval = CBool(rsData("ActiveFlag"))
                End If

                If(rsData("SettingCode") = strDualUserAuthenticationForAgentRemoval) Then
                    intARNumberForDualUser = rsData("SettingValue")
                    bEnableARApproval = CBool(rsData("ActiveFlag"))
                End If

                rsData.MoveNext
             wend
        End If
    End If

rsData.Close

dataCenter = GetAccountDataCenterByAccountId(AccountID)
If dataCenter = strEUDC Then
disabled = "disabled"
End If

'Get the Geolocation Tracking feature's status for this account
Dim strHdnGeoLocationEnabled
strGeoLocationEnabled = " "
strHdnGeoLocationEnabled=""
If IsGeolocationEnabledInCcForAccount(AccountID) = 1 Then 
    strGeoLocationEnabled = " checked='on' "
    strHdnGeoLocationEnabled = "on"
End If 

If bFastContactAvailable = 1 then
    'Get the Fast Contact feature's status for this account
    strFastContactEnabled = " "
    if IsFastContactEnabledInCcForAccount(AccountID) = 1 then strFastContactEnabled = " checked='on' "
End If

If bSCAvailableFlag = 1 Then
     intReturnCode = GetSmartCallSettingsForAccount(AccountID, bEnableSCFlag, intSCPeriodTime, bEnableSCLocation, bEnableSCSoftware, bEnableSCLoggedOn, bEnableSCHardware, bEnableSCNetwork, intBits)
     
    If intReturnCode <> 1 then
       strMessage = strMessage & "The operation [GetSmartCallSettingsForAccount] failed unexpectedly ("& intReturnCode &").  Please contact website administrator<br>"
    End If
End If

Dim intIntelAtpUserAuthentication
intIntelAtpUserAuthentication = GetIntelAtpUserAuthentication(AccountID)

%>

<%
'-- Use Global Variable AccountID, strCCConnString
Private Function GetCCUsersInCCDB(ByRef myDic)

    Dim iretCode
    Dim lrsData, lcmd
    
    Dim lconnection
    Set lconnection = Server.CreateObject("ADODB.Connection")
    lconnection.ConnectionString = strCCConnString 
    lconnection.open 
    
    Set lrsData = Server.CreateObject("ADODB.Recordset")
    Set lcmd = Server.CreateObject("ADODB.Command")
    With lcmd
	    .CommandTimeout = 120
	    .CommandType = adCmdStoredProc
	    .CommandText = "dbo.ems_getCCAccounts"
        .NamedParameters = True
	    .Parameters.Append .CreateParameter("@ReturnValue", adInteger, adParamReturnValue)
        .Parameters.Append .CreateParameter("@AccountId", adInteger, adParamInput, 255, AccountID)
        
	    Set .ActiveConnection = lconnection
    End With
    
    With lrsData
        .CursorLocation = adUseClient
	    .Open lcmd, , adOpenForwardOnly, adLockReadOnly
    End With	    
    
    iretCode = lcmd.Parameters("@ReturnValue").Value
  
    If iretCode = 1 Then
        'Success
        Set lrsdata.ActiveConnection = Nothing
        If lrsdata.recordcount >0 Then
            
            Set myDic = Server.CreateObject("Scripting.Dictionary")
            
            lrsdata.MoveFirst
            While Not lrsdata.EOF 
                myDic.Add lrsdata("UserUID").value, lrsdata("WebLoginID").value
                
                lrsdata.MoveNext
            Wend
            
        End If  
        
        Set lrsdata = nothing
    End If  
    
    lconnection.close
    Set lcmd = nothing
    Set lconnection = nothing
    
    GetCCUsersInCCDB = iretCode

End Function


'-- Global Variable webloginDic
'-- input status will be 0(Disabled/LOCKED),1 (Enable/Active)
Private Sub ChangeWebUsersStatus(ByVal accountid, Byval instatus)
	    'Add Disable/Enable each customer center user
        If Not IsEmpty(webloginDic) Then
            'instatus: 1 (Active)/ 0(locked)
            Dim dickeys, diccount, iii, iretcode
            dickeys = webloginDic.Keys
            diccount = webloginDic.Count -1
            For iii=0 to diccount
               ' iretcode = ChangeWebAcctStatusInCC (accountid, dickeys(iii), instatus)
               ' 'only successfully change status in CC    
               ' If iretcode = 1 Then
               '     iretcode = ChangeWebAcctStatusInUserDB(dickeys(iii), instatus)
               ' End If   
                iretcode = ChangeWebAcctStatusInUserDB(dickeys(iii), instatus)                                
            Next 
        End If 
End Sub


'Ex: strDate : 2014-03-10T23:11:58.19
'    rtn: 03/10/2014 11:11:58 PM
Private Function FormatISODate(ByVal strDate) 
    Dim arrT 
    Dim arrDash 
    Dim arrDot 
    Dim srtnDate

    arrT = Split(strDate, "T") 
    arrDash = Split(arrT(0), "-") 
    arrDot = Split(arrT(1), ".") 
    
    srtnDate = arrDash(1) & "/" & arrDash(2) & "/" & arrDash(0) & " " & arrDot(0) 
    
    FormatISODate = FormatDateTime(srtnDate)
    
End Function 

Private Function ExtractXmlElemValueWithNS(ByRef strXmlData, ByRef strTagToExtract, ByVal strTagNS)

    Dim strXmlElement, intStartTagPos, intEndTagPos, intPosEndStartTag
    
    ExtractXmlElemValueWithNS = ""

    If IsNull(strXmlData) or trim(strXmlData) = "" Then Exit Function
    If IsNull(strTagToExtract) or trim(strTagToExtract) = "" Then Exit Function
    
    Dim strChkTag
    strChkTag = strTagToExtract + strTagNS

    'Find <TAG/>, If so, return ""
    intStartTagPos = InStr(strXmlData, "<" + strChkTag + "/>")
    If intStartTagPos > 0 Then Exit Function
    
    ' Find start of element
    intStartTagPos = InStr(strXmlData, "<" + strChkTag + ">")
    If intStartTagPos = 0 Then Exit Function

    intEndTagPos = InStr(intStartTagPos + 1, strXmlData, "</" + strTagToExtract + ">")
    If intEndTagPos = 0 Then Exit Function

    intPosEndStartTag = intStartTagPos + len(strChkTag) + 2  

    strXmlElement = Mid(strXmlData, intPosEndStartTag, intEndTagPos - intPosEndStartTag)

    ExtractXmlElemValueWithNS = Trim(strXmlElement)
End Function
%>
<%
'Use Global Variable strCTConnString, AccountID
Private Function GetOrdersInCTDB()

    Dim iretCode
    Dim lrsData, lcmd
    
    Dim lconnection
    Set lconnection = Server.CreateObject("ADODB.Connection")
    lconnection.ConnectionString = strCTConnString 
    lconnection.open 
    
    Set lrsData = Server.CreateObject("ADODB.Recordset")
    Set lcmd = Server.CreateObject("ADODB.Command")
    With lcmd
	    .CommandTimeout = 120
	    .CommandType = adCmdStoredProc
	    .CommandText = "dbo.ems_getOrdersEMS"
        .NamedParameters = True
	    .Parameters.Append .CreateParameter("@ReturnValue", adInteger, adParamReturnValue)
        .Parameters.Append .CreateParameter("@AccountId", adInteger, adParamInput, 255, AccountID)
        
	    Set .ActiveConnection = lconnection
    End With
    
    With lrsData
        .CursorLocation = adUseClient
	    .Open lcmd, , adOpenForwardOnly, adLockReadOnly
	    'Disconnected RecordSet
	    set .ActiveConnection = Nothing
    End With	    
    
    iretCode = lcmd.Parameters("@ReturnValue").Value
  
    If iretCode = 1 Then
        set GetOrdersInCTDB = lrsData
    Else
        set GetOrdersInCTDB = nothing
    End If  
    
    
    lconnection.close
    Set lcmd = nothing
    Set lconnection = nothing
    
End Function

 %>

<div class="ems-body-container account-detail-page">
	<div class="account-info row">
		<div class="col-md-12 col-sm-12 col-xs-12">
		     
		   <div class="account-info__account-name control-group">
				<label for="edit-account-name" class="control-label">
					<a class="link" href="AccountDetails.asp?AccID=<%=AccountID%>"><b>Account <%= AccountID %>: </b>
					<span class="account-name-label"><%=AccountName%></span></a><span class="glyphicon glyphicon-edit edit-button"></span>
				</label>
				<input type="text" class="edit-account-name" name="edit-account-name" />
			</div>
		   <div class="account-info__account-uid"><b>AccountUID: <%=LCase(AccountUID)%>  </b>
			</div>
			<% if Session("EnterpriseType") = "Absolute" then %>
			<div class="account-info__silo-info siloInfo">
					<p><b>Silo:</b> <span class="silo-name"><%=strSiloName %></span> - <b>CT Server: </b><span class="ct-server"><%
						if intCallDataMapServerId <> -1 then %> 
							<%= strCallDataServer%>
						<%
						end if %></span> - <b>CC Server: </b><span class="cc-server"><%
						if intCCDataMapServerId <> -1 then %> 
							<%= strCCDataServer %>
							<%
							end if %></span>
						<%if datacenter = strEUDC then%>
						- <b>Data Center: </b>  <span class="data-center"> <%= datacenter %></span>
						<% end if%>
					</p>
			</div>
			<% end if %>
			<% 
				If strMessageSC <> "" Then
			%>
				<div class="account-info__alert">
			<%
				response.Write strMessageSC
			%>
				</div>
			<%
				End If 

			%>   
	   </div>
	</div>
	<div class="clearfix"></div>
	<div class="ems-widget" merge-account-history accountid="<%=AccountID%>" accountuid="<%=AccountUID%>" datacenter="<%=dataCenter%>"></div>
	<div class="clearfix"></div>
	<div class="account-update-block row" style="margin-top: 20px;">
		<form name="update_account" action="AccountDetails.asp?AccID=<%=AccountID%>" method="post">
			<div class="col-md-12 col-sm-12 col-xs-12 form-group">
				<div class="account-update-block__left-block col-md-6 col-sm-6 no-padding">
					<div class="col-md-12 col-sm-12 col-xs-12 row no-padding row">
						<%
							if Session("EnterpriseType") <> "Customer" then
										strProc = "exec dbo.ems_getAccountNote @NoteTypeCode = 'SPCLINSTR', @account_id="  & AccountID
										rsData.Open strProc, strCTConnString, adOpenForwardOnly, adLockReadOnly, adCmdText

										If Not rsData.EOF Then

											strSpecInst = rsData("Text")
											strSpecInstUID = rsData("NoteUID")

										End If

										rsData.Close

									
										%>
										<div class="col-md-12 col-sm-12 col-xs-12">
										Special Instructions:<br>
										<textarea name="special_instr" class="form-control" style="min-height: 50px" ><%=strSpecInst%></textarea>
										</div>
										<%
									end if
									%>
						</div>
					<div>
					<div class="clearfix"></div>
					
							<% If bFastContactAvailable = 1 then %>
					<input type="checkbox" <%=strFastContactEnabled %> name="chkEnableFastContact" />&nbsp;Enable
					RTTIP in Customer Center
					<br />
					<% End If %>
					<input type="checkbox" <%=strGeoLocationEnabled %> name="chkEnableGeoLocationTracking" />&nbsp;Enable
					Geolocation Tracking in Customer Center
					<br />
					<% If bSCAvailableFlag = 1 then %>
					<script type="text/javascript" language="javascript">
						function EnableDisableSmartCall() {
							var frm = document.update_account;
							var status = frm.chkSmartCall.checked;
							var statusAccount = '<%=bEnableSCFlag %>';

							frm.chkSCLocation.disabled = !status;
							frm.chkSCSoftware.disabled = !status;
							frm.chkSCLoggedOn.disabled = !status;
							frm.chkSCHardware.disabled = !status;
							frm.chkSCNetwork.disabled = !status;
							frm.ddlSMFrequency.disabled = !status;

							if (!status) {
								frm.chkApplyForAllDevices.checked = status;
								frm.chkApplyForAllDevices.disabled = !status;
							}

							if (statusAccount == "True" && status) {
								frm.chkApplyForAllDevices.disabled = !status;
							}


							if (frm.chkSCLocation.checked == !status && frm.chkSCSoftware.checked == !status
							&& frm.chkSCLoggedOn.checked == !status && frm.chkSCHardware.checked == !status
							&& frm.chkSCNetwork.checked == !status) {
								frm.chkSCLocation.checked = status;
								frm.chkSCSoftware.checked = status;
								frm.chkSCLoggedOn.checked = status;
								frm.chkSCHardware.checked = status;
								frm.chkSCNetwork.checked = status;
							}
						}	
					</script>
					<input id="chkSmartCall" type="checkbox" name="chkSmartCall" onclick="EnableDisableSmartCall()"
												<%if (bEnableSCFlag = "True") then Response.Write(" checked=""checked"" ")  end if%>><label
													for="chkSmartCall">Enable Event Calling</label>
												<div  style="padding: 5px 0 5px 16px;">
													<input id="chkApplyForAllDevices" type="checkbox" name="chkApplyForAllDevices"                                           
													 <%if (bEnableSCFlag <> "True") then 
													  Response.Write(" disabled=""true"" ") 
													end if%> onclick="return false;">   
													<label for="chkApplyForAllDevices">Only apply to all devices where event calling
													 is enabled</label> <img src="Images/icon_info.png" title="Applies settings to all support devices in the account by clearing the checkbox" /> 
											</div>
											<div style="padding: 5px 0 5px 20px;">
												Minimum Event Call Period:
												<% WriteSmartCallPeriodControlWithClass "ddlSMFrequency", bEnableSCFlag, intSCPeriodTime, "", "form-control", true %>
											</div>
											<div id="pnSCConfig" style="width: 400px; padding-left: 20px; padding-top: 8px;">
												<fieldset>
													<label class="legend">Configuration Options</label>
													<table cellpadding="0" cellspacing="2" border="0" width="100%">
														<tr>
															<td style="width: 50%; vertical-align: top">
																<input id="chkSCLocation" type="checkbox" name="chkSCLocation" <%if (bEnableSCLocation > 0  and bEnableSCFlag = "True") then 
																	Response.Write(" checked=""checked"" ") 
																end if%> <%if (bEnableSCFlag <> "True") then Response.Write(" disabled=""true"" ") end if%> onclick="return false;"><label
																	for="chkSCLocation">Location changes
																</label>
																<div style="padding: 5px 0;">
																	<input id="chkSCSoftware" type="checkbox" name="chkSCSoftware" <%if( bEnableSCSoftware > 0 and bEnableSCFlag = "True") then Response.Write(" checked=""checked"" ") end if%>
																		<%if (bEnableSCFlag <> "True") then Response.Write(" disabled=""true"" ") end if%> onclick="return false;"><label
																			for="chkSCSoftware">Software changes</label>
																</div>
																<input id="chkSCLoggedOn" type="checkbox" name="chkSCLoggedOn" <%if (bEnableSCLoggedOn > 0  and bEnableSCFlag = "True") then Response.Write(" checked=""checked"" ") end if%>
																	<%if (bEnableSCFlag <> "True") then Response.Write(" disabled=""true"" ") end if%> onclick="return false;"><label
																		for="chkSCLoggedOn">Logged in user changes</label>
															</td>
															<td style="vertical-align: top">
																<input id="chkSCHardware" type="checkbox" name="chkSCHardware" <%if (bEnableSCHardware > 0  and bEnableSCFlag = "True") then Response.Write(" checked=""checked"" ") end if%>
																	<%if (bEnableSCFlag <> "True") then Response.Write(" disabled=""true"" ") end if%> onclick="return false;"><label
																		for="chkSCHardware">Hardware changes
																	</label>
																<div style="padding-top: 5px;">
																	<input id="chkSCNetwork" type="checkbox" name="chkSCNetwork" <%if (bEnableSCNetwork > 0  and bEnableSCFlag = "True")  then Response.Write(" checked=""checked"" ") end if%>
																		<%if (bEnableSCFlag <> "True") then Response.Write(" disabled=""true"" ") end if%> onclick="return false;"><label
																			for="chkSCNetwork">Network changes</label>
																</div>
															</td>
														</tr>
													</table>
												</fieldset>                                    
											</div>
					<% End If %>
					</div>
						
				</div>
				<div class="account-update-block__right-block col-md-6 col-sm-6 col-xs-12">
					
					<div class="col-md-12 col-sm-12 col-xs-12 row no-padding">
							<label class="col-md-8 col-sm-8 col-xs-8">Account type: </label>
							<div class="col-md-8 col-sm-8 col-xs-8">
									<%	
										if Session("EnterpriseType") = "Customer" then %>
											<input type="text" value="<% = strAccountTypeDescription %>" size="40" name="accounttypedescription"
												disabled="disabled" />
											<input type="hidden" name="acc_type" value="<%=AccountTypeID %>" />
											<%
										else
											%>
											<select class="selectpicker form-control" name="acc_type">
												<%

												'run ems_getAccountTypes() to get the different account types available from the account_type table
												strProc = "exec dbo.ems_getAccountTypes"
												rsData.Open strProc, strCTConnString, adOpenForwardOnly, adLockReadOnly, adCmdText

												'Populate Account type with contents of table account_type
												do while not rsData.eof %>
												<option value="<%=rsData("account_type_id")%>" <% if AccountTypeID = rsData("account_type_id") then %>
													selected="selected" <% End If %>>
													<%=rsData("account_type_desc")%>
													<% rsData.movenext %>
													<% loop

												'close ems_getAccountTypes
												rsData.Close
													%>
											</select>
											<%
										end if
											%>
							</div>
							
						</div>
					<div class="col-md-12 col-sm-12 col-xs-12 row no-padding">
						<label class="col-md-8 col-sm-8 col-xs-8">Time Zone Offset: </label>
						<div class="col-md-8 col-sm-8 col-xs-8">
							<select name="timezone" class="selectpicker form-control" style="width: 70px;">
								<option <% if intTimeZoneOffset = "-12" then response.write("selected=""selected""") %>
									value="-12">-12</option>
								<option <% if intTimeZoneOffset = "-11" then response.write("selected=""selected""") %>
									value="-11">-11</option>
								<option <% if intTimeZoneOffset = "-10" then response.write("selected=""selected""") %>
									value="-10">-10</option>
								<option <% if intTimeZoneOffset = "-9" then response.write("selected=""selected""") %>value="-9">
									-9</option>
								<option <% if intTimeZoneOffset = "-8" then response.write("selected=""selected""") %>
									value="-8">-8</option>
								<option <% if intTimeZoneOffset = "-7" then response.write("selected=""selected""") %>
									value="-7">-7</option>
								<option <% if intTimeZoneOffset = "-6" then response.write("selected=""selected""") %>
									value="-6">-6</option>
								<option <% if intTimeZoneOffset = "-5" then response.write("selected=""selected""") %>
									value="-5">-5</option>
								<option <% if intTimeZoneOffset = "-4" then response.write("selected=""selected""") %>
									value="-4">-4</option>
								<option <% if intTimeZoneOffset = "-3" then response.write("selected=""selected""") %>
									value="-3">-3</option>
								<option <% if intTimeZoneOffset = "-2" then response.write("selected=""selected""") %>
									value="-2">-2</option>
								<option <% if intTimeZoneOffset = "-1" then response.write("selected=""selected""") %>
									value="-1">-1</option>
								<option <% if intTimeZoneOffset = "0" then response.write("selected=""selected""") %>
									value="0">0</option>
								<option <% if intTimeZoneOffset = "1" then response.write("selected=""selected""") %>
									value="1">1</option>
								<option <% if intTimeZoneOffset = "2" then response.write("selected=""selected""") %>
									value="2">2</option>
								<option <% if intTimeZoneOffset = "3" then response.write("selected=""selected""") %>
									value="3">3</option>
								<option <% if intTimeZoneOffset = "4" then response.write("selected=""selected""") %>
									value="4">4</option>
								<option <% if intTimeZoneOffset = "5" then response.write("selected=""selected""") %>
									value="5">5</option>
								<option <% if intTimeZoneOffset = "6" then response.write("selected=""selected""") %>
									value="6">6</option>
								<option <% if intTimeZoneOffset = "7" then response.write("selected=""selected""") %>
									value="7">7</option>
								<option <% if intTimeZoneOffset = "8" then response.write("selected=""selected""") %>
									value="8">8</option>
								<option <% if intTimeZoneOffset = "9" then response.write("selected=""selected""") %>
									value="9">9</option>
								<option <% if intTimeZoneOffset = "10" then response.write("selected=""selected""") %>
									value="10">10</option>
								<option <% if intTimeZoneOffset = "11" then response.write("selected=""selected""") %>
									value="11">11</option>
								<option <% if intTimeZoneOffset = "12" then response.write("selected=""selected""") %>
									value="12">12</option>
								<option <% if intTimeZoneOffset = "13" then response.write("selected=""selected""") %>
									value="13">13</option>
							</select>
						</div>
						
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12 row no-padding">
						<label class="col-md-8 col-sm-8 col-xs-8">Locale: </label>
						<div class="col-md-8 col-sm-8 col-xs-8">
							<select name="localeCode" class="selectpicker form-control">
								<option value=""></option>
								<%
						strProc = "exec getlocalelist"
						rsData.Open strProc, strCTConnString, adOpenForwardOnly, adLockReadOnly, adCmdText
						Do While Not rsData.EOF
							Response.Write "<option value=""" & rsData("LocaleCode") & """"
							if UCase(strLocaleCode) = UCase(rsData("LocaleCode")) then Response.Write " selected=""selected"" " end if
							Response.Write ">" & rsData("Culture") & "</option>"
							rsData.MoveNext
						Loop

						rsData.Close
								%>
							</select>
						</div>
						
            
            
					</div>
								<!--Dual-User Authentication-->
					<div class="col-md-12 col-sm-12 col-xs-12 no-padding">
						<span>Require second Security Administrator to approve:</span>
						<br>
						<input type="checkbox" name="chkDDNumberForDualUser" id="chkDDNumberForDualUser" value="1" onchange='SecondSecurityChange(this,"txtDDNumberForDualUser")'
						<% if (bEnableDDApproval = true) then Response.Write(" checked=""checked"" ") end if %>                         
							   />
					   <label for="chkDDNumberForDualUser"> Data Delete requests with </label>
					   <input type="text" name="txtDDNumberForDualUser" id="txtDDNumberForDualUser" style="width: 50px" maxlength="10" value="<% if intDDNumberForDualUser > 0 Then Response.Write(intDDNumberForDualUser)  End If %>"           
					   <% if (bEnableDDApproval = false) then Response.Write(" disabled=""disabled"" ") end if%>            
							   />
					   <label for="chkDDNumberForDualUser"> or more devices </label>
					   <br>
					   <input type="checkbox" name="chkDFNumberForDualUser" id="chkDFNumberForDualUser" value="1" onchange='SecondSecurityChange(this,"txtDFNumberForDualUser")'
								<% if (bEnableDFApproval = true) then Response.Write(" checked=""checked"" ") end if %>                        
							 />
							   <label for="chkDFNumberForDualUser"> Device Freeze requests with </label>
							   <input type="text" name="txtDFNumberForDualUser" id="txtDFNumberForDualUser" style="width: 50px" maxlength="10"  value="<% if intDFNumberForDualUser > 0 Then Response.Write(intDFNumberForDualUser) End If %>"  
							   <% if (bEnableDFApproval = false) then Response.Write(" disabled=""disabled"" ") end if%>                       
							   />
							   <label for="chkDFNumberForDualUser"> or more devices </label>
						<br>	   
						<input type="checkbox" name="chkARNumberForDualUser" id="chkARNumberForDualUser" value="1" onchange='SecondSecurityChange(this,"txtARNumberForDualUser")'
								<% if (bEnableARApproval = true) then Response.Write(" checked=""checked"" ") end if %>
							  />
							   <label for="chkARNumberForDualUser"> Agent Removal requests with </label>
							   <input type="text" name="txtARNumberForDualUser" id="txtARNumberForDualUser"  style="width: 50px" maxlength="10"  value="<%  if intARNumberForDualUser > 0 Then Response.Write(intARNumberForDualUser)  End If %>"                       
							   <% if (bEnableARApproval = false) then Response.Write(" disabled=""disabled"" ") end if%>  
							   />
							   <label for="chkARNumberForDualUser"> or more devices </label>
					</div>

            <!--The End Of Dual-User Authentication-->
				
				</div>
			</div>
			    <%
					
					If strMoveStatus <> "" Then
						%>
						<div class="col-md-12 col-sm-12 col-xs-12">
							<b>This Account is being moved!</b><br>
							<b>Move Status:</b><%=strMoveStatus%><br>
							<b>Created By:</b><%=strCreatedBy%><br>
							<b>Created Timestamp:</b><%=dtCreatedTS%>
						</div>
						
						<% 
					End If
				%>
			<div class="col-md-12 col-sm-12 col-xs-12 updated-data">
				<button class="account-update-block__ems-button account-update-block__ems-button--save btn" type="button" name="Update" onclick="validateform()">Save Changes
				</button>
				<div class="col-md-12 col-sm-12 col-xs-12 no-padding-left">
				<span name="creationDate"><b>Creation Date UTC:</b>
				<%=creationDateUTC%></span>
				<span name="lastSaveUTC"><b>Last saved UTC:</b>
				<%=lastUpdateDateUTC%></span>
				<span name="lastSaveBy"><b>Last saved by:</b>
				<%=lastUpdateUser%></span>
				</div>
				
			</div>
			
			
			<table width="90%" border="0" cellspacing="0" cellpadding="5" style="display: none;">
				<tr valign="top">
					<td width="50%">
						<input type="hidden" name="ifupdate" value="false">
						<input type="hidden" name="DisableCCAccounts" value="false">
						<input type="hidden" name="OldAccountTypeID" value="<%=AccountTypeID%>">
						<input type="hidden" name="AccountID" value="<%=AccountID%>">
						<input type="hidden" name="DDAuthorizationId_out" value="<%=intDDAuthorizationId_out%>">
						<input type="hidden" name="hdnUidContactName" value="<%=uidContactName %>" />         
						<input type="hidden" name="hdEnableSCFlag" value="<%=bEnableSCFlag %>">
						<input type="hidden" name="hdintSCPeriodTime" value="<%=intSCPeriodTime %>">
						<input type="hidden" name="hdintBits" value="<%=intBits %>">
            <br />
            <br />
            Account Name:<br>
            <input type="Text" id="account_name_textbox" name="account_name" size="40" value="<%=AccountName%>" maxlength="50">
            <br>
            <br>
            Address:<br>
            <input type="Text" name="Address1" size="40" value="<%=address1%>" maxlength="255"><br />
            <input type="Text" name="Address2" size="40" value="<%=address2%>" maxlength="255"><br />
            <input type="Text" name="Address3" size="40" value="<%=address3%>" maxlength="255"><br />
            <input type="Text" name="Address4" size="40" value="<%=address4%>" maxlength="255">
            <br />
            <br />
            City:<br>
            <input type="text" name="city" size="40" value="<%=city%>" maxlength="255">
            <br>
            <br>
            State/Province:<br />
            <%
		strProc = "exec dbo.ems_getStateList"
		'debug "ems_getStateList",strProc
		rsData.Open strProc, strCTConnString, adOpenForwardOnly, adLockReadOnly, adCmdText
            %>
            <select name="state">
                <% do while not rsData.eof %>
                <option value="<%=rsData("state_id")%>" <% If rsData("state_id") = state_id then %>
                    selected<% End If %>>
                    <%=rsData("state_desc")%>
                    <% rsData.movenext %>
                    <% loop 

				rsData.close()
                    %>
            </select>
            <br />
            <br />
            Zip/Postal Code:<br>
            <input type="Text" name="zip" size="40" value="<%=postalCode%>" maxlength="30">
            <br />
            <br />
            Country:<br>
            <select name="country">
                <option value="0">Select country</option>
                <%
		'run ems_getCountryTypes stored procedure to get all the country_id and country_desc from the country table
		strProc="exec dbo.ems_getCountryTypes"
		rsData.Open strProc, strCTConnString, adOpenForwardOnly, adLockReadOnly, adCmdText
		'populate country field from the country table
		Do While Not rsData.EOF
			Response.Write "<option value=""" & rsData("CountryCode") & """"
			if CountryCode = rsData("CountryCode") then Response.Write " selected " end if
			Response.Write ">" & rsData("country_desc") & vbcrlf & "</option>"
			rsData.MoveNext
		Loop

		'close ems_getCountryTypes
		rsData.Close
                %>
            </select>
            <br />
            <br />
            
        </td>
        <td width="50%">
            E-mail:<br>
            <input type="text" name="email" size="40" value="<%=AccountEmail%>" maxlength="255" />
            <br>
            <br>
            Phone Number:
            <table border="0" cellpadding="1" cellspacing="1">
                <tr>
                    <td>
                        Country Code:
                    </td>
                    <td>
                        <input maxlength="5" size="10" name="phonecountrycode" value="<%=strPhoneCountryCode %>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Number:
                    </td>
                    <td>
                        <input maxlength="255" size="25" type="text" name="phonenumber" value="<%=PhoneNumber%>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Extension:
                    </td>
                    <td>
                        <input maxlength="10" size="10" name="phoneext" value="<%=strPhoneExt %>" />
                    </td>
                </tr>
            </table>
            <br />
            Fax Number:
            <table border="0" cellpadding="1" cellspacing="1">
                <tr>
                    <td>
                        Country Code:
                    </td>
                    <td>
                        <input maxlength="5" size="10" name="faxcountrycode" value="<%=strFaxCountryCode %>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Number:
                    </td>
                    <td>
                        <input maxlength="255" size="25" type="text" name="faxnumber" value="<%=FaxNumber%>" />
                    </td>
                </tr>
            </table>
            <br />
            <%	

		if Session("EnterpriseType") = "Absolute" then
		
		    strProc = "exec dbo.ems_getAccountNote @NoteTypeCode = 'RCVRYINSTR', @account_id="  & AccountID
            rsData.Open strProc, strCTConnString, adOpenForwardOnly, adLockReadOnly, adCmdText

            If Not rsData.EOF Then

                strRecInst = rsData("Text")
                strRecInstUID = rsData("NoteUID")

            End If

            rsData.Close
            %>
            Recovery Instructions:<br>
            <textarea name="recovery_instr" cols="35" rows="3" style="font-family: courier,monotype"><%=strRecInst%></textarea>
            <%
	    end if
            %>
            <br />
            <br />
            Intel AT Security Gating:<br />
            <select name="ddlIntelATSecurityGating">
                <option value=""></option>
                <option <%if intIntelAtpUserAuthentication = 0 then response.write("selected=""selected""") end if %>
                    value="0">None</option>
                <option <%if intIntelAtpUserAuthentication = 1 then response.write("selected=""selected""") end if %>
                    value="1">Same as Security Admin</option>
            </select>
            <input type="hidden" name="hdnIntelATSecurityGating" value="<% =intIntelAtpUserAuthentication %>" />
        </td>
    </tr>
    <tr>
        <td valign="top">
            Customer Center Service ID:<br />
            <%
		if Session("EnterpriseType") = "Customer" then %>
            <input type="text" value="<% = strServiceName %>" size="40" name="servicename" disabled="disabled" />
            <input type="hidden" value="<% = ServiceId %>" name="service" />
            <%
		else %>
            <select name="service">
                <%
		    'run ems_getServiceTypes stored procedure to get all the service_name and service_description from the service table
		    strProc = "dbo.ems_getServiceTypes"
		    rsData.Open strProc, strCTConnString, adOpenForwardOnly, adLockReadOnly, adCmdText

		    'populate service field from service table
		    do while not rsData.eof
			    if rsData("Service_ID") = ServiceID then
                %><option selected="selected" value="<%=rsData("Service_ID")%>">
                    <%
				    Response.Write rsData("ServiceName")
			    else
                    %><option value="<%=rsData("Service_ID")%>">
                        <%
				    Response.Write rsData("ServiceName")
			    end if
			    rsData.movenext
		    loop

		    'close ems_getServiceTypes
		    rsData.Close
                        %>
            </select>
            <%
		end if
            %>
        </td>
    </tr>
    <%
			Dim strRemoteFileRetrievalEnabled,strRemoteFileRetrievalEnabledText,  blnRemoteFileRetrievalEnabled, strHiddenValue

			blnRemoteFileRetrievalEnabled = HasRemoteFileRetrievalSetting(AccountID)

			strRemoteFileRetrievalEnabled = " "
			strRemoteFileRetrievalEnabledText = "Disabled"
			strHiddenValue = ""
			If blnRemoteFileRetrievalEnabled = true Then
				strRemoteFileRetrievalEnabled = " checked=""checked"" "
				strRemoteFileRetrievalEnabledText = "Enabled"
				strHiddenValue = "on"
			End If

			Dim strFipsEnabled, strHdnFipsEnabled, strFipsEnabledText
			strHdnFipsEnabled = ""
			strFipsEnabledText = "Disabled"
			If IsFipsEnabledForAccount(AccountID) = 1 Then
				strFipsEnabled = " checked=""checked"" "
				strFipsEnabledText = "Enabled"
				strHdnFipsEnabled = "on"
			End If

			Dim strDNSCacheEnabled, strHdnDNSCacheEnabled, strDNSCacheEnabledText
			strDNSCacheEnabled = ""
			strHdnDNSCacheEnabled = ""
			strDNSCacheEnabledText = "Disabled"
			If IsDNSCacheEnabledForAccount(intAccountSF) = 1   Then
				strDNSCacheEnabled = " checked=""checked"" "
				strHdnDNSCacheEnabled = "on"
				strDNSCacheEnabledText = "Enabled"
			End If


			Dim strDEATIIEnabled, strHdnDEATIIEnabled, strDEATIIEnabledText
			strDEATIIEnabled = ""
			strHdnDEATIIEnabled = ""
			strDEATIIEnabledText = "Disabled"
			If IsDEATIIEnabledForAccount(intAccountSF) = 1   Then
				strDEATIIEnabled = " checked=""checked"" "
				strHdnDEATIIEnabled = "on"
				strDEATIIEnabledText = "Enabled"
			End If
		
		
		
		%>
    <input type="hidden" name="hdnRemoteFileRetrievalEnabled" value="<%=strHiddenValue %>" />
    <input type="hidden" name="hdnFipsEnabled" value="<%=strHdnFipsEnabled %>" />
    <input type="hidden" name="hdnDNSCacheEnabled" value="<%=strHdnDNSCacheEnabled %>" />
    <input type="hidden" name="hdnDEATIIEnabled" value="<%=strHdnDEATIIEnabled %>" />
    <input type="hidden" name="hdnGeoLocationEnabled" value="<%=strHdnGeoLocationEnabled %>" />
    <input type="hidden" name="hdnEnableApproval" value="<%=bEnableApprovalFeauture %>" />
    
</table>
</form>
</div>
<div class="clearfix"></div>
<div class="account-information-block">
	<h4 class="block-label">Account Information</h4>
	<div class="clearfix"></div>
	<div class="account-information-block__left-block col-md-6 col-sm-12 col-xs-12 no-padding">
		<div class="agent-package">
            <div class="ems-widget" agent-version accountuid="<%=AccountUID%>" datacenter="<%=dataCenter%>"></div>
		</div>
		<div class="pending-action">
            <div class="ems-widget" pending-action accountid="<%=AccountID%>" accountuid="<%=AccountUID%>" datacenter="<%=dataCenter%>"></div>
		</div>
	</div>
	<div class="account-information-block__right-block col-md-6 col-sm-12 col-xs-12">
		<div class="a7-freeze">
           <label class="col-xs-6 col-md-8 col-sm-6 label-block no-padding-left">A7 Device Freeze enabled: </label><span id="DDS6DFSetting" class="col-xs-6 col-md-4 col-sm-6"></span>
		</div>
		<div class="clearfix"></div>
		<div class="account-pingtime">
           <div class="ems-widget" account-pingtime accountuid="<%=AccountUID%>" datacenter="<%=dataCenter%>"></div>
		</div>
		<div class="2fa-setting">
           <div class="ems-widget" two-fa-settings accountuid="<%=AccountUID%>" datacenter="<%=dataCenter%>"></div>
		</div>
		<div class="siem-service">
           <div class="ems-widget" siem-widget accountuid="<%=AccountUID%>" datacenter="<%=dataCenter%>"></div>
		</div>
		<div class="remote-file-retrieval-info">
		
			<table class="table table-hover table-bordered">
			  <tr class="first-col-label">
				<td class="col-md-6 col-sm-6 col-xs-6" id="chkRFR_label">Remote File Retrieval in Customer Center</td>
				<td class="col-md-6 col-sm-6 col-xs-6" id="chkRFR_value"><%=strRemoteFileRetrievalEnabledText%>
				</td>
			  </tr>
			  <tr class="first-col-label">
				<td class="col-md-6 col-sm-6 col-xs-6" id="chkFipsEnabled_label">FIPS Certified Encryption</td>
				<td class="col-md-6 col-sm-6 col-xs-6" id="chkFipsEnabled_value"><%=strFipsEnabledText%>
				</td>
			  </tr>
			  <tr class="first-col-label">
				<td class="col-md-6 col-sm-6 col-xs-6" id="chkDNSCacheEnabled_label">DNS Cache</td>
				<td class="col-md-6 col-sm-6 col-xs-6" id="chkDNSCacheEnabled_value"><%=strDNSCacheEnabledText%>
				</td>
			  </tr>
			  <tr class="first-col-label">
				<td class="col-md-6 col-sm-6 col-xs-6" id="chkDEATIIEnabled_label">ATII Collection</td>
				<td class="col-md-6 col-sm-6 col-xs-6" id="chkDEATIIEnabled_value"><%=strDEATIIEnabledText%>
				</td>
			  </tr>
			 </table>
			
			<div class="hidden">
				<input type="checkbox" <% =strRemoteFileRetrievalEnabled %> name="chkRFR" />&nbsp;Enable
            Remote File Retrieval in Customer Center
            <br />
            <input type="checkbox" <% =strFipsEnabled %> name="chkFipsEnabled" />&nbsp;<%=FIPS_DESCRIPTION %><br />
            <input type="checkbox" <% =strDNSCacheEnabled %> name="chkDNSCacheEnabled" />&nbsp;<%=DNSCACHE_DESCRIPTION %><br />
            <input type="checkbox" <% =strDEATIIEnabled %> name="chkDEATIIEnabled" />&nbsp;<%=DEATII_DESCRIPTION %>
			</div>
			
		</div>
	</div>
</div>
<div class="clearfix"></div>
          <br/>
          <br/>
          <div class="utility">
            <span class="btn btn-lg utility__ems-button-collapse" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="true" aria-controls="collapseExample">
                <span class="glyphicon glyphicon-menu-hamburger"></span> Utility
            </span>
			
            <div class="group-button">
                <div class="collapse in" id="collapseExample">
                    <button class="utility__ems-button btn" type="button" title="Show this account's ESNs" name="viewesns" onclick="javascript:document:location='ShowESNs.asp?AccID=<%=AccountID %>&accountuid=<%=AccountUID %>&datacenter=<%=dataCenter%>'"> Show this account's ESNs
                    </button>
                    <button class="utility__ems-button btn" type="button" name="downloadCSV" title="Download CSV for this account's ESNs" <%=disabled%> onclick="DownloadCSV(<%=AccountID%>)">Download CSV for this account's ESNs
                    </button>
					<% if Session("EnterpriseType") = "Absolute" then %>
                    <button class="utility__ems-button btn" type="button" name="registeredIP" title="Registered Refurbisher IPs" onclick="javascript:document.location='RefurbisherIPs.asp?AId=<%=AccountID %>&AName=<%=AccountName%>'">Registered Refurbisher IPs
                    </button>
					<% end if %>
                    <button class="utility__ems-button btn" type="button" name="DCProfileHistory" title="Device Container Profile History" onclick="javascript:document.location='OrderProfileHistory.asp?AId=<%=AccountID %>&AName=<%=AccountName%>'">Device Container Profile History
                    </button>
					<% if Session("EnterpriseType") = "Absolute" then %>
                    <button class="utility__ems-button btn" type="button" name="AccountServerMap" title="Account/Server Map" onclick="javascript:document.location='MapAccountDetails.asp?AId=<%=AccountID %>'">Account/Server Map
                    </button>
					<% end if %>
                    <button class="utility__ems-button btn" type="button" name="EventStore" title="Event Store" onclick="javascript:document.location='EventStore.asp?AccID=<%=AccountID %>&AccountUID=<%=AccountUID%>&datacenter=<%=dataCenter%>'" title="Event Store" >Event Store
                    </button>
                    <button class="utility__ems-button btn" type="button" name="APIToken" title="API Token Management" onclick="javascript:document.location='ApiToken.asp?AccID=<%=AccountID %>&AccountUID=<%=AccountUID %>&datacenter=<%=dataCenter%>'">API Token Management
                    </button>
                    <button class="utility__ems-button btn" type="button" name="BulkUnenroll" title="Bulk Unenroll request" onclick="javascript:document.location='BulkUnenroll.asp?AccID=<%=AccountID %>&AccountUID=<%=AccountUID %>&datacenter=<%=dataCenter%>'">Bulk Unenroll request
                    </button>
					<%if Session("EnterpriseType") <> "Customer" then%>
                    <button class="utility__ems-button btn" type="button" name="mergeAccount" title="Merge this account into another account" onclick="MergeAccount(<%=AccountID%>,'<%=AccountUID%>')" <%=disabled%>>Account Merge					
                    </button>
					<%end if%>
					<%if (Session("EnterpriseType") = "Absolute" or Session("EnterpriseType") = "Enterprise") then%>
                    <button class="utility__ems-button btn" type="button" <%if intDDReturnCode = -2 then response.write("disabled ") end if%> name="DDButton" title="Security Admin Pre-Authorization" onclick="getPreAuthorization(<%=AccountID%>, <%=intDDAuthorizationId_out %>)"><%if intDDAuthorizationId_out = 0 then response.write("Create ") end if%>Security Admin Pre-Authorization				
                    </button>
					<%elseif (Session("EnterpriseType") = "Customer" and intDDAuthorizationId_out <> 0) then%>
                    <<button class="utility__ems-button btn" type="button" name="DDButton" title="Security Admin Pre-Authorization" onclick="getPreAuthorization(<%=AccountID%>, <%=intDDAuthorizationId_out %>)">Security Admin Pre-Authorization				
                    </button>
					<%end if%>
                    <button class="utility__ems-button btn" type="button" name="BatchJobs" title="Batch Jobs" <%=disabled%> onclick="EsnBatchJobs(<%=AccountID%>)">Batch Jobs					
                    </button>
                    <button class="utility__ems-button btn" type="button" name="DMHistory" title="Device Move History" onclick="javascript:document.location='DeviceMoveHistory.asp?AccID=<%=AccountID %>'">Device Move History					
                    </button>
                    <button class="utility__ems-button btn" type="button" name="accountMergeHistory" title="Account Merge History" onclick="GetAccountMergeHistory(<%=AccountID%>)">Account Merge History					
                    </button>
					<button class="utility__ems-button btn" type="button" name="PolicyInformation" title="Policy & License Information" onclick="javascript:document.location='PolicyInformation.asp?AccID=<%=AccountID %>&AccUID=<%=AccountUID%>&datacenter=<%=dataCenter%>'">Policy & License Information				
                    </button>
					<button class="utility__ems-button btn" type="button" name="GGAccount" title="Google Account Infomation" onclick="javascript:document.location='GoogleAccount.asp?AccID=<%=AccountID %>&AccountUID=<%=AccountUID %>&datacenter=<%=dataCenter%>'">Google Account Infomation				
                    </button>
                    <button class="utility__ems-button btn" type="button" name="ActionRequest" title="Action Requests" onclick="javascript:document.location='AccountRequestAction.asp?AccID=<%=AccountID %>&AccountUID=<%=AccountUID %>'">Action Requests				
                    </button>
              </div>
            </div>
          </div>
           <br>
<div class="clearfix"></div>
<div class="device-containers">
	<h4 class="block-label">Device Containers</h4>
	<div class="col-md-12 col-xs-12 col-sm-12 no-padding">
		<table class="table table-striped hidden" id="deviceContainer">
			<thead>
				<th class="containerIDHeader">
					<b>Container Id</b>
				</th>
                <th class="productNumHeader">
					<b>Product</b>
				</th>
                <th class="orderTypeHeader">
					<b>DC Type</b>
				</th>
				<th class="containerCreationDateHeader" style="width: 150px">
					<b>DC Creation Date</b>
				</th>
				<th class="activeESNsHeader">
					<b>Active ESNs</b>
				</th>
				<th class="disabledESNsHeader">
					<b>Disabled ESNs</b>
				</th>
				<th class="inactiveESNsHeader">
					<b>Inactive ESNs</b>
				</th>	
			</thead>
			<tbody>
				<%
    'Work Ticket #59180: EMS: Unable to retrieve information about orders on Account details page � timeout error (stored procedure: dbo.ems_getOrders)
    ' This will have timeout 
'strProc = "exec dbo.ems_getOrders @accountID=" & AccountID
'rsData.Open strProc, strCTConnString, adOpenForwardOnly, adLockReadOnly, adCmdText

    Set rsData = GetOrdersInCTDB()
    If Not rsData Is Nothing Then
        If rsData.recordcount >0 Then
            rsData.MoveFirst
            'write out Orders table body
			Dim containerCount
			containerCount = 0
            While Not rsData.EOF
                Response.Write "<tr class=""dc-record"">"
        	    'write out the order ID# and link it with the Order_Details page for that order
	            Response.Write "<td>&nbsp; <A HREF=" & strQuote & "OrderDetails.asp" & _
		            "?AccID=" & AccountID & "&OrdID=" & rsData("order_id") & "&accountuid=" & AccountUID & "&datacenter=" & dataCenter & strQuote _
		            &  ">" & rsData("order_id") & "</A>"
                Response.Write "<td>"& rsData("product_desc") & "</td>"
                Response.Write "<td class=""order_type"">"& rsData("order_type_desc") & "</td>"
	            'Write out the creation date for that order
	            Response.Write "<td>"& rsData("creation_date") & "</td>"

	            'write out the disabled/active/inactive count
	            Response.Write "<td>"& rsData("ActiveCount") & "</td>"
	            Response.Write "<td>"& rsData("DisabledCount") & "</td>"
	            Response.Write "<td>"& rsData("InactiveCount") & "</td>"

	            Response.Write "</tr>"
	            rsData.MoveNext
            Wend
        End If            
        
        If rsData.State = adStateOpen Then
            rsData.Close
        End If
        
    End If        

        %>
			</tbody>
		</table>
		<button class="account-information-block__ems-button btn package-btn" type="button" name="neworder" onclick="GotoNewOrderPage(<%=AccountID%>,0,'<%=AccountUID%>')">Create a new Device Containers</button>
		<button class="account-information-block__ems-button btn package-btn" type="button" name="allorder" onclick="GotoOrderListPage(<%=AccountID%>)">Display All Device Containers</button>
	</div>
</div>

<%if intIsLojackSilo = 0 then
    'We are in an CCDATA database, so go ahead and list CC Account info.
%>
<div class="clearfix"></div>
<div class="idp-information">
	<div class="ems-widget" idp-information accountid="<%=AccountID%>" accountuid="<%=AccountUID%>" datacenter="<%=dataCenter%>"></div>
</div>
<div class="clearfix"></div>
<div class="console-user">
	<h4 class="block-label">Console Users</h4>
	<div class="ems-widget" user-list accountid="<%=AccountID%>" ssllogin ="<%=session("ssl_login")%>" accountuid="<%=AccountUID%>" datacenter="<%=dataCenter%>"></div>
</div>

<%
elseif strMessage <> "" then
    'Report unknown failure.
    Response.Write strMessage
end if

%>
<script language="JavaScript">
function GetDDRequestDetails(DDRequestId) {
	window.open("DDRequestDetails.asp?RId="+DDRequestId,"");
}

function EsnBatchJobs(AccID, dataCenter)
{
	document.location="BatchJobs.asp?AccID=" + AccID + "&datacenter=<%=dataCenter%>"
} //EsnBatchJobs

function MergeAccount(AccID, AccUID)
{
	document.location="MergeAccount.asp?AccID=" + AccID + "&AccUID=" + AccUID
} //MergeAccount

//This method is used to send the user to the Show ESNs page
function ViewESNs(AccID)
{
	document.location="ShowESNs.asp?AccID=" + AccID
}

//send the user to the Merge CD page
function MergeCD(AccID) {
	document.location="MergeCD.asp?AccID=" + AccID
}

//send the user to the AddUnknownCFIESNs page
function AddUnknownESNs(AccID) {
	document.location="AddUnknownCFIESNs.asp?AccID=" + AccID
}

//send the user to the download CSV file for the accounts ESNs page
function DownloadCSV(AccountID) {
	document.location="DownloadEsnCsv.asp?AId=" + AccountID
}

//This method is used to send the user to the Contact Details page, when creating a new contact
function CreateNewContactWnd(AccID,ContactID)
{
	document.location="ContactDetails.asp?AccID=" + AccID + "&ContactID=" + ContactID + "&ifNew=0&statusSave=0"
}

//This method is used to send the user to the Contact Details page, when clicking on an contact
function GotoContactPage(AccID,ContactID)
{
	document.location="ContactDetails.asp?AccID=" + AccID + "&ContactID=" + ContactID+ "&ifNew=2"
}

//This method is used to send the user to the New Orders page
function GotoNewOrderPage(AccID,OrderID,AccUID)
{
	document.location="NewOrder.asp?AccID="+ AccID + "&accountuid=" + AccUID
	return true;
}

//This method is used to send the user to the Order list page
function GotoOrderListPage(AccID)
{
	document.location="OrderList.asp?AccID="+AccID
	return true;
}

//send user to the New Non-multiSKU Order page
function GotoNewNonMultiSKUOrderPage(AccID,OrderID) {
	document.location="NewNonMultiSKUOrder.asp?AccID="+AccID
	return true;
}

// validates the area code and the phone number field
function validatephonefield( frmfield, emptymsg, invalidmsg ) {
	//if there is nothing in the field then alert the user
	if (frmfield.value=="") {
		alert(emptymsg);
		frmfield.focus();
		return true;
	}
	//remove "." or "-" characters in the field
	for (i = 0; i < frmfield.value.length; i++) {
		if (frmfield.value.charAt(i) == "-" || frmfield.value.charAt(i) == "." ) {
			frmfield.value = frmfield.value.substr(0,i) + frmfield.value.substr(i+1);
			i--;
		}
		else if (isNaN(frmfield.value.charAt(i))) {
			if (!(frmfield.value.charAt(i) == " " || frmfield.value.charAt(i) == "+")) {
				alert(invalidmsg);
				frmfield.focus();
				return true;
			}
		}
	}
return false;
}

function SecondSecurityChange(o,txtNumberId){
    if(o.checked){
       document.getElementById(txtNumberId).disabled = false;
    }
    else{
        document.getElementById(txtNumberId).disabled = true;
    }
}

//This function is called from the "save changes" button
//It checks the input boxes to make sure the data is in a valid form
function validateform() {
	var frm = document.update_account;



	//If the company name was not entered then alert the user
	if (frm.account_name.value == "") {
		alert("Please fill in the \"Account Name:\" field.");
		frm.account_name.focus();
		return false;
	}

	//If the company name was not entered then alert the user
	if (frm.city.value == "") {
        frm.city.value = "No value";
	}

	//If the company name was not entered then alert the user
	if (frm.Address1.value == "") {
		frm.Address1.value = "No value";
	}

//	//If the email field is not of the form X@Y.Z then alert the user
//	if (!( frm.email.value.indexOf("@") > 0 && frm.email.value.indexOf(".") > 0) ) {
//		alert("Please enter a valid E-mail address.");
//		frm.email.focus();
//		return false;
//
//	}

	//If the status is set to disabled or inactive confirm with user
	if( frm.acc_type.value == 6 ) {
		if( frm.OldAccountTypeID.value != 6 ) {
			//If the user confirms then send alert and return true
			if( confirm("Disabling this account will disable all Customer Centre accounts belonging to this account.\nAre you sure you want to perform this action?")) {
				alert("Don't forget to disable the device containers belonging to this account.");
				frm.DisableCCAccounts.value = "true";
			}
			//otherwise return false
			else {
				frm.acc_type.focus();
				return false;
			}
		}
	}
	//If a country was not picked then alert the user
	if (frm.country.options[frm.country.selectedIndex].value == 0) {
        frm.country.selectedIndex = 3;
	}
	
    if (frm.phonenumber.value != "") {
        if (frm.phonecountrycode.value == null || frm.phonecountrycode.value == "") {
            frm.phonecountrycode.value = 1;
        }
        if (validate_isNumeric(frm.phonecountrycode,"Country code must be a number.")==false) {
            frm.phonecountrycode.focus();
            return false;
        }        
    }  

    if (frm.faxnumber.value != "") {
        if (validate_required(frm.faxcountrycode,"Please enter a country code.")==false) {
            frm.faxcountrycode.focus();
            return false;
        }
        if (validate_isNumeric(frm.faxcountrycode,"Country code must be a number.")==false) {
            frm.faxcountrycode.focus();
            return false;
        }
    }      

    //Validate smart call
    <% If bSCAvailableFlag = 1 then %>
    if(frm.chkSmartCall.checked)
    { 
	    if(!frm.chkSCLocation.checked && !frm.chkSCSoftware.checked && !frm.chkSCLoggedOn.checked && !frm.chkSCHardware.checked && !frm.chkSCNetwork.checked)
	    {
		    alert("At least one call settings option must be selected");
		    return false;
	    }
    }
    <% End if %>
   
    if(frm.hdnEnableApproval.value == "False" && (frm.chkDDNumberForDualUser.checked || frm.chkDFNumberForDualUser.checked || frm.chkARNumberForDualUser.checked))
    { 
	    alert("To enable dual approval of security operations, you first need to set at least two Security Administrators as Approvers.");
		return false;
    }
    if(frm.hdnEnableApproval.value == "True"){
        chkDDNumberForDualUser = document.getElementById("chkDDNumberForDualUser");
        chkDFNumberForDualUser = document.getElementById("chkDFNumberForDualUser");
        chkARNumberForDualUser = document.getElementById("chkARNumberForDualUser");
        txtDDNumberForDualUser = document.getElementById("txtDDNumberForDualUser");
        txtDFNumberForDualUser = document.getElementById("txtDFNumberForDualUser");
        txtARNumberForDualUser = document.getElementById("txtARNumberForDualUser");
        var number;
        var maxNumber = <%=intMaxNumberOfDualUserAuthentication %>;

        if(chkDDNumberForDualUser.checked){
            number= parseInt(txtDDNumberForDualUser.value);
            if(number<=0 || number> maxNumber || txtDDNumberForDualUser.value == ""  || isNaN(txtDDNumberForDualUser.value)){
                alert("The number of devices for Data Delete requests is invalid. Enter a value between 1 and "+maxNumber+".");
                return;
            }
        }
        if(chkDFNumberForDualUser.checked ){
            number= parseInt(txtDFNumberForDualUser.value);
            if(number<=0 || number> maxNumber || txtDFNumberForDualUser.value == "" || isNaN(txtDFNumberForDualUser.value)){
                alert("The number of devices for Device Freeze requests is invalid. Enter a value between 1 and "+maxNumber+".");
                return;
            }
        }
        if(chkARNumberForDualUser.checked){
            number= parseInt(txtARNumberForDualUser.value);
            if(number<=0 || number> maxNumber || txtARNumberForDualUser.value == "" || isNaN(txtARNumberForDualUser.value)){
                alert("The number of devices for Agent Removal requests is invalid. Enter a value between 1 and "+maxNumber+".");
                return;
            }
        }
    
    }

frm.ifupdate.value = "true";
frm.submit();

}

function getPreAuthorization(accountId,DDAuthorizationId_out) {
    if (DDAuthorizationId_out == 0)
	    window.open("DDPreAuthorizationCreate.asp?AuId=" + accountId + "&AccId=" + accountId)
	else
	    window.open("DDPreAuthorizationDetails.asp?AuId=" + DDAuthorizationId_out + "&AccId=" + accountId)
}

function GetAccountMergeHistory(AccountId) {
	window.open("AccountMergeHistory.asp?AccID="+AccountId,"");
}

</script>
<script language="JavaScript">

     waitingJquery(getfreezesetting);

    function getfreezesetting() {
      var accountuid = '<%=AccountUID%>';
      var dc = '<%=dataCenter%>'
      var urlNG =  "<%=constEMSApi%>/v1/esn/" + accountuid +"/freezesetting"
      if ( dc === 'EUDC')  
        {
            urlNG = "<%=constEMSApiEUDC%>/v1/esn/" + accountuid +"/freezesetting"
        }
        $.ajax({
               url: urlNG,
               xhrFields: {
                   withCredentials: true
               }
           }).success(function (data) {
                var DDS6DFReturn = data;
                
                if(DDS6DFReturn && DDS6DFReturn.data && DDS6DFReturn.data.value){
                    result = ' Yes';
                }else{
                    result = ' No';
                }
                document.getElementById('DDS6DFSetting').innerHTML = result;
                })
  }
  
	function collapseOrderList(){
		var parentSel = $('.device-containers table');
		var selector = parentSel.find('tr.dc-record');
		var count = selector.length;
		if(count > 10){
			for(var i = 10;  i <count; i++){
				selector.eq(i).addClass('collapse-item hidden');
			}
			parentSel.after('<div id="dcContainerSep" class="show-more-separator">Show more <span class="glyphicon glyphicon-menu-down"></span></div>');
			bindingDCSep();
			
		}
		parentSel.removeClass('hidden');
	}

    function shortenOrderDesc() {
			var selector = $('.order_type');

            selector.each(function(index,item){
                var sel = $(this);
                if(sel.text().length > 10){
                    sel.text(sel.text().substring(0, 8) + '..');
                }
            })
			
        }
	
	function bindingDCSep() {
            var seperator = $('#dcContainerSep');
			var selector = $('.device-containers table');
			
            seperator.on('click', function () {
                if ($(this).text() == "Show more ") {
                    $(this).html("Show less <span class='glyphicon glyphicon-menu-up'></span>");
                } else {
                    $(this).html("Show more <span class='glyphicon glyphicon-menu-down'></span>");
                }
				selector.find('.collapse-item').toggleClass('hidden');

			});
			
            seperator.delegate('span', 'click', function (e) {
                e.stopPropagation();
                $(this).parent().click();
            })
        }
		
	function bindingUserSep() {
			
            $('.console-user').delegate('#userListSep', 'click', function () {
                if ($(this).text() == "Show more ") {
                    $(this).html("Show less <span class='glyphicon glyphicon-menu-up'></span>");
                } else {
                    $(this).html("Show more <span class='glyphicon glyphicon-menu-down'></span>");
                }
				$('.console-user table .collapse-item').toggleClass('hidden');

			});
			
            $('.console-user').delegate('#userListSep span', 'click', function (e) {
                e.stopPropagation();
                $(this).parent().click();
            })
        }
		
		function editButton(){
			$(document).ready(function() {
				$('span.edit-button').click(function () {
					var parentSel = $('.account-info__account-name');
					var inputSel = parentSel.find('input[type="text"]');
					var textSel = parentSel.find('span.account-name-label');
					
					inputSel.css('width', textSel.css('width'));
					inputSel.val(textSel.text());
					textSel.hide();
					inputSel.show().focus();
					$(this).hide();
				});
				
				$('input[type=text].edit-account-name').focusout(function() {
					var inputSel = $(this);
					var textSel = $('.account-info__account-name span.account-name-label');
					
					textSel.text(inputSel.val());
					inputSel.hide();
					textSel.show();
					$('span.edit-button').show();
					$('#account_name_textbox').val(inputSel.val());
				});
			});
		}
		waitingJquery(shortenOrderDesc);
		waitingJquery(collapseOrderList);
		waitingJquery(bindingUserSep);
		waitingJquery(editButton);

</script>
<%
        Call cnnCC.Close()

        set rsData = nothing
        set rsData2 = nothing
        set rs = nothing
        set cmd = nothing
        set cnnCC = nothing
        set cnnRdir = nothing      
%>
<!-- #INCLUDE FILE = "BottomInclude2.asp" -->
