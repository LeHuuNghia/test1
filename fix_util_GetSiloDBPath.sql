USE [ActivationDB]
GO
/****** Object:  StoredProcedure [dbo].[util_GetSiloDBPath]    Script Date: 3/25/2021 3:55:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* Copyright (C) 1995 - 2007 Absolute Software Corp.  All Rights Reserved.

<Name> util_GetSiloDBPath </Name>
<Description>
Get the DBPath for a particular silo on which to create an account
Returns : DBPath as ServerName.DBName, DBPath_Server, DBPath_Database  as Server and DBName.
		  DBpath is blank if no data found
Return value : 1 = success, -1 = Invalid value, -2 = No data found for requested silo and database

</Description>

Get the default path to create an account
Exec @ReturnCode = dbo.util_GetSiloDBPath @DBName = @DBName, 
	@SiloCode = Null, 
	@DefaultSiloFlag = 1, 
	@DBPath = @DBPath Output, 
	@DBPath_Server = @DBPath_Server Output, 
	@DBPath_Database = @DBPath_Database Output


<Consumers>   EMS, ORI Create Account  </Consumers>
<Enterprise>  Yes       </Enterprise>
<SubSystem> CommonDB </SubSystem>
<CreateOnDatabases>   All  </CreateOnDatabases>

<Package> Make_RedirectionBase </Package>
<Version> 2.0 </Version>
<History>
Date          Release  Task #    Who Description
===================================================================================
10 Apr 2007					     JEP Created
23 May 2007            12145     JEP Improve error handling
28 May 2009  CDB 6.4 WI27319 EL Added new logical database names for Monitoring Center 2.0.
25 May 2010  CDB 6.9  34100   VT Change grant to CommonDB_Role instead of Public.
25 May 2010  CDB 6.9  35123   VT Add @EnquoteOutputFlag to allow surrounding of output variables with [] (needed for named instance support)
07 Jun 2010  CDB 6.9  35168   VT Added handling for CallDataExtDB and CTData_Ext as valid logical/physical DB names
24 Jun 2010  CDB6.9    35168  VT Removed handling of CTData_Ext as valid DB name - supporting only logical names going forward
</History>

*/


ALTER Procedure [dbo].[util_GetSiloDBPath] 
  @DBName     nvarchar(255)
, @SiloCode     char(4) = Null
, @DefaultSiloFlag bit = 1 
, @DBPath     nvarchar(255) OUTPUT
, @DBPath_Server nvarchar(255) = NULL OUTPUT
, @DBPath_Database nvarchar(255) = NULL OUTPUT
, @EnquoteOutputFlag Bit = 0  --Surround output variables with [] (needed for named instance support)
As
Begin

--
-- Generates the @ErrorVariableXML and settings based on the procedure signature
-- Exec Dev_GetErrorVariablesXML 'util_GetSiloDBPath' 
--
Declare @ErrorVariablesXML nVarChar(max)
Select @ErrorVariablesXML = 
	'<Var N="@DBName" V="' + IsNull(dbo.fn_xmlesc(@DBName), 'NULL') + '"/>' +
	'<Var N="@SiloCode" V="' + IsNull(dbo.fn_xmlesc(@SiloCode), 'NULL') + '"/>' +
	'<Var N="@DefaultSiloFlag" V="' + IsNull(Convert(VarChar(10), @DefaultSiloFlag), 'NULL') + '"/>' +
	'<Var N="@DBPath" V="' + IsNull(dbo.fn_xmlesc(@DBPath), 'NULL') + '"/>' +
	'<Var N="@DBPath_Server" V="' + IsNull(dbo.fn_xmlesc(@DBPath_Server), 'NULL') + '"/>' +
	'<Var N="@DBPath_Database" V="' + IsNull(dbo.fn_xmlesc(@DBPath_Database), 'NULL') + '"/>'
	

Set @DBPath = Null
Set @SiloCode = NullIf(@SiloCode, '')

If (@SiloCode Is Null And @DefaultSiloFlag Is Null)  or (@SiloCode Is Not Null and @DefaultSiloFlag Is Not Null)
Begin
	-- 	80014, 'Either parameter %s or %s must not be Null, but not both.
 	Exec dbo.RaiseCustomError
 		@MessageID=80014, @ProcID=@@ProcID, @VariablesXML=@ErrorVariablesXML,
 		--@PublicMessageID,
 		@Argument1='@SiloCode', @Argument2='@DefaultSiloFlag'
 		--,@WriteToLog
	
	Set @DBPath = Isnull(@DBPath, '')
	
	Return (-1)
End


If @SiloCode Is Null
Begin
	If @DefaultSiloFlag  = 1 and @DBName in (  'CTDATA', 'ltrctdata',
												'CallDataDB', 'ConsumerCallDataDB')
	Begin
		Select @DBPath = Rtrim(IsNull(MS.Server + '.', ''))  +  Rtrim(Isnull(MS.DBName, '' ))
			 , @DBPath_Server = IsNull(MS.Server, '')
			 , @DBPath_Database = IsNull(MS.DBName, '')
		From dbo.MapSilo Silo With (Nolock) 
			Inner Join dbo.MapServer MS (Nolock) on Silo.CallDataServerId = MS.MapServerId
		Where 
			Silo.DefaultNewSiloFlag = 1
	End
	Else If @DefaultSiloFlag  = 1 and  @DBName In ( 'CCDATA', 'ltrdb',
													'CorporateDB', 'ConsumerDB' )
	Begin
		Select @DBPath = Rtrim(IsNull(MS.Server + '.', ''))  +  Rtrim(Isnull(MS.DBName, '' ))
			 , @DBPath_Server = IsNull(MS.Server, '')
			 , @DBPath_Database = IsNull(MS.DBName, '')
		From dbo.MapSilo Silo With (Nolock) 
			Inner Join dbo.MapServer MS (Nolock) on Silo.CCDataServerId = MS.MapServerId
		Where 
			Silo.DefaultNewSiloFlag = 1
	End
    Else If @DefaultSiloFlag  = 1 and  @DBName In ( 'CallDataExtDB' )
    Begin
       Select @DBPath = Rtrim(IsNull(MS.Server + '.', ''))  +  Rtrim(Isnull(MS.DBName, '' ))
            , @DBPath_Server = IsNull(MS.Server, '')
            , @DBPath_Database = IsNull(MS.DBName, '')
         From dbo.MapSilo Silo With (Nolock) 
        Inner Join dbo.MapServer MS (Nolock) On Silo.SiloCode = MS.SiloCode
        Where Silo.DefaultNewSiloFlag = 1
          And MS.DefaultServerFlag = 1
          And MS.DBType = 2  --ODBC
          And MS.DBClassInd = 'CTX'
    End
End
Else 
Begin
	If  @DBName In ( 'CTDATA', 'ltrctdata',
					'CallDataDB', 'ConsumerCallDataDB' )
	Begin
		Select @DBPath = Rtrim(IsNull(MS.Server + '.', ''))  +  Rtrim(Isnull(MS.DBName, '' ))
			 , @DBPath_Server = IsNull(MS.Server, '')
			 , @DBPath_Database = IsNull(MS.DBName, '')
		From dbo.MapSilo Silo With (Nolock) 
			Inner Join dbo.MapServer MS (Nolock) on Silo.CallDataServerId = MS.MapServerId
		Where 
			Silo.SiloCode = @SiloCode
	End

	Else If @DBName In ( 'CCDATA',  'ltrdb',
						'CorporateDB', 'ConsumerDB' )
	Begin
		Select @DBPath = Rtrim(IsNull(MS.Server + '.', ''))  +  Rtrim(Isnull(MS.DBName, '' ))
			 , @DBPath_Server = IsNull(MS.Server, '')
			 , @DBPath_Database = IsNull(MS.DBName, '')
		From dbo.MapSilo Silo With (Nolock) 
			Inner Join dbo.MapServer MS (Nolock) on Silo.CCDataServerId = MS.MapServerId
		Where 
			Silo.SiloCode = @SiloCode
	End
    Else If @DBName In ( 'CallDataExtDB' )
    Begin
       Select Top 1 @DBPath = Rtrim(IsNull(MS.Server + '.', ''))  +  Rtrim(Isnull(MS.DBName, '' ))
            , @DBPath_Server = IsNull(MS.Server, '')
            , @DBPath_Database = IsNull(MS.DBName, '')
         From dbo.MapSilo Silo With (Nolock) 
        Inner Join dbo.MapServer MS (Nolock) On Silo.SiloCode = MS.SiloCode
        Where Silo.SiloCode = @SiloCode
          And MS.DBClassInd = 'CTX'
          And MS.DBType     = 2  --ODBC
        Order By MS.ServerId

    End
End			

--
-- Surround output variables with [] if requested
--
If IsNull(@EnquoteOutputFlag, 0) = 1
Begin
   Set @DBPath =  dbo.Enquote_fn(@DBPath)
   Set @DBPath_Server = dbo.Enquote_fn(@DBPath_Server)
   Set @DBPath_Database = dbo.Enquote_fn(@DBPath_Database)
End

--
-- Null is not an acceptable return value
--   
--
If @DBPath Is Null
Begin
	
	Set @DBPath = Isnull(@DBPath, '')
	Return (-2)

End

Set @DBPath = Replace(@DBPath,'mssql-persistance.cdwswjp7nuvv.ca-central-1.rds.amazonaws.com','[' + @@ServerName + ']')

return	(1)

end
