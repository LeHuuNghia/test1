/**
 * Created by byan on 12/17/2015.
 */

import sql from 'mssql';
import Q from 'q';
import _ from 'lodash';

var SqlServerService = function(){
  this.cachedConnections = {};

  var connConfig = sails.config.connections[sails.config.lookupDB];
  this.persist = connConfig.persist;
}

// get connection from esn or account id on ctdata or ccdata
//dbName can be CTDATA or CCDATA, default to CTDATA
SqlServerService.prototype.getConnection = function(esn, accountId, dbName) {

  var self = this;
  var deferred = Q.defer();

  if ((esn == null || esn == undefined) && (accountId == null || accountId == undefined)) {
    var err = 'esn or accountId must have a value';
    deferred.reject(err);

    return deferred.promise;
  }

  if (dbName == null || dbName ==undefined) {
    dbName = 'CTDATA';
  }

  if (dbName != 'CTDATA' && dbName != 'CCDATA') {
    var err = 'Wrong dbName for connection: ' + dbName;
    deferred.reject(err);

    return deferred.promise;
  }

  var dbPathOut;

  self.getDBPath(dbName, accountId, esn)
    .then(function(dbPath){

      dbPathOut = dbPath;

      if (self.cachedConnections[dbPath] && self.cachedConnections[dbPath].connected && self.persist == true) {
        return deferred.resolve(self.cachedConnections[dbPath]);
      }
      else {
        return self.getConnectionByDBPath(dbPath);
      }

    })
    .then(function(connection){
      if (connection) {
        sails.log.info('dbPath for account = ' + dbPathOut + ', ' + accountId);
        deferred.resolve(connection);
      }
    })
    .catch(function (err) {
      sails.log.error('getConnection failed : ' + err);
      return deferred.reject(err);
    });

  return deferred.promise;
}

SqlServerService.prototype.getDBPath = function(dbName, accountId, esn) {

  var self = this;
  var deferred = Q.defer();

  var storedProc = 'dbo.ems_getDBPath';
  var params = [
    {
      name:'DBName',
      type:sql.VarChar(255),
      value:dbName,
      input:true
    },
    {
      name:'AccountId',
      type:sql.Int,
      value:accountId,
      input:true
    },
    {
      name:'ESN',
      type:sql.VarChar(20),
      value:esn,
      input:true
    },
    {
      name:'DBPath_Out',
      type:sql.VarChar(255),
      input:false
    }
  ];

  self.getLookupDBConnection()
    .then(function(connLookupDB){
      return self.execStoredProcedureOnDB(connLookupDB, storedProc, params);
    })
    .then(function(result){
      var dbPath = result.params.DBPath_Out.value; //like DV2CORP3DB.ctdata
      deferred.resolve(dbPath);
    })
    .catch(function (err) {
      sails.log.error('SqlServerService.prototype.getDBPath : ' + JSON.stringify({dbName:dbName, accountId:accountId, esn:esn}), err);
      return deferred.reject(err);
    });

  return deferred.promise;
}

SqlServerService.prototype.getRedirectionDBConnection = function() {

  var self = this;
  var deferred = Q.defer();



  var dbName = 'RedirectionDB';   //cache will by by this name
  if (self.cachedConnections[dbName] && self.cachedConnections[dbName].connected == true && self.persist == true) {
    deferred.resolve(self.cachedConnections[dbName]);
    return deferred.promise;
  }

  self.getDBPath(dbName,0,null)
    .then(function(dbPath){
      return self.getConnectionByDBPath(dbPath);
    })
    .then(function(connection){
      //cache it
      self.cachedConnections[dbName] = connection;
      deferred.resolve(connection);
    })
    .catch(function (err) {
      sails.log.error('getRedirectionDBConnection failed : ' + err);
      return deferred.reject(err);
    });

  return deferred.promise;
}


// dbPath is like DV2CORP3DB.ctdata
SqlServerService.prototype.getConnectionByDBPath = function(dbPath) {

  var self = this;
  var deferred = Q.defer();

  var dbPaths = dbPath.split('.');
  if (dbPaths.length < 2) {
    var err = 'getConnectionByDBPath failed... ' + 'dbPath ' + dbPath + ' is incorrect';
    sails.log.error(err);
    deferred.reject(err);

    return deferred.promise;
  }

  // assume all the silos use the same credential as lookupDB
  var connConfig = _.cloneDeep(sails.config.connections[sails.config.lookupDB]);  //sails.config.connections[sails.config.lookupDB];

  //To support in case of diffrent domain
  //EX : ems_api : dv2int2ems1api.dv.absolute.com
  //     sql     : dv2internals2.absolute.com
  //Because of ems_api and sql are different domain , so to make sure ems_api can connect to sql server, system need use full domain

  connConfig.server = dbPaths[0] + connConfig.full_domain;

  connConfig.database = dbPaths[1];
  connConfig.options.database = connConfig.database;


  var connection = new sql.ConnectionPool(connConfig, function(err) {

    if (err) {
      sails.log.error('getLookupDBConnection failed: ' + err);
      return deferred.reject(err);
    }

    //cache the connection
    self.cachedConnections[dbPath] = connection;
    deferred.resolve(connection);
  });

  return deferred.promise;
}

SqlServerService.prototype.getLookupDBConnection = function() {

  var self = this;
  var deferred = Q.defer();

  if (self.cachedConnections[sails.config.lookupDB] && self.cachedConnections[sails.config.lookupDB].connected == true && self.persist == true) {
    deferred.resolve(self.cachedConnections[sails.config.lookupDB]);
    return deferred.promise;
  }

  var connConfig = sails.config.connections[sails.config.lookupDB];

  var connection = new sql.ConnectionPool(connConfig, function(err) {

    if (err) {
      sails.log.error('getLookupDBConnection failed: ' + err);
      return deferred.reject(err);
    }

    //cache the connection
    self.cachedConnections[sails.config.lookupDB] = connection;
    deferred.resolve(connection);
  });


  return deferred.promise;

}

//this will be used for looking up DB: exec stored procedure with given connection
SqlServerService.prototype.execStoredProcedureOnDB = function (connection, storedProc, paramList) {

  var self = this;

  var deferred = Q.defer();

  var request  = connection.request();

  if (paramList && paramList.length > 0) {
    paramList.forEach(function (param) {

      if (param.input) {
        request.input(param.name, param.type, param.value)
      }
      else {
        request.output(param.name, param.type);
      }
    });
  }

  request.execute(storedProc, function (err, data, returnValue) {
    //close connection for non-persist connection
    if (self.persist == false) {
      connection.close();
    }
     if (err) {
       sails.log("Execute stored procedure error. " + err);
       return deferred.reject(err);
     }

	 if (data && data.output && Object.keys(data.output).length) {
      var returnList = data.output;
      for (var key in returnList) {
        if (returnList.hasOwnProperty(key)) {
          if(request && request.parameters && request.parameters[key]){
            request.parameters[key].value = returnList[key];
          }
        }
      }
    }

     return deferred.resolve({
		recordSet: data.recordsets,
		params: request.parameters,
		returnValue: data.returnValue
     })
  })

  return deferred.promise;

}

//execute stored procedure on ctdata based on esn or accountId
//esn, accountId one of them must be valid
//dbName is CTDATA or CCDATA. Default to CTDATA
SqlServerService.prototype.execStoredProcedure = function (esn, accountId, storedProc, paramList, dbName) {

  var self = this;

  var deferred = Q.defer();

  self.getConnection(esn, accountId, dbName)
    .then(function(connection){
      return self.execStoredProcedureOnDB(connection,storedProc, paramList);
    })
    .then(function(result){
      deferred.resolve(result);
    })
    .catch(function (err) {
      sails.log.error('execStoredProcedure failed: ' + storedProc + ', ' + err);
      return deferred.reject(err);
    });

  return deferred.promise;
}

//esn, accountId one of them must be valid
SqlServerService.prototype.execQueryOnDB = function (connection, sqlStmt, paramList) {

  var self = this;

  var deferred = Q.defer();


  var request = connection.request();

  if (paramList && paramList.length > 0) {
    paramList.forEach(function (param) {
      request.input(param.name, param.type, param.value);
    });
  }

  request.query(sqlStmt, function (err, data) {
    //close connection for non-persist connection
    if (self.persist == false) {
      connection.close();
    }

     if (err) {
        sails.log.error("execQueryOnDB error. " + sqlStmt + '. ' + err);
         return deferred.reject(err);
     }

     return deferred.resolve(data.recordset);
  });

  return deferred.promise;
}

// execute query on ctdata for a ESN or AccountId
//esn, accountId one of them must be valid
//dbName is CTDATA or CCDATA. Default to CTDATA
SqlServerService.prototype.execQuery = function (esn, accountId, sqlStmt, paramList, dbName) {

  var self = this;

  var deferred = Q.defer();

  self.getConnection(esn, accountId, dbName)
    .then(function (connection) {
      return self.execQueryOnDB(connection, sqlStmt, paramList );
    })
    .then(function(result){
      deferred.resolve(result);
    })
    .catch(function (err) {
      sails.log.error('execQuery failed: ' + sqlStmt + ', ' + err);
      return deferred.reject(err);
  });

  return deferred.promise;
}

//execute Stored Procedure on RedirectDB (Activation DB)
SqlServerService.prototype.execStoredProcedureOnRedirectDB = function (storedProc, paramList) {

  var self = this;

  var deferred = Q.defer();

  self.getRedirectionDBConnection()
    .then(function(connection){
      return self.execStoredProcedureOnDB(connection,storedProc, paramList);
    })
    .then(function(result){
      deferred.resolve(result);
    })
    .catch(function (err) {
      sails.log.error('execStoredProcedure failed: ' + storedProc + ', ' + err);
      return deferred.reject(err);
    });

  return deferred.promise;
}

//execute Stored Procedure on ActivationDB (Activation DB)
SqlServerService.prototype.execQueryOnRedirectDB = function (sqlStmt, paramList) {

  var self = this;

  var deferred = Q.defer();


  self.getRedirectionDBConnection()
    .then(function (connection) {
      return self.execQueryOnDB(connection, sqlStmt, paramList );
    })
    .then(function(result){
      deferred.resolve(result);
    })
    .catch(function (err) {
      sails.log.error('execQuery failed: ' + sqlStmt + ', ' + err);
      return deferred.reject(err);
    });

  return deferred.promise;
}

module.exports = new SqlServerService();
